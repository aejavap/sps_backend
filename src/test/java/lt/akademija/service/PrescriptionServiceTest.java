package lt.akademija.service;

import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.dto.interfaces.PrescriptionDTOInterface;
import lt.akademija.model.entity.Prescription;
import lt.akademija.repository.PrescriptionRepository;
import org.junit.Test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;


import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = lt.akademija.App.class)
public class PrescriptionServiceTest {


    @TestConfiguration
    static class ServiceTestContextConfiguration {
        @Bean
        PrescriptionService getPrescriptionService() {
            return new PrescriptionService();
        }
    }

    @Mock
    private PrescriptionRepository repository;

    @Mock
    private PrescriptionFillsService fillsService;

    @InjectMocks
    private PrescriptionService service;

    @Before
    public void setUp() throws Exception {
        PrescriptionDTO prescriptionDTO = mock(PrescriptionDTO.class);
        Prescription prescription = mock(Prescription.class);
        Long patientId = 15L;
        Long prescriptionId = 20L;
        Long patientPid = 30L;
        Long fillsNo = 100L;
        Date date = new Date();
        List<PrescriptionDTO> pList = Arrays.asList(new PrescriptionDTO[]{prescriptionDTO});
        Page<PrescriptionDTO> pPage = new PageImpl<>(pList);

        when(prescriptionDTO.getPrescriptionId()).thenReturn(prescriptionId);

        when(prescription.getId()).thenReturn(prescriptionId);

        when(fillsService.findFillsNoByPrescriptionId(prescriptionId)).thenReturn(fillsNo);

        when(repository.findAllByPatient_Id(eq(patientId), any(PageRequest.class))).thenAnswer(new Answer<Page<PrescriptionDTO>>() {
            @Override
            public Page<PrescriptionDTO> answer(InvocationOnMock invocationOnMock) throws Throwable {
                return pPage;
            }
        });

        when(repository.findAllByPatient_Pid(eq(patientPid), any(PageRequest.class))).thenAnswer(new Answer<Page<PrescriptionDTO>>() {
            @Override
            public Page<PrescriptionDTO> answer(InvocationOnMock invocationOnMock) throws Throwable {
                return pPage;
            }
        });

        when(repository.saveDTO(any(PrescriptionDTO.class))).thenAnswer(new Answer<PrescriptionDTO>() {
            @Override
            public PrescriptionDTO answer(InvocationOnMock inMock) throws Throwable {
                PrescriptionDTO DTO = inMock.getArgument(0);
                return DTO;
            }
        });

        when(repository.findOneById(prescriptionId)).thenAnswer(new Answer<PrescriptionDTOInterface>() {
            @Override
            public PrescriptionDTOInterface answer(InvocationOnMock invocationOnMock) throws Throwable {
                return prescriptionDTO;
            }
        });
    }


    @Test
    public void findAllByPatientIdTest() {
        Long patientId = 15L;
        service.findAllByPatientId(patientId, new PageRequest(0, 10));
        verify(repository).findAllByPatient_Id(anyLong(), any(PageRequest.class));
        verify(fillsService).findFillsNoByPrescriptionId(anyLong());
    }

    @Test
    public void findAllByPatientPidTest() {
        Long patientPid = 30L;
        service.findAllByPatientPid(patientPid, new PageRequest(0, 10));
        verify(repository).findAllByPatient_Pid(anyLong(), any(PageRequest.class));
        verify(fillsService).findFillsNoByPrescriptionId(anyLong());
    }

    @Test
    public void createFromDTOTest() throws Exception {
        PrescriptionDTO prescription = mock(PrescriptionDTO.class);
        assertThat(service.createFromDTO(prescription), is(prescription));
        verify(repository).saveDTO(any(PrescriptionDTO.class));
    }

    @Test
    public void findByPrescriptionIdTest() {
        Long prescriptionId = 20L;
        assertThat(service.findByPrescriptionId(prescriptionId).getPrescriptionId(), is(prescriptionId));
        verify(repository).findOneById(anyLong());
    }
}