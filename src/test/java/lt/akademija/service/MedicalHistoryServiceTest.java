package lt.akademija.service;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.dto.MedicalHistoryPkDTO;
import lt.akademija.model.dto.interfaces.MedicalHistoryDTOInterface;
import lt.akademija.model.entity.MedicalHistoryPK;
import lt.akademija.repository.MedicalHistoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = lt.akademija.App.class)
public class MedicalHistoryServiceTest {


    @TestConfiguration
    static class ServiceTestContextConfiguration {
        @Bean
        MedicalHistoryService getMedicalHistoryService() {
            return new MedicalHistoryService();
        }
    }

    @Mock
    private MedicalHistoryRepository repository;

    @InjectMocks
    private MedicalHistoryService service;

    @Before
    public void setUp() throws Exception {
        MedicalHistoryDTO historyDTO = mock(MedicalHistoryDTO.class);
        MedicalHistoryPkDTO historyPK = mock(MedicalHistoryPkDTO.class);
        List<MedicalHistoryDTO> lHistory = Arrays.asList(new MedicalHistoryDTO[]{historyDTO});

        Long goodId = 10L;
        Long goodPid = 1000L;
        Long goodDiagnosisId = 1L;
        Long goodDoctorId = 15L;
        Date goodDate = new Date();

        when(historyDTO.getPatientId()).thenReturn(goodId);
        when(historyDTO.getPatientPid()).thenReturn(goodPid);
        when(historyPK.getDiagnosisId()).thenReturn(goodDiagnosisId);
        when(historyPK.getDoctorId()).thenReturn(goodDoctorId);
        when(historyPK.getPatientId()).thenReturn(goodId);

        when(repository.findAllByMedicalHistoryPK_Patient_Id(eq(goodId), any(PageRequest.class))).thenReturn(new PageImpl(lHistory));
        when(repository.findByMedicalHistoryPK_Patient_Pid(eq(goodPid), any(PageRequest.class))).thenReturn(new PageImpl(lHistory));
        when(repository.findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp
                (goodId, goodDoctorId, goodDiagnosisId, goodDate)).thenReturn(historyDTO);
        when(repository.saveDTO(any(MedicalHistoryDTO.class))).thenAnswer(new Answer<MedicalHistoryDTO>() {
            @Override
            public MedicalHistoryDTO answer(InvocationOnMock invocationOnMock) throws Throwable {
                MedicalHistoryDTO history = invocationOnMock.getArgument(0);
                return history;
            }
        });
        when(repository.saveDTO(argThat(pk -> pk.getDoctorId() == goodDoctorId
                && pk.getDiagnosisId() == goodDiagnosisId
                && pk.getPatientId() == goodId))).thenReturn(historyDTO);
        when(repository.findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp
                (anyLong(), anyLong(), anyLong(),any(Date.class) )).thenAnswer(new Answer<MedicalHistoryDTO>() {
            @Override
            public MedicalHistoryDTO answer(InvocationOnMock inMock) throws Throwable {
                Long patientId = inMock.getArgument(0);
                Long doctorId = inMock.getArgument(1);
                Long diagnosisId = inMock.getArgument(2);
                Date date = inMock.getArgument(3);
                MedicalHistoryDTO historyDTO = new MedicalHistoryDTO();
                historyDTO.setPatientId(patientId);
                historyDTO.setDoctorId(doctorId);
                historyDTO.setDiagnosisId(diagnosisId);
                historyDTO.setDate(date);
                return historyDTO;
            }
        });
    }

    @Test
    public void findByPatientIdTestTest() throws Exception {
        Long id = 10L;
        service.findByPatientId(id, new PageRequest(1,10));
        verify(repository).findAllByMedicalHistoryPK_Patient_Id(eq(id), any(PageRequest.class));
    }

    @Test
    public void findByPatientPid() {
        Long pid = 1000L;
        service.findByPatientPid(pid, new PageRequest(1,10));
        verify(repository).findByMedicalHistoryPK_Patient_Pid(eq(pid), any(PageRequest.class));
    }

    @Test
    public void findByPkTest() throws Exception {
        Date date = mock(Date.class);
        Long patientId = 1L;
        Long diagnosisId = 2L;
        Long doctorId = 3L;
        MedicalHistoryDTO history = service.findByPk(patientId,doctorId,diagnosisId,date);
        assertThat(history.getPatientId(),is(patientId));
        assertThat(history.getDoctorId(), is(doctorId));
        assertThat(history.getDiagnosisId(),is(diagnosisId));
        verify(repository)
                .findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp
                        (anyLong(), anyLong(), anyLong(),any(Date.class));

    }

    @Test(expected = NullPointerException.class)
    public void findByPkNullDateTest() throws Exception {
        Date date = null;
        Long patientId = 1L;
        Long diagnosisId = 2L;
        Long doctorId = 3L;
        MedicalHistoryDTO history = service.findByPk(patientId,doctorId,diagnosisId,date);
        assertThat(history.getPatientId(),is(patientId));
        assertThat(history.getDoctorId(), is(doctorId));
        assertThat(history.getDiagnosisId(),is(diagnosisId));
        verify(repository)
                .findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp
                        (anyLong(), anyLong(), anyLong(),any(Date.class));

    }

    @Test(expected = Exception.class)
    public void findByPkNUllPatientTest() throws Exception {
        Date date = mock(Date.class);
        Long patientId = null;
        Long diagnosisId = 2L;
        Long doctorId = 3L;
        MedicalHistoryDTO history = service.findByPk(patientId,doctorId,diagnosisId,date);
        assertThat(history.getPatientId(),is(patientId));
        assertThat(history.getDoctorId(), is(doctorId));
        assertThat(history.getDiagnosisId(),is(diagnosisId));
        verify(repository)
                .findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp
                        (anyLong(), anyLong(), anyLong(),any(Date.class));

    }

    @Test (expected = Exception.class)
    public void findByPkNullDoctorTest() throws Exception {
        Date date = mock(Date.class);
        Long patientId = 1L;
        Long diagnosisId = 2L;
        Long doctorId = null;
        MedicalHistoryDTO history = service.findByPk(patientId,doctorId,diagnosisId,date);
        assertThat(history.getPatientId(),is(patientId));
        assertThat(history.getDoctorId(), is(doctorId));
        assertThat(history.getDiagnosisId(),is(diagnosisId));
        verify(repository)
                .findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp
                        (anyLong(), anyLong(), anyLong(),any(Date.class));

    }

    @Test(expected = Exception.class)
    public void findByPkNullDianosisTest() throws Exception {
        Date date = mock(Date.class);
        Long patientId = 1L;
        Long diagnosisId = null;
        Long doctorId = 3L;
        MedicalHistoryDTO history = service.findByPk(patientId,doctorId,diagnosisId,date);
        assertThat(history.getPatientId(),is(patientId));
        assertThat(history.getDoctorId(), is(doctorId));
        assertThat(history.getDiagnosisId(),is(diagnosisId));
        verify(repository)
                .findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp
                        (anyLong(), anyLong(), anyLong(),any(Date.class));

    }



    @Test
    public void createByDTOTest() throws Exception {
     MedicalHistoryDTO historyDTO = mock(MedicalHistoryDTO.class);
     assertThat(service.createByDTO(historyDTO), is(historyDTO));
     verify(repository).saveDTO(any(MedicalHistoryDTO.class));
    }
}