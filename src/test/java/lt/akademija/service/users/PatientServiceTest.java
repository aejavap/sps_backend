package lt.akademija.service.users;

import lt.akademija.model.dto.search.PatientSearchDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.PatientRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;


import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = lt.akademija.App.class)
public class PatientServiceTest {

    /**
     * Test shows that this class smells
     */
    @TestConfiguration
    static class ServiceTestContextConfiguration {
        @Bean
        PatientService getPatientService() {
            return new PatientService();
        }
    }

    @Mock
    private PatientRepository repository;
    @InjectMocks
    private PatientService service;

    @Before
    public void setUp() throws Exception
    {
        Patient patient = mock(Patient.class);
        PatientDTO patientDTO = mock(PatientDTO.class);
        Long existingPid = 1000L;
        Long existingId = 10L;
        Long doctorId =15L;
        Long badDoctorId = 20L;

        Patient patient1 = mock(Patient.class);
        Patient patient2 = mock(Patient.class);
        Patient patient3 = mock(Patient.class);

        List<Patient> pList = Arrays.asList(new Patient[] {patient1, patient2, patient3});
//        when(patient.getPid()).thenReturn(existingPid);
//        when(patient.getId()).thenReturn(existingId);

//        when(repository.findOneByPid(existingPid)).thenAnswer(new Answer<Patient>() {
//            @Override
//            public Patient answer(InvocationOnMock invocationOnMock) throws Throwable {
//                return patient;
//            }
//        });
//        when(repository.findOneByPid(anyLong())).thenReturn(null);
        when(repository.findPatientListByDoctorId(doctorId)).thenReturn(pList);
        when(repository.findPatientListByDoctorId(badDoctorId)).thenReturn(new ArrayList());
        when(repository.searchByNameAndSurnameAndPid(anyString(), anyString(), anyString(), anyLong(), any(PageRequest.class)))
                .thenReturn(new PageImpl<Patient>(pList));
        when(repository.searchByNameAndSurnameAndPidAndDiagnosis(anyString(),anyString(),anyString(),anyString(),anyLong(),any(PageRequest.class)))
                .thenReturn(new PageImpl<Patient>(pList));
    }

    @Test
    public void searchByNameAndSurnameAndPidAndDiagnosisTest() throws Exception {
        PatientSearchDTO search = new PatientSearchDTO();
        search.setFirstName("test");
        search.setPid(385L);
        search.setDoctorId(10L);
        service.searchByNameAndSurnameAndPidAndDiagnosis(search, new PageRequest(1,10));
        verify(repository).searchByNameAndSurnameAndPid(anyString(), anyString(), anyString(), anyLong(), any(PageRequest.class));
        search.setDiagnosisTitle("title");
        service.searchByNameAndSurnameAndPidAndDiagnosis(search, new PageRequest(1,10));
        verify(repository).searchByNameAndSurnameAndPidAndDiagnosis(anyString(), anyString(), anyString(), anyString(),
                anyLong(), any(PageRequest.class));
    }

    @Test(expected = Exception.class)
    public void searchByNameAndSurnameAndPidAndDiagnosisDoctorNullPassedTest() throws Exception {
        PatientSearchDTO search = new PatientSearchDTO();
        search.setFirstName("test");
        search.setPid(385L);
        service.searchByNameAndSurnameAndPidAndDiagnosis(search, new PageRequest(1,10));
    }

    @Test
    public void searchByNameAndSurnameAndPidAndDiagnosisAllNullPassedTest() throws Exception {
        PatientSearchDTO search = new PatientSearchDTO();
        search.setDoctorId(10L);
        service.searchByNameAndSurnameAndPidAndDiagnosis(search, new PageRequest(1,10));
        verify(repository).searchByNameAndSurnameAndPid(anyString(), anyString(), anyString(), anyLong(), any(PageRequest.class));
    }


    @Test
    public void findPatientsByDoctorIdTest() {
        List<PatientDTO> result = service.findPatientsByDoctorId(15L);
        assertThat(result.isEmpty(), is(false));
        assertThat(result.size(), is(3));
        verify(repository).findPatientListByDoctorId(anyLong());

    }

    @Test
    public void findPatientsByDoctorIdNoResultTest() {
        List<PatientDTO> result = service.findPatientsByDoctorId(20L);
        assertThat(result.isEmpty(), is(true));
        verify(repository).findPatientListByDoctorId(anyLong());
    }


//    @Test
//    public void findPrescriptionsTest() {
//    }
//    @Test
//    public void findPatientByPidTest() {
//        Long testPid = 1000L;
//        assertThat(service.findByPidDTO(1000L).getPid(), is(testPid));
//        verify(repository).findOneByPid(anyLong());
//    }
//
//    @Test
//    public void findPatientByPidNotFoundTest() {
//        Long testPid = 100L;
//        assertNull(service.findByPidDTO(100L));
//        verify(repository).findOneByPid(anyLong());
//    }
//
//    @Test
//    public void findMedicalRecords() {
//    }
//
//    @Test
//    public void updateByIdDTO() {
//    }
//
//    @Test
//    public void findPrescriptions() {
//    }
//
//    @Test
//    public void findPatientList() {
//    }
//
//    @Test
//    public void createDTO() {
//    }
//
//    @Test
//    public void searchByNameAndSurnameAndPidAndDiagnosis() {
//    }
//
//    @Test
//    public void findAllByDob() {
//    }
//
//    @Test
//    public void findPatientsByDoctorId() {
//    }

}