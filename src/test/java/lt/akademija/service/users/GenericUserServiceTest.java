package lt.akademija.service.users;

import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.PatientRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = lt.akademija.App.class)
public class GenericUserServiceTest {

    @TestConfiguration
    static class ServiceTestContextConfiguration {
        @Bean
        PatientService getPatientService() {
            return new PatientService();
        }
    }

    @Mock
    private PatientRepository repository;

    @InjectMocks
    private PatientService service;

    @Before
    public void setUp() throws Exception {
        Patient patient = mock(Patient.class);
        PatientDTO patientDTO = mock(PatientDTO.class);
        Long existingPid = 1000L;
        Long existingId = 10L;

        Patient patient1 = mock(Patient.class);
        Patient patient2 = mock(Patient.class);
        Patient patient3 = mock(Patient.class);

        List<Patient> pList = Arrays.asList(new Patient[] {patient1, patient2, patient3});

        when(patient.getId()).thenReturn(existingId);
        when(patient.getPid()).thenReturn(existingPid);
        when(patientDTO.getId()).thenReturn(existingId);
        when(patientDTO.getPid()).thenReturn(existingPid);

        when(repository.findAll(any(PageRequest.class))).thenReturn(new PageImpl(pList));
        when(repository.findOneByPid(existingPid)).thenReturn(patient);
        when(repository.findOne(existingId)).thenReturn(patient);
        when(repository.saveDTO(argThat(p -> p.getPid() == 1000L))).thenReturn(patientDTO);
    }

    @Test
    public void findAllTest() {
        PageRequest pageR = new PageRequest(1,10);
        service.findAll(pageR);
        verify(repository).findAll(any(PageRequest.class));
    }

    @Test
    public void findByIdDTOTest() throws Exception{
        Long id = 10L;
        assertThat(service.findByIdDTO(id), is(instanceOf(PatientDTO.class)));
        verify(repository).findOne(anyLong());
        PatientDTO patient =  service.findByIdDTO(id);
        assertThat(patient.getId(),is(id));
    }

    @Test
    public void findByIdDTOWithWrongIdTest() throws Exception{
        Long id = 15L;
        assertNull(service.findByIdDTO(id));
        verify(repository).findOne(anyLong());
    }

    @Test
    public void findByIdTest() throws Exception {
        Long id = 10L;
        assertThat(service.findById(id), is(instanceOf(Patient.class)));
        verify(repository).findOne(anyLong());
        Patient patient =  service.findById(id);
        assertThat(patient.getId(),is(id));
    }

    @Test
    public void findByIdWithWrongIdTest() throws Exception{
        Long id = 15L;
        assertNull(service.findById(id));
        verify(repository).findOne(anyLong());
    }

    @Test
    public void findByPidDTOTest() throws Exception {
        Long pid = 1000L;
        assertThat(service.findByPidDTO(pid), is(instanceOf(PatientDTO.class)));
        verify(repository).findOneByPid(anyLong());
        PatientDTO patient =  service.findByPidDTO(pid);
        assertThat(patient.getPid(),is(pid));
    }

    @Test (expected = Exception.class)
    public void findByPidDTOWithWrongPidTest() throws Exception{
        Long pid = 15L;
        service.findByPidDTO(pid);
        verify(repository).findOneByPid(anyLong());
    }

    @Test
    public void findByPidTest() throws Exception {
        Long pid = 1000L;
        assertThat(service.findByPid(pid), is(instanceOf(Patient.class)));
        verify(repository).findOneByPid(anyLong());
        Patient patient =  service.findByPid(pid);
        assertThat(patient.getPid(),is(pid));
    }


    @Test
    public void findByPidWithWrongPidTest() throws Exception{
        Long pid = 15L;
        assertNull(service.findByPid(pid));
        verify(repository).findOneByPid(anyLong());
    }


    @Test
    public void createUserTest() throws Exception {
        PatientDTO patient = new PatientDTO();
        patient.setPid(1000L);
        assertThat(service.createUser(patient),is(instanceOf(PatientDTO.class)));
        verify(repository).saveDTO(any(PatientDTO.class));
        assertThat(service.createUser(patient).getId(),is(10L));
    }

    @Test
    public void createEntityTest() throws Exception {
        PatientDTO patient = new PatientDTO();
        patient.setPid(1000L);
        assertThat(service.createEntity(patient),is(instanceOf(Patient.class)));
        verify(repository).saveDTO(any(PatientDTO.class));
        assertThat(service.createUser(patient).getId(),is(10L));
    }
//
    @Test
    public void removeByIdTest() {
        service.removeById(10L);
        verify(repository).delete(anyLong());
    }
//
//    @Test
//    public void updateByPid() {
//    }
//
//    @Test
//    public void update() {
//    }
//
//    @Test
//    public void searchUsersByNameOrSurnameOrPid() {
//    }
//
//    @Test
//    public void canHandle() {
//    }
//
//    @Test
//    public void getType() {
//    }
}