package lt.akademija.service.Facade;

import lt.akademija.model.dto.AccountDTO;
import lt.akademija.model.dto.AccountDTOFull;
import lt.akademija.model.dto.users.*;
import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.simple.AbstractSimpleEntity;
import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.model.entity.users.*;
import lt.akademija.service.AccountService;
import lt.akademija.service.simple.DiagnosisService;
import lt.akademija.service.simple.GenericSimpleService;

import lt.akademija.service.simple.IngredientService;
import lt.akademija.service.simple.SpecializationService;
import lt.akademija.service.users.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import javax.security.auth.login.AccountException;
import java.lang.reflect.Method;
import java.util.*;

//import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalMatchers.gt;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = lt.akademija.App.class)
public class AdminFacadeTest {
    /**
     * Test shows that this class smells
     */
    @TestConfiguration
    static class ServiceTestContextConfiguration {
        @Bean
        AdminFacade getAdminFacade() {
            return new AdminFacade();
        }
    }

    @Mock
    private AccountService accountService;
    @Mock
    private PatientService patientService;
    @Mock
    private DoctorService doctorService;
    @Mock
    private PharmacistService pharmacistService;
    @Mock
    private AdminService adminService;
    @Mock
    private IngredientService ingredientService;
    @Mock
    private SpecializationService specializationService;
    @Mock
    private DiagnosisService diagnosisService;
    @InjectMocks
    private AdminFacade adminFacade;

    @Before
    public void setUp() throws Exception {
//        MockitoAnnotations.initMocks(this);
        /**
         * Nesiautowirina automatiskai Mockai i lista :/
         */
        List<GenericUserService<?, ? extends AbstractUserDTO>> userServiceList = new ArrayList<>();
        List<GenericSimpleService<? extends AbstractSimpleEntity>> simpleServiceList = new ArrayList<>();

        userServiceList.add(patientService);
        userServiceList.add(doctorService);
        userServiceList.add(pharmacistService);
        userServiceList.add(adminService);

        simpleServiceList.add(ingredientService);
        simpleServiceList.add(specializationService);
        simpleServiceList.add(diagnosisService);

        adminFacade.setUserServiceList(userServiceList);
        adminFacade.setSimpleServiceList(simpleServiceList);
        adminFacade.setAccountService(accountService);

        PatientDTO dummyPatient = mock(PatientDTO.class);
//        DoctorDTO dummyDoctor = mock(DoctorDTO.class);
        Doctor dummyEDoctor = mock(Doctor.class);
        PharmacistDTO dummyPharmacist = mock(PharmacistDTO.class);
        AdminDTO dummyAdmin = mock(AdminDTO.class);
        AccountDTOFull dummyAccount = mock(AccountDTOFull.class);

        DoctorDTO goodDoctor = new DoctorDTO();
        goodDoctor.setId(10L);
        goodDoctor.setPid(100L);

        PatientDTO goodPatient = new PatientDTO();
        goodPatient.setDoctorPid(100L);
        goodPatient.setDoctorId(10L);
        goodPatient.setId(15L);
        goodPatient.setPid(30L);

        Patient goodPatient2 = new Patient();
        goodPatient.setDoctorPid(100L);
        goodPatient.setDoctorId(10L);
        goodPatient.setId(15L);
        goodPatient.setPid(30L);

        Account account = new Account();
        account.setUsername("test");
        account.setPassword("test");

        /**
         * GetType stubs
         */
        when(doctorService.getType()).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return Doctor.class;
            }
        });

        when(patientService.getType()).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return Patient.class;
            }
        });

        when(pharmacistService.getType()).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return Pharmacist.class;
            }
        });

        when(adminService.getType()).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return Admin.class;
            }
        });
        /**
         * Patient find by id and pid
         */

        when(patientService.findByIdDTO(gt(0L))).thenReturn(dummyPatient);

        when(patientService.findByPidDTO(gt(0L))).thenReturn(dummyPatient);

        when(patientService.findByPidDTO(300L)).thenReturn(null);

        when(patientService.findById(15L)).thenReturn(goodPatient2);

        when(patientService.findByPidDTO(20L)).thenReturn(dummyPatient);

        when(patientService.update(gt(0L), any(PatientDTO.class))).thenAnswer(new Answer<PatientDTO>() {
            @Override
            public PatientDTO answer(InvocationOnMock invocationOnMock) throws Throwable {
                PatientDTO patientDTO = (PatientDTO) invocationOnMock.getArgument(1);
                Long id = (Long) invocationOnMock.getArgument(0);
                PatientDTO patient = new PatientDTO();
                BeanUtils.copyProperties(patientDTO, patient);
                patient.setId(id);
                return patient;
            }
        });

        when(patientService.updateByPid(gt(0L), any(PatientDTO.class))).thenAnswer(new Answer<PatientDTO>() {
            @Override
            public PatientDTO answer(InvocationOnMock invocationOnMock) throws Throwable {
                PatientDTO patientDTO = (PatientDTO) invocationOnMock.getArgument(1);
                Long id = (Long) invocationOnMock.getArgument(0);
                PatientDTO patient = new PatientDTO();
                BeanUtils.copyProperties(patientDTO, patient);
                patient.setPid(id);
                return patient;
            }
        });

        //Doctor for patient create search
        when(doctorService.findByIdDTO(1L)).thenReturn(null);

        when(doctorService.findByPid(20L)).thenAnswer(new Answer<Doctor>() {
            @Override
            public Doctor answer(InvocationOnMock invocationOnMock) throws Throwable {
                Doctor doctor = new Doctor();
                doctor.setPid(20L);
                doctor.setId(1L);
                doctor.setFirstName("TGydytojas");
                doctor.setLastName("Tikrai geras");
                return doctor;
            }
        });

        when(doctorService.findByPidDTO(1L)).thenReturn(null);

        when(doctorService.findByIdDTO(10L)).thenAnswer(new Answer<DoctorDTO>() {
            @Override
            public DoctorDTO answer(InvocationOnMock invocationOnMock) throws Throwable {
                DoctorDTO doctor = new DoctorDTO();
                doctor.setId(10L);
                doctor.setPid(100L);
                return doctor;
            }
        });

        when(doctorService.findByPidDTO(100L)).thenAnswer(new Answer<DoctorDTO>() {
            @Override
            public DoctorDTO answer(InvocationOnMock invocationOnMock) throws Throwable {
                DoctorDTO doctor = new DoctorDTO();
                doctor.setPid(100L);
                doctor.setId(10L);
                return doctor;
            }
        });

        when(doctorService.createEntity(any(DoctorDTO.class))).thenAnswer(new Answer<Doctor>() {
            @Override
            public Doctor answer(InvocationOnMock invocationOnMock) throws Throwable {
                Doctor doctor = new Doctor();
                doctor.setPid(100L);
                doctor.setId(10L);
                return doctor;
            }
        });


        when(accountService.getAccountDTOById(30L)).thenReturn(dummyAccount);

        when(accountService.createAccount(any(Account.class), any(Patient.class))).thenAnswer(new Answer<Account>() {
            @Override
            public Account answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (invocationOnMock != null && invocationOnMock.getArguments() != null) {
                    Account account = (Account) invocationOnMock.getArguments()[0];
                    account.setId(15L);
                    return account;
                }
                return null;
            }
        });

        when(accountService.createAccount(argThat(acc -> acc.getUsername().equals("test")), any(Doctor.class))).thenThrow(new AccountException());

        when(accountService.existsById(15L)).thenReturn(false);

        when(accountService.existsById(333L)).thenReturn(true);

        when(accountService.updateById(anyLong(), any(AccountDTO.class))).thenAnswer(new Answer<Account>() {
            @Override
            public Account answer(InvocationOnMock invocationOnMock) throws Throwable {
                Account account = new Account();
                AccountDTO dto = (AccountDTO) invocationOnMock.getArgument(1);
                BeanUtils.copyProperties(dto, account);
                account.setId((Long) invocationOnMock.getArgument(0));
                return account;
            }
        });


        when(ingredientService.create(any(Ingredient.class))).thenAnswer(new Answer<Ingredient>() {
            @Override
            public Ingredient answer(InvocationOnMock invocationOnMock) throws Throwable {
                Ingredient ingredient = (Ingredient) invocationOnMock.getArguments()[0];
                ingredient.setId(10L);
                System.out.println("ing: " + ingredient);
                return ingredient;
            }
        });

        when(ingredientService.findByTitle("test")).thenAnswer(new Answer<Ingredient>() {
            @Override
            public Ingredient answer(InvocationOnMock invocationOnMock) throws Throwable {
                System.out.println("ingredient update called");
                if (invocationOnMock.getArgument(0).equals("test")) {
                    Ingredient ingredient = new Ingredient();
                    ingredient.setId(10L);
                    ingredient.setTitle("test");
                    return ingredient;
                }
                return null;
            }
        });

        when(ingredientService.update(gt(0L), any(Ingredient.class))).thenAnswer(new Answer<Ingredient>() {
            @Override
            public Ingredient answer(InvocationOnMock invocationOnMock) throws Throwable {
                System.out.println("ingredient update called");
                if (invocationOnMock.getArgument(0) == (Long) 10L) {
                    Ingredient ingredient = invocationOnMock.getArgument(1);
                    ingredient.setId(10L);
                    ingredient.setTitle("test");
                    return ingredient;
                }
                return null;
            }
        });
    }

    /*
    1. Test that class chooses correct user service
     */

    @Test
    public void chooseUserServiceCorrectlyTest() throws Exception {
        //Code smells if you have to test private method with reflections
        //On the other hand other testing all other methods of this class
        //sums up to testing this method
        Method method = AdminFacade.class.getDeclaredMethod("chooseUserService", String.class);
        method.setAccessible(true);
        assertEquals(patientService, method.invoke(adminFacade, "patient"));
        assertEquals(doctorService, method.invoke(adminFacade, "doctor"));
        assertEquals(pharmacistService, method.invoke(adminFacade, "pharmacist"));
        assertEquals(adminService, method.invoke(adminFacade, "admin"));
    }

      /*
    2. Test that class chooses correct service
     */

    @Test
    public void chooseSimpleServiceCorrectlyTest() throws Exception {
        //Code smells if you have to test private methods with reflections
        //On the other hand other testing all other methods of this class
        //sums up to testing this method
        Method method = AdminFacade.class.getDeclaredMethod("chooseSimpleService", String.class);
        method.setAccessible(true);
        assertEquals(ingredientService, method.invoke(adminFacade, "ingredient"));
        assertEquals(diagnosisService, method.invoke(adminFacade, "diagnosis"));
        assertEquals(specializationService, method.invoke(adminFacade, "specialization"));
    }

    /*
  3. Test that positive id passed
   */
    @Test
    public void getUserByPositiveIdTest() throws Exception {
        assertThat(adminFacade.getUserDetailsById("patient", 10L), is(instanceOf(PatientDTO.class)));
        verify(patientService).findByIdDTO(anyLong());
    }

       /*
  4. Test that negative id throws exception
   */

    @Test(expected = IllegalArgumentException.class)
    public void failGetUserByNegativeIdTest() throws Exception {
        PatientDTO dPatient = mock(PatientDTO.class);
        assertEquals(dPatient.getClass(), adminFacade.getUserDetailsById("patient", -10L).getClass());
        verify(patientService, never()).findByIdDTO(anyLong());
    }

    @Test
    public void getUserByPositivePid() throws Exception {
        PatientDTO dPatient = mock(PatientDTO.class);
        assertThat(adminFacade.getUserDetailsByPid("patient", 10L), is(instanceOf(PatientDTO.class)));
        verify(patientService).findByPidDTO(anyLong());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserByNegativePid() throws Exception {
        PatientDTO dPatient = mock(PatientDTO.class);
        assertThat(adminFacade.getUserDetailsByPid("patient", -10L), is(instanceOf(PatientDTO.class)));
        verify(patientService, never()).findByPidDTO(anyLong());
    }

    @Test
    public void getAccountByPositiveId() throws Exception {
        assertThat(adminFacade.getAccountById(30L), is(instanceOf(AccountDTOFull.class)));
        verify(accountService).getAccountDTOById(anyLong());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAccountByNegativeId() throws Exception {
        assertThat(adminFacade.getAccountById(-10L), is(instanceOf(AccountDTOFull.class)));
        verify(accountService, never()).getAccountDTOById(anyLong());
    }

    //Tests create user with  account
//    @Test
//    public void createPatientWithoutAccountWithDoctorPidData() throws Exception {
//        PatientDTO patient = new PatientDTO();
//        patient.setDoctorPid(100L);
//        patient.setDoctorId(null);
//        patient.setPid(300L);
//        assertThat(adminFacade.createPatient(patient),is(15L));
//        verify(patientService).createUser(patient);
//        verify(doctorService).findByPidDTO(patient.getDoctorPid());
//    }

//    @Test
//    public void createPatientWithoutAccountWithDoctorIdData() throws Exception {
//        PatientDTO patient = new PatientDTO();
//        patient.setDoctorId(10L);
//        patient.setPid(300L);
//        assertThat(adminFacade.createPatient(patient),is(15L));
//        verify(patientService).createUser(patient);
//        verify(doctorService).findByIdDTO(patient.getDoctorId());
//    }

    @Test(expected = IllegalArgumentException.class)
    public void createPatientWithoutAccountWithBadDoctorIdDataTest() throws Exception {
        PatientDTO patient = new PatientDTO();
        patient.setPid(20L);
        patient.setDoctorId(1L);
        adminFacade.createPatient(patient);
        verify(doctorService).findById(patient.getDoctorId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createPatientWithoutAccountWithBadDoctorPidDataTest() throws Exception {
        PatientDTO patient = new PatientDTO();
        patient.setDoctorPid(1L);
        patient.setPid(20L);
        adminFacade.createPatient(patient);
    }

    @Test(expected = Exception.class)
    public void createPatientWithoutAccountWithDoctorWithExistingPidTest() throws Exception {
        PatientDTO patient = new PatientDTO();
        patient.setDoctorId(10L);
        patient.setDoctorPid(100L);
        patient.setPid(10L);
        Long id = adminFacade.createPatient(patient);
    }

    @Test(expected = Exception.class)
    public void createUserDoctorWithExistingEntityPidTest() throws Exception {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setPid(20L);
        doctorDTO.setFirstName("Testas");
        doctorDTO.setLastName("Testauskas");
        adminFacade.createUser("doctor", doctorDTO, "testUsername", "testPassword");
    }

    @Test(expected = Exception.class)
    public void createUserAdminWithExistingEntityPidTest() throws Exception {
        AdminDTO adminDTO = new AdminDTO();
        adminDTO.setPid(20L);
        adminFacade.createUser("admin", adminDTO, "testUsername", "testPassword");
    }

    @Test(expected = Exception.class)
    public void createUserPharmacistWithExistingEntityPidTest() throws Exception {
        PharmacistDTO pharmacistDTO = new PharmacistDTO();
        pharmacistDTO.setPid(20L);
        adminFacade.createUser("pharmacist", pharmacistDTO, "testUsername", "testPassword");
    }

    @Test(expected = Exception.class)
    public void createUserPatientCallTest() throws Exception {
        PatientDTO patientDTO = new PatientDTO();
        adminFacade.createUser("patient", patientDTO, "testUsername", "testPassword");
    }

    @Test
    public void createUserCorrectIdTest() throws Exception {
        DoctorDTO doctorDTO = new DoctorDTO();
        Long id = adminFacade.createUser("doctor", doctorDTO, "testUsername", "testPassword");
        assertEquals((Long) 10L, id);
        verify(doctorService, times(1)).createEntity(any(DoctorDTO.class));

    }

    @Test(expected = AccountException.class)
    public void createUserWithExistingUserNameTest() throws Exception {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setId(10L);
        doctorDTO.setPid(100L);
        Long id = adminFacade.createUser("doctor", doctorDTO, "test", "test");
    }

    @Test
    public void createPatientAccountCorrectTest() throws Exception {
        AccountDTO account = new AccountDTO();
        account.setPassword("testukas");
        account.setUsername("testukas");

        assertThat(adminFacade.createPatientAccount(15L, account), is(15L));
        verify(accountService).createAccount(any(Account.class), any(Patient.class));
    }

    @Test(expected = AccountException.class)
    public void createPatientAccountExceptionTest() throws Exception {
        AccountDTO dummyAccount = mock(AccountDTO.class);
        adminFacade.createPatientAccount(333L, dummyAccount);
        verify(accountService).createAccount(any(Account.class), any(Patient.class));
    }

    @Test
    public void createSimpleEntityCorrectlyTest() throws Exception {
        Ingredient ing = new Ingredient();
        ing.setTitle("simple");
        assertThat(adminFacade.createSimpleEntity("ingredient", ing), is(10L));
        verify(ingredientService).create(any(Ingredient.class));
    }

    @Test(expected = Exception.class)
    public void createSimpleEntityWithExistingTitleTest() throws Exception {
        Ingredient ing = new Ingredient();
        ing.setTitle("test");
        adminFacade.createSimpleEntity("ingredient", ing);
    }

    @Test
    public void updateUserAccountByIdTest() throws Exception {
        Long id = 15L;
        AccountDTO dummyAcc = mock(AccountDTO.class);
        assertThat(adminFacade.updateUserAccountById(id, dummyAcc).getId(), is(id));
        verify(accountService).updateById(eq(id), any(AccountDTO.class));
    }

    @Test
    public void updateUserByIdTest() throws Exception {
        Long id = 15L;
        PatientDTO dummyPat = mock(PatientDTO.class);
        assertThat(adminFacade.updateUserById("patient", id, dummyPat).getId(), is(id));
        verify(patientService).update(eq(id), any(PatientDTO.class));
    }

    //
    @Test
    public void updateUserByPidTest() throws Exception {
        Long pid = 15L;
        PatientDTO dummyPat = mock(PatientDTO.class);
        assertThat(adminFacade.updateUserByPid("patient", pid, dummyPat).getPid(), is(pid));
        verify(patientService).updateByPid(eq(pid), any(PatientDTO.class));
    }

    @Test(expected = Exception.class)
    public void updateSimpleEntityByIdNotFoundTest() throws Exception {
        Ingredient ing = new Ingredient();
        ing.setId(10L);
        ing.setTitle("test");
        adminFacade.updateSimpleEntityById("ingredient", 10L, ing);
    }

    @Test
    public void updateSimpleEntityByIdTest() throws Exception {
        Ingredient ing = new Ingredient();
        ing.setTitle("testUpdated");
        assertThat(adminFacade.updateSimpleEntityById("ingredient", 10L, ing).getId(), is(10L));
        verify(ingredientService).update(anyLong(), eq(ing));
    }
//
//    @Test
//    public void setEnabledById(){}
//
//    @Test
//    public void resetPasswordById(){}
//
//    @Test
//    public void searchUsersByNameOrSurnameOrPid() {
//    }
//
}