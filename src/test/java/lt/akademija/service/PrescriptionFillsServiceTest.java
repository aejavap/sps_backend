package lt.akademija.service;

import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.dto.interfaces.PrescriptionDTOInterface;
import lt.akademija.model.dto.interfaces.PrescriptionFillsDTOInterface;
import lt.akademija.model.entity.Prescription;
import lt.akademija.repository.PrescriptionFillsRepository;
import lt.akademija.repository.PrescriptionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;


import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;


@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = lt.akademija.App.class)
public class PrescriptionFillsServiceTest {


    @TestConfiguration
    static class ServiceTestContextConfiguration {
        @Bean
        PrescriptionFillsService getService() {
            return new PrescriptionFillsService();
        }
    }

    @Mock
    private PrescriptionFillsRepository fillsRepository;

    @Mock
    private PrescriptionRepository prescriptionRepository;

    @InjectMocks
    private PrescriptionFillsService service;

    @Before
    public void setUp() throws Exception {
        PrescriptionFillsDTO fillsDTO = mock(PrescriptionFillsDTO.class);
        Long pharmacistId = 10L;
        Long prescriptionId = 20L;
        Long fillsNo = 100L;
        Date date = new Date();
        List<PrescriptionFillsDTO> pL = Arrays.asList(new PrescriptionFillsDTO[]{fillsDTO});
        Page<PrescriptionFillsDTO> pFills = new PageImpl<>(pL);


        when(fillsDTO.getPrescriptionId()).thenReturn(prescriptionId);
        when(fillsDTO.getPhramacistId()).thenReturn(pharmacistId);
        when(fillsDTO.getDate()).thenReturn(date);

        when(prescriptionRepository.findOneById(prescriptionId)).thenAnswer(new Answer<PrescriptionDTO>() {
            @Override
            public PrescriptionDTO answer(InvocationOnMock invocationOnMock) throws Throwable {
                Long prescriptionId = invocationOnMock.getArgument(0);
                PrescriptionDTO prescriptionDTO = new PrescriptionDTO();
                prescriptionDTO.setPrescriptionId(prescriptionId);
                return prescriptionDTO;
            }
        });

        when(fillsRepository.saveDTO(any(PrescriptionFillsDTO.class))).thenAnswer(new Answer<PrescriptionFillsDTO>() {
            @Override
            public PrescriptionFillsDTO answer(InvocationOnMock inMock) throws Throwable {
               PrescriptionFillsDTO fillsDTO = inMock.getArgument(0);
               return fillsDTO;
            }
        });

        when(fillsRepository.countByPrescriptionFillsPK_Prescription_Id(prescriptionId)).thenReturn(fillsNo);

        when(fillsRepository.findByPrescriptionFillsPK_Prescription_Id(anyLong(), any(PageRequest.class))).thenAnswer(new Answer<Page<PrescriptionFillsDTO>>() {
            @Override
            public Page<PrescriptionFillsDTO> answer(InvocationOnMock invocationOnMock) throws Throwable {
                return pFills;
            }
        });
    }

    @Test
    public void createByDTOTest() throws Exception {
        PrescriptionFillsDTO fillsDTO = new PrescriptionFillsDTO();
        fillsDTO.setPrescriptionId(20L);
        fillsDTO.setDate(new Date());
        fillsDTO.setPhramacistId(10L);
        assertThat(service.createByDTO(fillsDTO), is(fillsDTO));
    }

    @Test(expected = NullPointerException.class)
    public void createByDTONoPrescriptionTest() throws Exception {
        PrescriptionFillsDTO fillsDTO = new PrescriptionFillsDTO();
        fillsDTO.setPrescriptionId(10L);
        fillsDTO.setDate(new Date());
        fillsDTO.setPhramacistId(10L);
        assertThat(service.createByDTO(fillsDTO), is(fillsDTO));
    }

    @Test
    public void findFillsNoByPrescriptionIdTest() {
        Long prescriptionId = 20L;
        Long fillsNo = 100L;
        assertThat(service.findFillsNoByPrescriptionId(prescriptionId), is(fillsNo));
        verify(fillsRepository).countByPrescriptionFillsPK_Prescription_Id(anyLong());
    }

    @Test
    public void findByPrescriptionIdTest() {
        service.findByPrescriptionId(1L,new PageRequest(1,1));
        verify(fillsRepository).findByPrescriptionFillsPK_Prescription_Id(anyLong(), any(PageRequest.class));
    }
}