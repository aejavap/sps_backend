package lt.akademija.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.*;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = DiagnosisValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DiagnosisFormat {

    String message() default "Invalid diagnosis format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
