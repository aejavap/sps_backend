package lt.akademija.validators;

import java.util.Arrays;
import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lt.akademija.model.entity.users.Patient;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PatientValidator implements ConstraintValidator<PatientFormat, Object> {
	
	private static final Logger logger = LogManager.getLogger(PatientValidator.class);

    @Override
    public void initialize(PatientFormat patientFormat) {
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext ctx) {

        logger.info("PatientValidator called");
        /* Check if dob corresponds to pid */
        if (o instanceof Patient) {
            Patient patient = (Patient) o;
            char[] pidChars = patient.getPid().toString().toCharArray();

            char firstDigit = pidChars[0];
            StringBuilder sb = new StringBuilder();

            if (firstDigit == '1' || firstDigit == '2') {
                sb.append("18");
            } else if (firstDigit == '3' || firstDigit == '4') {
                sb.append("19");
            } else if (firstDigit == '5' || firstDigit == '6') {
                sb.append("20");
            }

            char[] datefromPid = Arrays.copyOfRange(pidChars, 1, 7);
            sb.append(datefromPid);
            String dateFromPid = sb.toString();

            sb.delete(0, sb.length());

            Calendar calDateOfBirth = Calendar.getInstance();
            calDateOfBirth.setTime(patient.getDob());
            sb.append(Integer.toString(calDateOfBirth.get(Calendar.YEAR)));
            int month = calDateOfBirth.get(Calendar.MONTH) + 1;
            if (month < 10) {
                sb.append("0");
                sb.append(Integer.toString(month));
            } else {
                sb.append(Integer.toString(month));
            }
            int day = calDateOfBirth.get(Calendar.DATE);

            if (day < 10) {
                sb.append("0");
                sb.append(Integer.toString(day));
            } else {
                sb.append(Integer.toString(day));
            }

            String dateOfBirth = sb.toString();

            boolean match = dateOfBirth.equals(dateFromPid);
            if (!match) {
                ctx.disableDefaultConstraintViolation();
                ctx.buildConstraintViolationWithTemplate("personal ID date of birth: " +patient.getPid()+"/n"+ "not corresponding to birth date: "+patient.getDob())
                        .addConstraintViolation();
            }
            return match;
        }
        return false;
    }
}
