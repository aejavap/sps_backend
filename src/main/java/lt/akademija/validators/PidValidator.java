package lt.akademija.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lt.akademija.validators.PidFormat;

public class PidValidator implements ConstraintValidator<PidFormat, Long> {
	
	private static final Logger logger = LogManager.getLogger(PidValidator.class);

	@Override
	public void initialize(PidFormat pidformat) {
	}

	@Override
	public boolean isValid(Long pid, ConstraintValidatorContext cxt) {
		logger.info("PidValidator called");
		
		StringBuilder sb = new StringBuilder();

		boolean pidCorrect = true;

		char[] pidChars = pid.toString().toCharArray();

		/* check if pid has 11 numbers */
		if (pidChars.length != 11) {
			pidCorrect = false;
		}

		/* check if pid 1 digit is from 1 to 6 */

		int firstDigit = Character.getNumericValue(pidChars[0]);

		if (firstDigit < 1 || firstDigit > 6) {
			pidCorrect = false;
		}

		/* check if pid last digit corresponds to formula */

		char lastDigit = '\0';
		int sum = 0;

		for (int i = 0; i < 10; i++) {
			if (i < 9) {
				sum += (Character.getNumericValue(pidChars[i])) * (i + 1);
			} else {
				sum += (Character.getNumericValue(pidChars[i])) * 1;
			}
		}

		if (sum % 11 != 10) {
			lastDigit = (Integer.toString(sum % 11)).charAt(0);
		} else {
			sum = 0;
			for (int i = 0; i < 10; i++) {
				if (i < 7) {
					sum += (Character.getNumericValue(pidChars[i])) * (i + 3);
				} else {
					sum += (Character.getNumericValue(pidChars[i])) * (i - 6);
				}
			}
			if (sum % 11 != 10) {
				lastDigit = Integer.toString(sum % 11).charAt(0);
			} else {
				lastDigit = '0';
			}
		}

		if (lastDigit != pidChars[10]) {
			pidCorrect = false;
		}

		/* check if month is from 01 to 12 */
		if (pidChars[3] != '0') {
			sb.append(pidChars[3]);
		}
		sb.append(pidChars[4]);
		int month = Integer.parseInt(sb.toString());

		if (month < 1 || month > 12) {
			pidCorrect = false;
		}

		/* check if day is from 01 to 31 */

		sb.delete(0, 2);
		if (pidChars[5] != '0') {
			sb.append(pidChars[5]);
		}
		sb.append(pidChars[6]);
		int day = Integer.parseInt(sb.toString());

		if (day < 1 || day > 31) {
			pidCorrect = false;
		}

		return pidCorrect;
	}

}
