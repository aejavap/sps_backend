package lt.akademija.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.*;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PidValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface PidFormat {

	String message() default "Invalid personal Id format";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
