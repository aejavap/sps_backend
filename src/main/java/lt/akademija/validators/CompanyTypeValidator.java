package lt.akademija.validators;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CompanyTypeValidator implements ConstraintValidator<CompanyTypeFormat, String> {

	private static final Logger logger = LogManager.getLogger(CompanyTypeValidator.class);

	@Override
	public void initialize(CompanyTypeFormat companyTypeFormat) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		logger.info("CompanyTypeValidator called");
		if (value == null || value.equals("")) {
			return false;
		}

		if (Arrays.asList("VšĮ", "UAB", "AB", "MB").contains(value)) {
			return true;
		} else {
			return false;
		}
	}

}
