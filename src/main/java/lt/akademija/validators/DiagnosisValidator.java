package lt.akademija.validators;

import lt.akademija.model.entity.simple.Diagnosis;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DiagnosisValidator implements ConstraintValidator<DiagnosisFormat, Object> {

	private static final Logger logger = LogManager.getLogger(DiagnosisValidator.class);

	@Override
	public void initialize(DiagnosisFormat diagnosisFormat) {

	}

	@Override
	public boolean isValid(Object o, ConstraintValidatorContext ctx) {
		logger.info("DiagnosisValidator called");
		boolean match = true;
		if (o instanceof Diagnosis) {
			Diagnosis diagnosis = (Diagnosis) o;
			if (diagnosis.getTitle().equals("000")) {
				return true;
			}
			char[] title = diagnosis.getTitle().trim().toCharArray();

			if (!(Character.isLetter(title[0]) && Character.isUpperCase(title[0]))) {
				match = false;
			}
			if (!(Character.isDigit(title[1]) && Character.isDigit(title[2]))) {
				match = false;
			}

			if (title.length >= 3) {
				if (title[3] != '.') {
					match = false;
				}
				for (int i = 4; i < title.length; i++) {
					if (!Character.isDigit(title[i]))
						match = false;
				}

				if (!match) {
					ctx.disableDefaultConstraintViolation();
					ctx.buildConstraintViolationWithTemplate("Diagnosis title form: " + diagnosis.getTitle()
							+ " is not correct. It should be of the form ^[A-Z][0-9][0-9].*").addConstraintViolation();
				}
			}
		}
		return match;
	}
}
