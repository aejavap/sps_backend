package lt.akademija.validators;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IngredientUnitsValidator implements ConstraintValidator<IngredientUnitsFormat, String> {

	private static final Logger logger = LogManager.getLogger(IngredientUnitsValidator.class);

	@Override
	public void initialize(IngredientUnitsFormat constraintAnnotation) {}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		logger.info("IngredientUnitsValidator called");
		if (value == null || value.equals("")) {
			return false;
		}

		if (Arrays.asList("mg", "µg", "TV/IU").contains(value)) {
			return true;
		} else {
			return false;
		}
	}

}
