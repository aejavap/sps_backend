package lt.akademija.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.*;

import javax.validation.Constraint;
import javax.validation.Payload;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;

@Documented
@Constraint(validatedBy = PatientValidator.class)
@Target({TYPE , ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface PatientFormat {

    String message() default "Invalid patient dob";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
