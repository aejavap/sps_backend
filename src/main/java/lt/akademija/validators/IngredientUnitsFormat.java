package lt.akademija.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = IngredientUnitsValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface IngredientUnitsFormat {
	
	String message() default "Invalid ingredient units. Valid units: mg, µg, TV/IU";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
