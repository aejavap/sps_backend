package lt.akademija.model.dto.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
// might have extension classes if we decide to introduce more user-type
// specific searches
// (e.g. by DoB / specialization id / number of patients / etc)
public class BasicUserSearchDTO {
	private String firstName;
	private String lastName;
	private Long pid;
}
