package lt.akademija.model.dto.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientSearchDTO extends BasicUserSearchDTO {
	private String diagnosisTitle;
	private Long doctorId;
}
