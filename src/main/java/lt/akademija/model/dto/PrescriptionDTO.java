package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.*;

import lombok.*;
import lt.akademija.model.dto.interfaces.PrescriptionDTOInterface;
import lt.akademija.validators.IngredientUnitsFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.Future;
import java.util.Date;

@Data
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
//@JsonIgnoreProperties(value = {"prescriptionId"/*, "activeIngredientTitle", "doctorFirstName","doctorLastName","prescriptionDate", "fillsNo"*/}, allowGetters = true)
public class PrescriptionDTO implements PrescriptionDTOInterface {
	private static final Logger logger = LogManager.getLogger(PrescriptionDTO.class);
	@JsonIgnore
	private Long prescriptionId;
	private Long activeIngredientId;
	private String activeIngredientTitle;
	private Double activeIngredientPerDose;
	@IngredientUnitsFormat
	private String activeIngredientUnits;
	private String dosageNotes;
	@JsonIgnore
	private Long doctorId;
	@JsonIgnore
	private String doctorFirstName;
	@JsonIgnore
	private String doctorLastName;

	private Long patientPid;
	private Long patientId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
	@JsonIgnore
	private Date prescriptionDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
	@Future
	private Date validUntil;
	@JsonIgnore
	private Long fillsNo;

	public static PrescriptionDTO toDTO(PrescriptionDTOInterface prescriptionDTOInterface) {
		logger.debug("PrescriptionDTO toDTO invoked");
		PrescriptionDTO prescription = new PrescriptionDTO();
		System.out.println("DTO constructor called");
		BeanUtils.copyProperties(prescriptionDTOInterface, prescription);
		logger.debug("PrescriptionDTO created " + prescription);
		return prescription;
	}

	@JsonProperty
	public Long getPrescriptionId() {
		return prescriptionId;
	}

	@JsonIgnore
	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	@JsonProperty
	public Long getDoctorId() {
		return doctorId;
	}

	@JsonIgnore
	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	@JsonProperty
	public String getDoctorFirstName() {
		return doctorFirstName;
	}

	@JsonIgnore
	public void setDoctorFirstName(String doctorFirstName) {
		this.doctorFirstName = doctorFirstName;
	}

	@JsonProperty
	public String getDoctorLastName() {
		return doctorLastName;
	}

	@JsonIgnore
	public void setDoctorLastName(String doctorLastName) {
		this.doctorLastName = doctorLastName;
	}

	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
	public Date getPrescriptionDate() {
		return prescriptionDate;
	}

	@JsonIgnore
	public void setPrescriptionDate(Date prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}

	@JsonProperty
	public Long getFillsNo() {
		return fillsNo;
	}

	@JsonIgnore
	public void setFillsNo(Long fillsNo) {
		this.fillsNo = fillsNo;
	}

	@JsonProperty
	public String getActiveIngredientTitle() {
		return activeIngredientTitle;
	}

	@JsonIgnore
	public void setActiveIngredientTitle(String activeIngredientName) {
		this.activeIngredientTitle = activeIngredientName;
	}

}
