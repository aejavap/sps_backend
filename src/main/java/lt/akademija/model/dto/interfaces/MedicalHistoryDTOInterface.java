package lt.akademija.model.dto.interfaces;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface MedicalHistoryDTOInterface {
	@Value("#{target.medicalHistoryPK.patient.id}")
	Long getPatientId();

	@Value("#{target.medicalHistoryPK.patient.pid}")
	Long getPatientPid();

	@Value("#{target.medicalHistoryPK.doctor.firstName}")
	String getDoctorFirstName();

	@Value("#{target.medicalHistoryPK.doctor.lastName}")
	String getDoctorLastName();

	@Value("#{target.medicalHistoryPK.timestamp}")
	Date getDate();

	@Value("#{target.medicalHistoryPK.diagnosis.id}")
	Long getDiagnosisId();

	@Value("#{target.medicalHistoryPK.diagnosis.description}")
	String getDiagnosisDescription();

	@Value("#{target.medicalHistoryPK.diagnosis.title}")
	String getDiagnosisTitle();

	String getNotes();

	Integer getAppointmentLength();

	Boolean getCompensated();

	@Value("#{target.repeatVisit}")
	Boolean getRepeatVisitation();
}
