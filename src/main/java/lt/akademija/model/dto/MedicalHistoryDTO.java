package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.dto.interfaces.MedicalHistoryDTOInterface;

import lt.akademija.validators.DiagnosisFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * Medical history data transfer object. Used to project needed information to endpoints. As well as, to transfer data from endpoint to services.
 */
@Data
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicalHistoryDTO implements MedicalHistoryDTOInterface {

	private static final Logger logger = LogManager.getLogger(MedicalHistoryDTO.class);

	private Long patientId;
	private Long patientPid;
	@JsonIgnore
	private Long doctorId;
	@JsonIgnore
	private String doctorFirstName;
	@JsonIgnore
	private String doctorLastName;
	@JsonIgnore
	private Date date;
	private Long diagnosisId;
	@DiagnosisFormat
	private String diagnosisTitle;
	private String diagnosisDescription;
	private String notes;
	private Integer appointmentLength;
	private Boolean compensated;
	private Boolean repeatVisitation;

	public static MedicalHistoryDTO toDTO(MedicalHistoryDTOInterface historyDTOInterface) {
		logger.info("MedicalHistoryDTO toDTO invoked");
		MedicalHistoryDTO historyDTO = new MedicalHistoryDTO();
		logger.debug("Medical history DTO constructor called");
		BeanUtils.copyProperties(historyDTOInterface, historyDTO);
		logger.debug("After medical history DTO constructor");
		logger.info("Medical history dto created : " + historyDTO);
		return historyDTO;
	}

	@JsonProperty
	public Long getDoctorId() {
		return doctorId;
	}

	@JsonIgnore
	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	@JsonProperty
	public String getDoctorFirstName() {
		return doctorFirstName;
	}

	@JsonIgnore
	public void setDoctorFirstName(String doctorFirstName) {
		this.doctorFirstName = doctorFirstName;
	}

	@JsonProperty
	public String getDoctorLastName() {
		return doctorLastName;
	}

	@JsonIgnore
	public void setDoctorLastName(String doctorLastName) {
		this.doctorLastName = doctorLastName;
	}

	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
	public Date getDate() {
		return date;
	}

	@JsonIgnore
	public void setDate(Date date) {
		this.date = date;
	}

	@JsonProperty
	public Long getDiagnosisId() {
		return diagnosisId;
	}

	@JsonIgnore
	public void setDiagnosisId(Long diagnosisId) {
		this.diagnosisId = diagnosisId;
	}

	@JsonProperty
	public String getDiagnosisDescription() {
		return diagnosisDescription;
	}

	@JsonIgnore
	public void setDiagnosisDescription(String diagnosisDescription) {
		this.diagnosisDescription = diagnosisDescription;
	}

	@JsonProperty
	public String getDiagnosisTitle() {
		return this.diagnosisTitle;
	}

	@JsonIgnore
	public void setDiagnosisTitle(String titleGiven) {
		this.diagnosisTitle = titleGiven;
	}

}
