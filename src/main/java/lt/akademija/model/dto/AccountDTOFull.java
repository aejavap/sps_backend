package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lt.akademija.model.entity.Account;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Getter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
/**
 * More elaborate version of AccountDTO, used to present account data on the
 * frontend side
 *
 */
public class AccountDTOFull extends AccountDTO {

	private static final Logger logger = LogManager.getLogger(AccountDTOFull.class);

	private List<String> roles;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss", timezone = "Europe/Vilnius", locale = "lt")
	private Date createdOn;
	private boolean enabled;
	private String password; // never send to frontend - for password udptate as admin puposes only

	@JsonCreator
	public AccountDTOFull(@JsonProperty("username") String username, @JsonProperty("roles") List<String> roles,
			@JsonProperty("createdOn") Date createdOn, @JsonProperty("enabled") boolean enabled) {
		super(username, null);
		System.out.println("Non default account constructor called");
		this.roles = roles;
		this.createdOn = createdOn;
		this.enabled = enabled;
	}

	/**
	 * Converts an Account entity to AccountDTOFull
	 * @param entity
	 * @return
	 */
	public static AccountDTOFull toDTO(Account entity) {
		logger.debug("AccountDTOFull toDTO invoked");
		AccountDTOFull account = new AccountDTOFull(entity.getUsername(), entity.getRoles(), entity.getCreatedOn(),
				entity.getEnabled());
		logger.debug("AccountFullDTO created " + account);
		return account;
	}

}
