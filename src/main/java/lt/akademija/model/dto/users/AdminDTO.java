package lt.akademija.model.dto.users;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.users.Admin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@EqualsAndHashCode(callSuper = false)
@Data
public class AdminDTO extends AbstractUserDTO {
	private static final long serialVersionUID = 6177268051187130309L;

	private static Logger logger = LogManager.getLogger(AdminDTO.class);
	public static AdminDTO toDTO(Admin admin) {
		AdminDTO adminDTO = AbstractUserDTO.toDTO(admin, AdminDTO.class);
		logger.debug("AdminDTO; " + admin);
		return adminDTO;
	}

}
