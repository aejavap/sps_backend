package lt.akademija.model.dto.users;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.simple.Specialization;
import lt.akademija.model.entity.users.Doctor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@EqualsAndHashCode(callSuper = false)
@Data
public class DoctorDTO extends AbstractUserDTO {

	private static final long serialVersionUID = 820089912158769623L;

	private static Logger logger = LogManager.getLogger(DoctorDTO.class);

	private Long specializationId;
	private String specializationTitle;

	public static DoctorDTO toDTO(Doctor doctor) {
		logger.debug("DTO constructor in DoctorDTO called");
		DoctorDTO doctorDTO = AbstractUserDTO.toDTO(doctor, DoctorDTO.class);
		if (doctor.getSpecialization() != null)
			logger.info("Specialization title: " + doctor.getSpecialization().getTitle());
		doctorDTO.setSpecialization(doctor.getSpecialization());
		return doctorDTO;
	}

	public void setSpecialization(Specialization specialization) {
		if (specialization != null) {
			this.specializationId = specialization.getId();
			this.specializationTitle = specialization.getTitle();
		}
	}

}
