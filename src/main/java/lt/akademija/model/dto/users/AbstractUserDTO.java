package lt.akademija.model.dto.users;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import lt.akademija.validators.PidFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "user")
@JsonSubTypes({ @JsonSubTypes.Type(value = DoctorDTO.class, name = "doctor"),
		@JsonSubTypes.Type(value = PharmacistDTO.class, name = "pharmacist"),
		@JsonSubTypes.Type(value = PatientDTO.class, name = "patient"),
		@JsonSubTypes.Type(value = AdminDTO.class, name = "admin") })
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractUserDTO implements Serializable {

	private static Logger logger = LogManager.getLogger(AbstractUserDTO.class);

	private static final long serialVersionUID = 4114624687002297493L;
	private Long id;
	private String firstName;
	private String lastName;
	@PidFormat
	private Long pid;

	public static <T extends AbstractUserDTO, E> T toDTO(final E entity, Class<T> type){
		try {
			T dto = type.newInstance();
			logger.debug("DTO constructor in AbstractUserDTO called");
			BeanUtils.copyProperties(entity, dto);
			return dto;
		} catch (Exception e) {
			logger.error("Exception caught in static toDTO of AbstractUser \n"+e.getMessage());
		}
		return null;
	}
}
