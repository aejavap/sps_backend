package lt.akademija.model.dto.users;

import lombok.*;
import lt.akademija.model.entity.users.Pharmacist;
import lt.akademija.validators.CompanyTypeFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

@EqualsAndHashCode(callSuper = false)
@JsonDeserialize(as = PharmacistDTO.class)
@Data
public class PharmacistDTO extends AbstractUserDTO {
	private static final long serialVersionUID = -5489678369150286711L;
	private static Logger logger = LogManager.getLogger(AbstractUserDTO.class);

	public static PharmacistDTO toDTO(Pharmacist pharmacist) {
		logger.debug("DTO constructor called");
		PharmacistDTO pharmacistDTO = AbstractUserDTO.toDTO(pharmacist, PharmacistDTO.class);
		pharmacistDTO.setCompanyType(pharmacist.getCompanyType());
		pharmacistDTO.setCompanyName(pharmacist.getCompanyName());
		return pharmacistDTO;
	}

	@CompanyTypeFormat
	private String companyType;
	private String companyName;

}
