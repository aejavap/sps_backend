package lt.akademija.model.dto.users;

import java.util.Date;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import lt.akademija.model.entity.users.*;
import lt.akademija.validators.PatientFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@EqualsAndHashCode(callSuper = false)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatientDTO extends AbstractUserDTO {
	private static final long serialVersionUID = -8308941776835177528L;
	private static Logger logger = LogManager.getLogger(AbstractUserDTO.class);

	public static PatientDTO toDTO(Patient patient) {
		logger.debug("DTO in PatientDTO constructor called");
		PatientDTO patientDTO = AbstractUserDTO.toDTO(patient, PatientDTO.class);
		patientDTO.setDob(patient.getDob());
		if (patient.getDoctor() != null) {
			logger.debug("Doctor not null");
			patientDTO.setDoctorId(patient.getDoctor().getId());
		}
		return patientDTO;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date dob;
	private Long doctorId;
	private Boolean hasAccount;
	private Long doctorPid;

	// This setter tricks BeanUtils still to copy doctor id if it exists;
	public void setDoctor(Doctor doctor) {
		if (doctor != null) {
			this.doctorId = doctor.getId();
			this.doctorPid = doctor.getPid();
		}
	}
}
