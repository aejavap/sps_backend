package lt.akademija.model.dto.simple;

import lombok.*;

@Data
public class IngredientDTO {

	private Long id;
	private String title;

}
