package lt.akademija.model.dto.simple;

import lombok.Data;
import lt.akademija.validators.DiagnosisFormat;

@Data
public class DiagnosisDTO {

	@DiagnosisFormat
	private String title;

	private String description;

}
