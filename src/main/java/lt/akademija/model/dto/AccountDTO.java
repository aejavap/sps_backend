package lt.akademija.model.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
/**
 * Lean DTO used when creating/updating accounts
 *
 */
public class AccountDTO {

	private String username;
	private String password;

	protected AccountDTO(String username, String password) {
		this.username = username;
		this.password = password;
	}

}
