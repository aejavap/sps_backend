package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lt.akademija.model.dto.interfaces.PrescriptionFillsDTOInterface;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
public class PrescriptionFillsDTO implements PrescriptionFillsDTOInterface{
	private static final Logger logger = LogManager.getLogger(PrescriptionFillsDTO.class);
	@JsonIgnore
	private Date date;

	private Long prescriptionId;
	@JsonIgnore
	private Long phramacistId;
	@JsonIgnore
	private String phFirstName;
	@JsonIgnore
	private String phLastName;

	public static PrescriptionFillsDTO toDTO(PrescriptionFillsDTOInterface prescriptionFillsDTOInterface) {
		logger.debug("PrescriptionFillsDTO toDTO invoked");
		PrescriptionFillsDTO prescriptionFills = new PrescriptionFillsDTO();
		System.out.println("DTO constructor called");
		BeanUtils.copyProperties(prescriptionFillsDTOInterface, prescriptionFills);
		logger.debug("PrescriptionFillsDTO created " + prescriptionFills);
		return prescriptionFills;
	}

	@JsonIgnore
	public Long getPhramacistId() {
		return phramacistId;
	}

	@JsonProperty
	public void setPhramacistId(Long phramacistId) {
		this.phramacistId = phramacistId;
	}

	@JsonProperty
	public String getPhFirstName() {
		return phFirstName;
	}

	@JsonIgnore
	public void setPhFirstName(String phFirstName) {
		this.phFirstName = phFirstName;
	}

	@JsonProperty
	public String getPhLastName() {
		return phLastName;
	}

	@JsonIgnore
	public void setPhLastName(String phLastName) {
		this.phLastName = phLastName;
	}

	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
	public Date getDate() {
		return date;
	}

	@JsonIgnore
	public void setDate(Date date) {
		this.date = date;
	}
}
