package lt.akademija.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.users.Pharmacist;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode
@Data
@Embeddable
/**
 * Entity representing the composite primary key of Prescription entity.
 *
 */
public class PrescriptionFillsPK implements Serializable {

	private static final long serialVersionUID = -3110261411467649038L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE", insertable = false, updatable = false, nullable = false)
	private Date timestamp;

	// Foreign key
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "PHARMACIST_ID", referencedColumnName = "id", insertable = false, updatable = false, nullable = false)
	private Pharmacist pharmacist;

	// Foreign key
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "PRESCRIPTION_ID", referencedColumnName = "id", insertable = false, updatable = false, nullable = false)
	private Prescription prescription;

	// Default constructor needed
	public PrescriptionFillsPK() {
	}

	public PrescriptionFillsPK(Date date, Pharmacist pharmacist, Prescription prescription) {
		this.pharmacist = pharmacist;
		this.prescription = prescription;
		this.timestamp = new Date();
	}
}
