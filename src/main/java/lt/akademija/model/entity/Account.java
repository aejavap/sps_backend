package lt.akademija.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.users.AbstractUser;

@Entity
@EqualsAndHashCode(exclude = "abstractUser")
@Data
@Table(name = "ACCOUNTS")
/**
 * User account entity. Needed for authentication.
 *
 */
public class Account implements UserDetails {

	private static final long serialVersionUID = 5891741116301575767L;

	public Account() {
		this.accountNonExpired = true;
		this.accountNonLocked = true;
		this.credentialsNonExpired = true;
		this.enabled = true;
	}

	public Account(AbstractUser abstractUserGiven) {
		this.accountNonExpired = true;
		this.accountNonLocked = true;
		this.credentialsNonExpired = true;
		this.enabled = true;
		this.abstractUser = abstractUserGiven;
	}

	@Id
	private Long id;

	/**
	 * User id and account id are shared.
	 */
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@MapsId
	@JoinColumn(name = "USER_ID")
	@JsonIgnore
	private AbstractUser abstractUser;

	@Column(name = "USERNAME", nullable = false, unique = true)
	private String username;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name = "ACCOUNT_ROLES", nullable = false)
	private List<String> roles;

	@Transient // not used but needed
	private boolean accountNonExpired;

	@Transient // not used but needed
	private boolean accountNonLocked;

	@Transient // not used but needed
	private boolean credentialsNonExpired;

	/**
	 * Allows/disallows user to authenticate
	 */
	@Column(name = "ACCOUNT_ENABLED")
	private boolean enabled;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "CREATED_ON")
	private Date createdOn;

	/**
	 * Method adding an authority to to an existing account. Only used in admin
	 * facade when creating new users.
	 */
	public void grantAuthority(String authority) {
		if (roles == null)
			roles = new ArrayList<>();
		roles.add(authority);
	}

	@Override
	public List<GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<>();
		roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getRoles() {
		return roles;
	}

	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public AbstractUser getAbstractUser() {
		return this.abstractUser;
	}

	public void setAbstractUser(AbstractUser abstractUserGiven) {
		this.abstractUser = abstractUserGiven;
	}

}
