package lt.akademija.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.users.Pharmacist;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode
@Entity
@Table(name = "PRESCRIPTION_FILLS")
/**
 * Entity representing prescription filling events
 *
 */
public class PrescriptionFills implements Serializable {

	private static final long serialVersionUID = -4013214598771256155L;
	@EmbeddedId
	private PrescriptionFillsPK prescriptionFillsPK;

	// Needed
	public PrescriptionFills() {
	}

	// To synchronize bidirectional relationship fill must always be created like
	// this to set primary key correctly
	@PersistenceConstructor
	public PrescriptionFills(Date date, Pharmacist pharmacist, Prescription prescription) {
		this.prescriptionFillsPK = new PrescriptionFillsPK(date, pharmacist, prescription);
	}

	public PrescriptionFillsPK getPrescriptionFillsPK() {
		return prescriptionFillsPK;
	}

	public void setPrescriptionFillsPK(PrescriptionFillsPK prescriptionFillsPK) {
		this.prescriptionFillsPK = prescriptionFillsPK;
	}
}
