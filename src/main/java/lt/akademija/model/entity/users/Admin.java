package lt.akademija.model.entity.users;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.users.AbstractUser;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("ADMIN")
@Table(name = "ADMINS")
@EqualsAndHashCode(callSuper = false)
@Data
/**
 * Admin user entity extending AbstractUser
 *
 */
public class Admin extends AbstractUser {

}
