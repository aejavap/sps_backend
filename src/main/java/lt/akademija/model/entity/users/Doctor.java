package lt.akademija.model.entity.users;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lt.akademija.model.entity.*;
import lt.akademija.model.entity.simple.Specialization;

import java.util.List;

@Entity
@DiscriminatorValue("DOCTOR")
@Table(name = "DOCTORS")
@Getter
@Setter
/**
 * Doctor user entity extending AbstractUser
 *
 */
public class Doctor extends AbstractUser {

	// Unidirectional relation
	// https://stackoverflow.com/questions/24994440/no-serializer-found-for-class-org-hibernate-proxy-pojo-javassist-javassist
	// Serialization made earlier than lazyFetch
	// a) do not lazyFetch(eagerFetch)
	// b) remove from JSON(means JsonIgnore it)
	// @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SPECIALIZATION_ID", nullable = true)
	@JsonIgnore
	private Specialization specialization;

	@OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	// @JsonIgnore
	private List<Patient> patientList;

	/**
	 * Prescriptions made by this doctor.
	 */
	@OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JsonIgnore
	private List<Prescription> prescriptions;

	/**
	 * Medical records made by this doctor.
	 */
	@OneToMany(mappedBy = "medicalHistoryPK.doctor", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<MedicalHistory> medicalHistories;

	// ******************METHODS***************

	/**
	 * Synchronizes patient attachment.. Relation controlled by Doctor entity.
	 * 
	 * @param patient
	 */
	public void addPatient(Patient patient) {

		patientList.add(patient);
		patient.setDoctor(this);
	}

	/**
	 * Synchronizes patient removal. Relation controlled by Doctor entity.
	 * 
	 * @param patient
	 */
	public void removePatient(Patient patient) {
		patientList.remove(patient);
		patient.setDoctor(null);
	}

	// Do not erase
	public Long getSpecializationId() {
		if (specialization != null)
			return specialization.getId();
		return null;
	}

	public void addPrescription(Prescription prescription) {
		this.prescriptions.add(prescription);
	}

}
