package lt.akademija.model.entity.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.model.entity.Prescription;
import lt.akademija.validators.PatientFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("PATIENT")
@Table(name = "PATIENTS")
@Getter
@Setter
@PatientFormat
/**
 * Patient user entity extending AbstractUser
 *
 */
public class Patient extends AbstractUser {

	@Column(name = "DATE_OF_BIRTH", nullable = false)
	@Temporal(TemporalType.DATE)
	@NotNull
	@Past
	private Date dob;

	@JsonIgnore
	@ManyToOne(optional = true, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinColumn(name = "DOCTOR_ID", nullable = true)
	private Doctor doctor;

	@JsonIgnore
	@OneToMany(mappedBy = "medicalHistoryPK.patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<MedicalHistory> medicalHistories;

	@JsonIgnore
	@OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Prescription> prescriptions;

	// ****************METHODS*****************

	// Add new medical record
	public void addMedicalHistory(MedicalHistory medicalHistory) {
		medicalHistories.add(medicalHistory);
	}

	public void addPrescription(Prescription prescription) {
		this.prescriptions.add(prescription);
	}

	@Override
	public String toString() {
		return "patient doctor: " + doctor;
	}

}
