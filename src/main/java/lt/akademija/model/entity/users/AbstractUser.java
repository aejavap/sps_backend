package lt.akademija.model.entity.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.*;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "TYPE_DISCRIMINATOR", discriminatorType = DiscriminatorType.STRING)
@Data
@EqualsAndHashCode
/**
 * Base abstract user class including fields/methods shared by all users
 *
 */
public abstract class AbstractUser {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "abstractUser", fetch = FetchType.LAZY)
	@JsonIgnore
	private Account account;

	@Column(name = "PERSONAL_ID", nullable = false, unique = true)
	private Long pid;

	@Column(name = "FIRST_NAME", nullable = false)
	private String firstName;

	@Column(name = "LAST_NAME", nullable = false)
	private String lastName;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "AbstractUser{" + "id=" + id + ", pid=" + pid + ", firstName='" + firstName + '\'' + ", lastName='"
				+ lastName + '\'' + '}';
	}
}
