package lt.akademija.model.entity.simple;

import javax.persistence.*;

import lombok.*;

@Table(name = "DIAGNOSES")
@EqualsAndHashCode(callSuper = false)
@Entity
@Data
/**
 * Simple entity Diagnosis extending AbstractSimpleEntity
 *
 */
public class Diagnosis extends AbstractSimpleEntity {
	private static final long serialVersionUID = 2491478342621682047L;
	@Column(name = "DESCRIPTION", length = 8000)
	private String description;

	@Override
	public String toString() {
		return "Diagnosis{" + "id='" + super.getId() + '\'' + "title='" + super.getTitle() + '\'' + ", description='"
				+ description + '\'' + '}';
	}
}
