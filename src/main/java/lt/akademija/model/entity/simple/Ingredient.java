package lt.akademija.model.entity.simple;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name = "INGREDIENTS")
@EqualsAndHashCode(callSuper = false)
@Entity
/**
 * Simple entity Ingredient extending AbstractSimpleEntity
 *
 */
public class Ingredient extends AbstractSimpleEntity {
	private static final long serialVersionUID = 8126225357499871810L;

}
