package lt.akademija.model.entity.simple;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "SPECIALIZATIONS")
@EqualsAndHashCode(callSuper = false)
@Data
/**
 * Simple entity Specialization extending AbstractSimpleEntity
 *
 */
public class Specialization extends AbstractSimpleEntity {
	private static final long serialVersionUID = -3902909108543632553L;
	@Column(name = "DESCRIPTION", length = 8000)
	private String description;

	@Override
	public String toString() {
		return "Specialization{" + "id='" + super.getId() + '\'' + "title='" + super.getTitle() + '\''
				+ ", description='" + description + '\'' + '}';
	}
}
