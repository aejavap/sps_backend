package lt.akademija.model.entity.simple;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "entityType")
@JsonSubTypes({ @JsonSubTypes.Type(value = Diagnosis.class, name = "diagnosis"),
		@JsonSubTypes.Type(value = Specialization.class, name = "specialization"),
		@JsonSubTypes.Type(value = Ingredient.class, name = "ingredient") })
@EqualsAndHashCode
@MappedSuperclass
/**
 * Base entity to be extended by simple entities
 * (diagnoses/specialization/ingredients). Includes fields shared by all simple
 * entities
 *
 */
public class AbstractSimpleEntity implements Serializable {

	private static final long serialVersionUID = -3971101658529158358L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "TITLE", length = 2048, nullable = false, unique = true)
	private String title;

}
