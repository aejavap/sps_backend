package lt.akademija.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.users.*;

import lombok.Data;

@Embeddable
@EqualsAndHashCode
@Data
/**
 * Entity representing the composite primary key of medical records/history
 * entity
 *
 */
public class MedicalHistoryPK implements Serializable {

	private static final long serialVersionUID = 51089092844772623L;

	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "PATIENT_ID", insertable = false, updatable = false, nullable = false)
	private Patient patient;

	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "DIAGNOSIS_ID", insertable = false, updatable = false, nullable = false)
	private Diagnosis diagnosis;

	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "DOCTOR_ID", insertable = false, updatable = false, nullable = false)
	private Doctor doctor;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE", insertable = false, updatable = false, nullable = false)
	private Date timestamp;

	// Default constructor needed
	public MedicalHistoryPK() {
	}

	public MedicalHistoryPK(Patient patient, Doctor doctor, Diagnosis diagnosis) {
		this.patient = patient;
		this.doctor = doctor;
		this.diagnosis = diagnosis;
		// date set on cretion
		this.timestamp = new Date();
	}

}
