package lt.akademija.controller.simple;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lt.akademija.service.simple.GenericSimpleService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Abstract controller extended by all simple entity related (diagnoses/ingredients/specialization) controllers.
 * User only to retrieve data from the server.
 */
abstract public class GenericSimpleEntityController<T> {

	private static final Logger logger = LogManager.getLogger(GenericSimpleEntityController.class);

	@Autowired
	protected GenericSimpleService<T> service;

	@ApiOperation(value = "Get all", notes = "Returns list of objects")
	@GetMapping(value = "/all")

	// Just for Swagger
	// https://stackoverflow.com/questions/35404329/swagger-documentation-for-spring-pageable-interface?rq=1
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	@ResponseStatus(value = HttpStatus.OK)
	public Page<T> objectList(Pageable pageRequest) {
		System.out.println("Generic object all called");
		return service.findAll(pageRequest);
	}

	@ApiOperation(value = "Find one by id", notes = "Returns one object by id")
	@GetMapping(value = "/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public T findById(@PathVariable final Long id) {
		return (T) service.findById(id);
	}

	@ApiOperation(value = "Sesarch entities by title", notes = "Returns one object by id")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping(value = "/search/by-title")
	public Page<T> findByTitle(@RequestParam final String title, Pageable pageRequest) {
		logger.info("Attempting to search entity by title " + title);
		return service.searchByTitle(title, pageRequest);
	}

}
