package lt.akademija.controller.simple;

import io.swagger.annotations.Api;
import lt.akademija.model.entity.simple.Ingredient;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "Ingredients")
@RequestMapping(value = "api/ingredients/")
/**
 * Controller used to retrieve data re: ingredients
 */
public class IngredientController extends GenericSimpleEntityController<Ingredient> {
}
