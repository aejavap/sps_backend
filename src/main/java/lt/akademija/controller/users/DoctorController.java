package lt.akademija.controller.users;

import io.swagger.annotations.*;
import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.dto.MedicalHistoryPkDTO;
import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.dto.search.PatientSearchDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.service.AccountService;
import lt.akademija.service.MedicalHistoryService;
import lt.akademija.service.PrescriptionFillsService;
import lt.akademija.service.PrescriptionService;
import lt.akademija.service.stat.DoctorStatService;
import lt.akademija.service.users.PatientService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@Api(value = "doctor")
@RequestMapping(value = "api/doctor/")
/**
 * Controller for use by doctors
 *
 */
public class DoctorController {

	private static final Logger logger = LogManager.getLogger(DoctorController.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private PrescriptionService prescriptionService;

	@Autowired
	private PrescriptionFillsService prescriptionFillsService;

	@Autowired
	private MedicalHistoryService medicalHistoryService;

	@Autowired
	private DoctorStatService doctorStatService;

	// #####################################################
	// ############## SEARCH ENDPOINTS ##########################
	// #####################################################

	@ApiOperation(value = "Searches all patients that are patients of an authenticated doctor")
	@PostMapping(value = "/patient/search/")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	@ResponseStatus(value = HttpStatus.OK)
	public Page<PatientDTO> searchUsersByNameAndSurnameAndPidAndDiagnosis (@RequestBody final PatientSearchDTO searchDTO,
			Pageable pageRequest) throws Exception{
		Long doctorId = accountService.getContextUser().getId();
		searchDTO.setDoctorId(doctorId);
		logger.info("Doctor with id " + doctorId
				+ " is attempting to search his patients using these search parameters: " + searchDTO);
		return patientService.searchByNameAndSurnameAndPidAndDiagnosis(searchDTO, pageRequest);
	}

	// #####################################################
	// ############## GET ENDPOINTS - GET MULTIPLE OBJECTS #############
	// #####################################################

	@ApiOperation(value = "Get all patients at once.", notes = "Returns a a list of patients.")
	@GetMapping(value = "/patient/all")
	@ResponseStatus(value = HttpStatus.OK)
	public List<PatientDTO> getAllPatient() {
		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId + " is attempting to retrieve one's patient list.");
		return patientService.findPatientsByDoctorId(doctorId);
	}

	@ApiOperation(value = "View medical history of a particular patient of an authenticated doctor.")
	@GetMapping(value = "/patient/{patientPid}/medical-record/all")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	@ResponseStatus(value = HttpStatus.OK)
	public Page<MedicalHistoryDTO> findPatientMedicalHistoryByPatientPid(@PathVariable Long patientPid,
			Pageable pageRequest) throws Exception {

		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId + " is attempting to retrieve medical history of patient with pid "
				+ patientPid);
		PatientDTO patient = getPatienDTOtByPid(patientPid);
		checkDoctorPatientPermissions(patient, doctorId);
		return medicalHistoryService.findByPatientPid(patientPid, pageRequest);
	}

	@ApiOperation(value = "View prescription history of a particular patient of an authenticated doctor.")
	@GetMapping(value = "/patient/{patientPid}/prescription/all")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	@ResponseStatus(value = HttpStatus.OK)
	public Page<PrescriptionDTO> findPatientPrescriptionsByPatientPid(@PathVariable Long patientPid,
			Pageable pageRequest) throws Exception {
		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId
				+ " is attempting to retrieve prescription history of patient with pid " + patientPid);
		PatientDTO patient = getPatienDTOtByPid(patientPid);
		checkDoctorPatientPermissions(patient, doctorId);
		return prescriptionService.findAllByPatientPid(patientPid, pageRequest);
	}

	@ApiOperation(value = "Finds all prescription fills for patient")
	@GetMapping(value = "/prescriptionFills/{prescriptionId}/search/")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	@ResponseStatus(value = HttpStatus.OK)
	public Page<PrescriptionFillsDTO> findFills(@PathVariable("prescriptionId") final Long prescriptionId,
			Pageable pageRequest) throws Exception {
		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId
				+ " is attepting to view all prescription fills of prescription with id " + prescriptionId);
		PrescriptionDTO prescriptionFound = prescriptionService.findByPrescriptionId(prescriptionId);
		if (prescriptionFound == null) {
			throw new NullPointerException("No prescription with id " + prescriptionId + " exists.");
		} else {
			logger.debug("Prescription with id " + prescriptionId + " was found.");
		}
		PatientDTO patientFound = getPatienDTOtByPid(prescriptionFound.getPatientPid());
		checkDoctorPatientPermissions(patientFound, doctorId);
		return prescriptionFillsService.findByPrescriptionId(prescriptionId, pageRequest);
	}

	// #####################################################
	// ############## GET ENDPOINTS - GET SINGLE OBJECT ###############
	// #####################################################

	@ApiOperation(value = "Given a pid, returns patient's details.")
	@GetMapping(value = "/patient/{pid}")
	public PatientDTO findOnePatientByPid(@PathVariable Long pid) throws Exception {
		return patientService.findByPidDTO(pid);
	}

	@ApiOperation(value = " View particular medical record details of one of my patients", notes = "Any doctor/patient ids sent will be ignored")
	@PostMapping(value = "/patient/{patientPid}/medical-record")
	@ResponseStatus(value = HttpStatus.OK)
	public MedicalHistoryDTO findPatientMedicalRecordByPk(@PathVariable("patientPid") Long patientPid,
			@RequestBody MedicalHistoryPkDTO dto) throws Exception {
		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId + " is attempting to medical record with PK " + dto
				+ " of patient with pid " + patientPid);
		PatientDTO patient = getPatienDTOtByPid(patientPid);
		Long patientId = patient.getId();
		Long diagnosisId = dto.getDiagnosisId();
		Date date = dto.getDate();
		checkDoctorPatientPermissions(patient, doctorId);
		MedicalHistoryDTO recordFound = medicalHistoryService.findByPk(patientId, doctorId, diagnosisId, date);
		if (recordFound == null) {
			throw new NullPointerException("No medical record with PK " + dto + " was found.");
		} else {
			logger.debug("Medical record with PK " + dto + " was founnd.");
			return recordFound;
		}
	}

	@ApiOperation(value = " View particular prescription details of one of my patients")
	@GetMapping(value = "/patient/{patientPid}/prescription/{prescriptionId}")
	@ResponseStatus(value = HttpStatus.OK)
	public PrescriptionDTO findPatientPrescriptionByPatientPidAndPrescriptionId(
			@PathVariable("patientPid") Long patientPid, @PathVariable("prescriptionId") Long prescriptionId)
			throws Exception {
		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId + " is attempting to access prescription with id " + prescriptionId
				+ " belonging to patient with pid " + patientPid);
		PatientDTO patient = getPatienDTOtByPid(patientPid);
		checkDoctorPatientPermissions(patient, doctorId);
		return prescriptionService.findByPrescriptionId(prescriptionId);
	}

	// #####################################################
	// ############## CREATE ENDPOINTS ##########################
	// #####################################################

	@ApiOperation(value = "Create a new prescription", notes = "Creates a new prescription")
	@PostMapping(value = "/new/prescription")
	@ResponseStatus(value = HttpStatus.CREATED)
	public PrescriptionDTO createPrescription(@RequestBody @Valid final PrescriptionDTO entityDTO) throws Exception {
		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId + " is attempting to create a prescription from dto " + entityDTO);
		entityDTO.setDoctorId(doctorId);
		return prescriptionService.createFromDTO(entityDTO);
	}

	@ApiOperation(value = "Create a new medical record", notes = "Creates a new medicalHistory")
	@PostMapping(value = "/new/medical-record")
	@ResponseStatus(value = HttpStatus.CREATED)
	public MedicalHistoryDTO createMedicalRecord(@RequestBody @Valid final MedicalHistoryDTO entityDTO) throws Exception {
		Long doctorId = accountService.getContextUser().getId();
		logger.info("Doctor with id " + doctorId + " is attempting to create a medical record from dto " + entityDTO);
		entityDTO.setDoctorId(doctorId);
		return medicalHistoryService.createByDTO(entityDTO);
	}

	// #####################################################
	// ############## STATISTICS ENDPOINTS ########################
	// #####################################################

	@ApiOperation(value = "Returns doctor's medical records per day numbers over a given period of time.", notes = "Start date included, end date excluded")
	@GetMapping(value = "/stats/records")
	@ResponseStatus(value = HttpStatus.OK)
	public List<Long> getDoctorRecordStat(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
		Long doctorId = accountService.getContextUser().getId();
		return doctorStatService.getDoctorRecordsStat(doctorId, startDate, endDate);
	}

	@ApiOperation(value = "Returns doctor's medical records per day numbers over a given period of time.", notes = "Start date included, end date excluded")
	@GetMapping(value = "/stats/records/visit-time")
	@ResponseStatus(value = HttpStatus.OK)
	public List<Long> getVisitTimeStat(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
		Long doctorId = accountService.getContextUser().getId();
		return doctorStatService.getDoctorVisitTimeStat(doctorId, startDate, endDate);
	}

	// #####################################################
	// ############## HELPER METHODS ###########################
	// #####################################################

	/**
	 * Helper method to retrieve a patient by pid with logging and exceptions
	 *
	 * @param patientPid
	 * @return non-null patient
	 * @throws NullPointerException
	 *             - throws if patient not found
	 */
	public PatientDTO getPatienDTOtByPid(Long patientPid) throws Exception, NullPointerException {
		PatientDTO patient = patientService.findByPidDTO(patientPid);
		if (patient == null) {
			throw new NullPointerException("No patient with pid " + patientPid + " exists.");
		} else {
			logger.debug("Patient with pid " + patientPid + " was found.");
			return patient;
		}
	}

	/**
	 * Helper method to check if doctor has permission to view a particular
	 * patient's details. Throws an exception if not.
	 *
	 * @param patient
	 *            - takes-in PatientDTO
	 * @param doctorId
	 * @throws Exception
	 *             - thrown if doctor does not have access to patient
	 */
	public void checkDoctorPatientPermissions(PatientDTO patient, Long doctorId) throws Exception {
		if (patient.getDoctorId().equals(doctorId)) {
			logger.info(
					"Doctor with id " + doctorId + " has acces to view details of patient with id " + patient.getId());
		} else {
			throw new Exception("Unauthorized access:  doctor is not authorized to view details of patient with id: "
					+ patient.getId());
		}
	}

}
