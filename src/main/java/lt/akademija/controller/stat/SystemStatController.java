package lt.akademija.controller.stat;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lt.akademija.service.AccountService;
import lt.akademija.service.stat.SystemStatService;

@RestController
@Api(value = "api/stat/")
/**
 * Controller for system-wide statistics
 *
 */
public class SystemStatController {

	@Autowired
	SystemStatService statService;

	@Autowired
	AccountService accountService;

	private static final Logger logger = LogManager.getLogger(SystemStatController.class);

	@ApiOperation(value = "Get a map <String title, Long Count> of top diagnoses with diagnoses count in the given time period", notes = "Start date included, end date excluded")
	@GetMapping(value = "/top-diagnoses")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.") })
	@ResponseStatus(value = HttpStatus.OK)
	public HashMap<String, Long> getTopDiagnoses(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
			Pageable pageRequest) throws Exception {
		Long userId = accountService.getContextUser().getId();
		logger.info("User with id " + userId + " is trying to access top diagnoses stats between " + startDate + " and "
				+ endDate + " with page request " + pageRequest);
		return statService.getTopDiagnoses(startDate, endDate, pageRequest);
	}

	@ApiOperation(value = "Get a map <String title, Double Percentage> of top diagnoses with the percentage values of all diagnosis made int the given time period", notes = "Start date included, end date excluded")
	@GetMapping(value = "/top-diagnoses/pc")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.") })
	@ResponseStatus(value = HttpStatus.OK)
	public HashMap<String, Double> getTopDiagnosesPc(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
			Pageable pageRequest) throws Exception {
		Long userId = accountService.getContextUser().getId();
		logger.info("User with id " + userId + " is trying to access top diagnosesPc stats between " + startDate
				+ " and " + endDate + "with page request " + pageRequest);
		return statService.getTopDiagnosesPc(startDate, endDate, pageRequest);
	}

	@ApiOperation(value = "Get a map <String title, Long Count> of top ingredients found in prescription fills in the given time period", notes = "Start date included, end date excluded")
	@GetMapping(value = "/top-ingredients")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.") })
	@ResponseStatus(value = HttpStatus.OK)
	public HashMap<String, Long> getTopIngredients(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
			Pageable pageRequest) throws Exception {
		Long userId = accountService.getContextUser().getId();
		logger.info("User with id " + userId + " is trying to access top ingredients stats between " + startDate
				+ " and " + endDate + "with page request " + pageRequest);
		return statService.getTopIngredients(startDate, endDate, pageRequest);
	}

	@ApiOperation(value = "Get a map <String title, Double Percentage> of top ingredient found in prescription fills with the percentage values of all prescriptions made int the given time period", notes = "Start date included, end date excluded")
	@GetMapping(value = "/top-ingredients/pc")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.") })
	@ResponseStatus(value = HttpStatus.OK)
	public HashMap<String, Double> getTopIngredientsPc(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
			Pageable pageRequest) throws Exception {
		Long userId = accountService.getContextUser().getId();
		logger.info("User with id " + userId + " is trying to access top ingredientsPc stats between " + startDate
				+ " and " + endDate + "with page request " + pageRequest);
		return statService.getTopIngredientsPc(startDate, endDate, pageRequest);
	}

	@ApiOperation(value = "Returns a list of average records per doctor per day during the given period", notes = "Start date included, end date excluded")
	@GetMapping(value = "/records-per-doctor-per-day")
	@ResponseStatus(value = HttpStatus.OK)
	public List<Double> getDoctorRecordStat(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
		Long userId = accountService.getContextUser().getId();
		logger.info("User with id " + userId
				+ " is trying to get stats re: med records per doctor per day in period between " + startDate + " and "
				+ endDate);
		return statService.getDoctorRecordsStat(startDate, endDate);
	}

	@ApiOperation(value = "Returns a list of average total visit time per doctor per day during the given period", notes = "Start date included, end date excluded")
	@GetMapping(value = "/total-visit-time-per-doctor-per-day")
	@ResponseStatus(value = HttpStatus.OK)
	public List<Double> getDoctorVisitTimeStat(
			@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
		Long userId = accountService.getContextUser().getId();
		logger.info("User with id " + userId
				+ " is trying to get stats re: total visitation time per doctor per day in period between " + startDate
				+ " and " + endDate);
		return statService.getDoctorVisitTimeStat(startDate, endDate);
	}

}
