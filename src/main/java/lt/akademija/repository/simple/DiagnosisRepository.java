package lt.akademija.repository.simple;

import lt.akademija.model.entity.simple.Diagnosis;

public interface DiagnosisRepository extends GenericSimpleEntityRepository<Diagnosis> {
}
