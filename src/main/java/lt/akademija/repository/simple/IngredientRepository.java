package lt.akademija.repository.simple;

import lt.akademija.model.entity.simple.Ingredient;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends GenericSimpleEntityRepository<Ingredient> {
}
