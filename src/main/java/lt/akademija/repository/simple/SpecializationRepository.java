package lt.akademija.repository.simple;

import lt.akademija.model.entity.simple.Specialization;

public interface SpecializationRepository extends GenericSimpleEntityRepository<Specialization> {

}
