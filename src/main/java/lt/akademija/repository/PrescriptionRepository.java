package lt.akademija.repository;

import lt.akademija.repository.users.RepositoryCustom;
import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.dto.interfaces.PrescriptionDTOInterface;
import lt.akademija.model.entity.Prescription;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository dealing with Prescription entity queries as well as queries where
 * prescriptions are the primary entities in question
 *
 */
@Repository
public interface PrescriptionRepository extends GenericRepository<Prescription>, RepositoryCustom<PrescriptionDTO> {
	Page<PrescriptionDTOInterface> findAllByPatient_Pid(Long pid, Pageable pageRequest);

	Page<PrescriptionDTOInterface> findAllByPatient_Id(Long id, Pageable pageRequest);

	PrescriptionDTOInterface findOneById(Long prescriptionId);

}
