package lt.akademija.repository.implementation;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Transactional
@Repository
public class MedicalHistoryRepositoryImpl implements RepositoryCustom<MedicalHistoryDTO> {
	private static Logger logger = Logger.getLogger(MedicalHistoryRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public MedicalHistoryDTO saveDTO(MedicalHistoryDTO historyDTO) throws Exception {
		Long doctorId = historyDTO.getDoctorId();
		if (doctorId == null) {
			throw new Exception("Doctor id cannot be null.");
		}
		Query query = em.createQuery("Select d from Doctor d where d.id = ?1").setParameter(1, doctorId);
		Doctor doctor = (Doctor) JpaResultHelper.getSingleResultOrNull(query);
		if (doctor == null) {
			throw new Exception("No doctor with id " + doctorId + " exists.");
		}
		historyDTO.setDoctorId(doctorId);

		String diagnosisTitle = historyDTO.getDiagnosisTitle();
		Long diagnosisId = historyDTO.getDiagnosisId();
		Diagnosis diagnosis = RepositoryDecisionMakers.decideDiagnosis(diagnosisId, diagnosisTitle, em);

		Long patientPid = historyDTO.getPatientPid();
		Long patientId = historyDTO.getPatientId();
		Patient patient = RepositoryDecisionMakers.decidePatient(patientId, patientPid, em);

		logger.debug("Medical history constructor about to be called: ");
		MedicalHistory history = new MedicalHistory(historyDTO, doctor, patient, diagnosis);
		logger.info("Created history: " + history);
		historyDTO.setDate(history.getMedicalHistoryPK().getTimestamp());
		historyDTO.setDoctorFirstName(history.getMedicalHistoryPK().getDoctor().getFirstName());
		historyDTO.setDoctorLastName(history.getMedicalHistoryPK().getDoctor().getLastName());
		historyDTO.setDiagnosisDescription(history.getMedicalHistoryPK().getDiagnosis().getDescription());
		historyDTO.setDiagnosisTitle(history.getMedicalHistoryPK().getDiagnosis().getTitle());
		this.em.persist(history);
		this.em.flush();
		return historyDTO;
	}

	@Override
	public MedicalHistoryDTO updateDTOByPid(Long pid, MedicalHistoryDTO historyDTO) {
		// against policy
		return null;
	}

	@Override
	public MedicalHistoryDTO updateDTOById(Long id, MedicalHistoryDTO historyDTO) {
		// against policy
		return null;
	}

}
