package lt.akademija.repository.implementation;

import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.PrescriptionFills;

import lt.akademija.model.entity.users.Pharmacist;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Date;

@Transactional
@Repository
public class PrescriptionFillsRepositoryImpl implements RepositoryCustom<PrescriptionFillsDTO> {
	private static Logger logger = Logger.getLogger(PrescriptionRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public PrescriptionFillsDTO saveDTO(PrescriptionFillsDTO prescriptionFillsDTO) throws Exception {
		logger.info("Creating prescription fill from dto " + prescriptionFillsDTO);
		Long pharmacistId = prescriptionFillsDTO.getPhramacistId();
		if (pharmacistId == null) {
			throw new NullPointerException("PharmacistId cannot be null.");
		}
		Query query = em.createQuery("Select d from Pharmacist d where d.id = ?1").setParameter(1, pharmacistId);
		Pharmacist pharmacist = (Pharmacist) JpaResultHelper.getSingleResultOrNull(query);
		if (pharmacist == null) {
			throw new NullPointerException("No pharmacist with id " + pharmacistId + " exists.");
		}
		prescriptionFillsDTO.setPhFirstName(pharmacist.getFirstName());
		prescriptionFillsDTO.setPhLastName(pharmacist.getLastName());
		Long prescriptionId = prescriptionFillsDTO.getPrescriptionId();
		if (prescriptionId == null) {
			throw new NullPointerException("PrescriptionId cannot be null.");
		}
		query = em.createQuery("Select d from Prescription d where d.id = ?1").setParameter(1, prescriptionId);
		Prescription prescription = (Prescription) JpaResultHelper.getSingleResultOrNull(query);
		if (prescription == null) {
			throw new NullPointerException("No prescription with id " + prescriptionId + " exists.");
		}
		Date date = prescriptionFillsDTO.getDate();
		PrescriptionFills prescriptionFills = new PrescriptionFills(date, pharmacist, prescription);
		prescriptionFillsDTO.setDate(prescriptionFills.getPrescriptionFillsPK().getTimestamp());
		this.em.persist(prescriptionFills);
		logger.info("Prescription fill created " + prescriptionFills);
		this.em.flush();
		return prescriptionFillsDTO;
	}

	@Override
	public PrescriptionFillsDTO updateDTOByPid(Long pid, PrescriptionFillsDTO DTO) {
		// policy forbids
		return null;
	}

	@Override
	public PrescriptionFillsDTO updateDTOById(Long id, PrescriptionFillsDTO DTO) {
		// policy forbids
		return null;
	}
}
