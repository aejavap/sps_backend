package lt.akademija.repository.implementation;

import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

// reikia aptvarkyt savo koda (darius apie savo koda cia)
@Transactional
@Repository
public class PrescriptionRepositoryImpl implements RepositoryCustom<PrescriptionDTO> {
	private static Logger logger = Logger.getLogger(PrescriptionRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public PrescriptionDTO saveDTO(PrescriptionDTO prescriptionDTO) throws Exception {
		logger.info("Creating prescription from dto : " + prescriptionDTO);

		Long doctorId = prescriptionDTO.getDoctorId();
		if (doctorId == null) {
			throw new Exception("DoctorId cannot be null.");
		}
		Query query = em.createQuery("Select d from Doctor d where d.id = ?1").setParameter(1, doctorId);
		Doctor doctor = (Doctor) JpaResultHelper.getSingleResultOrNull(query);
		if (doctor == null) {
			throw new NullPointerException("No doctor with id " + doctorId + " exists.");
		}

		Long ingredientId = prescriptionDTO.getActiveIngredientId();
		String ingredientTitle = prescriptionDTO.getActiveIngredientTitle();
		Ingredient ingredient = RepositoryDecisionMakers.decideIngredient(ingredientId, ingredientTitle, em);

		Long patientPid = prescriptionDTO.getPatientPid();
		Long patientId = prescriptionDTO.getPatientId();
		Patient patient = RepositoryDecisionMakers.decidePatient(patientId, patientPid, em);

		Prescription prescription = new Prescription(prescriptionDTO, ingredient, doctor, patient);
		this.em.persist(prescription);
		logger.info("Created prescription: " + prescription.toString());
		prescriptionDTO.setPrescriptionId(prescription.getId());
		prescriptionDTO.setDoctorFirstName(prescription.getDoctor().getFirstName());
		prescriptionDTO.setDoctorLastName(prescription.getDoctor().getLastName());
		prescriptionDTO.setPrescriptionDate(prescription.getPrescriptionDate());
		prescriptionDTO.setActiveIngredientTitle(prescription.getActiveIngredient().getTitle());
		this.em.flush();
		return prescriptionDTO;
	}

	@Override
	public PrescriptionDTO updateDTOByPid(Long pid, PrescriptionDTO DTO) {
		// against policy
		return null;
	}

	@Override
	public PrescriptionDTO updateDTOById(Long id, PrescriptionDTO DTO) {
		// against policy
		return null;
	}

}
