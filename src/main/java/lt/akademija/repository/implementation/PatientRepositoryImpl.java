package lt.akademija.repository.implementation;

import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Transactional
@Repository
public class PatientRepositoryImpl implements RepositoryCustom<PatientDTO> {
	private static Logger logger = Logger.getLogger(PatientRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	/**
	 * Creates patient entity from passed DTO patient instance;
	 *
	 * @param patientDTO
	 *            - patient DTO;
	 * @return - return DTO of created patient;
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PatientDTO saveDTO(PatientDTO patientDTO) throws Exception {
		logger.info("Creating a patient from dto " + patientDTO);
		Patient patient = new Patient();
		patient = localCopy(patientDTO, patient);
		this.em.persist(patient);
		logger.info("Patient created: " + patient);
		patientDTO.setId(patient.getId());
		patientDTO.setDoctor(patient.getDoctor());
		this.em.flush();
		return patientDTO;
	}

	/**
	 * Updates patient by its pid
	 * 
	 * @param pid
	 *            - patient pid;
	 * @param patientDTO
	 *            - patientDTO;
	 * @return created patient DTO;
	 * @throws Exception
	 */
	@Override
	public PatientDTO updateDTOByPid(Long pid, PatientDTO patientDTO) throws Exception {
		logger.info("Updating a patient from DTO " + patientDTO);
		Query query = em.createQuery("Select d from Patient d where d.pid = ?1").setParameter(1, pid);
		Patient patient = (Patient) JpaResultHelper.getSingleResultOrNull(query);
		if (patient == null) {
			throw new NullPointerException("No patient with pid " + pid + " exists.");
		}
		patient = localCopy(patientDTO, patient);
		patientDTO.setDoctor(patient.getDoctor());
		this.em.merge(patient);
		logger.info("Patient entity updated " + patient);
		this.em.flush();
		return patientDTO;
	}

	/**
	 * Finds patient by id and calls patient update method;
	 *
	 * @param Id
	 *            - patient id;
	 * @param patientDTO
	 *            - DTO of the patient
	 * @return - if successful returns DTO else returns null;
	 * @throws Exception
	 */

	@Override
	public PatientDTO updateDTOById(Long Id, PatientDTO patientDTO) throws Exception {
		logger.info("Updating a patient from DTO " + patientDTO);
		Query query = em.createQuery("Select d from Patient d where d.id = ?1").setParameter(1, Id);
		Patient patient = (Patient) JpaResultHelper.getSingleResultOrNull(query);
		if (patient == null) {
			throw new NullPointerException("No patient with id " + Id + " exists.");
		}
		patient = localCopy(patientDTO, patient);
		patientDTO.setDoctor(patient.getDoctor());
		this.em.merge(patient);
		this.em.flush();
		logger.info("Patient entity updated " + patient);
		return patientDTO;
	}

	/**
	 * Helper method used in other two update methods. Copies fields, finds doctor;
	 *
	 * @param patientDTO
	 *            - patient DTO;
	 * @param patient
	 *            - patient entity;
	 * @return updated patient entity;
	 * @throws Exception
	 */

	private Patient localCopy(PatientDTO patientDTO, Patient patient) throws Exception {
		logger.debug("Patient local copy invoked");
		Long doctorId = patientDTO.getDoctorId();
		Long doctorPid = patientDTO.getDoctorPid();
		Doctor doctor = RepositoryDecisionMakers.decideDoctor(doctorId, doctorPid, em);
		patient.setDoctor(doctor);
		patient.setPid(patientDTO.getPid());
		patient.setDob(patientDTO.getDob());
		patient.setFirstName(patientDTO.getFirstName());
		patient.setLastName(patientDTO.getLastName());
		return patient;
	}

}
