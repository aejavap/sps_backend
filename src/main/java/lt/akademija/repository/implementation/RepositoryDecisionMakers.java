package lt.akademija.repository.implementation;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.model.entity.simple.Specialization;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;

/**
 * Responsible for storing more elaborate exception handling when making
 * assignment when creating/updating entities.
 *
 */
public class RepositoryDecisionMakers {

	// TODO: more DRY : make more generic methods with params
	// (String,Long)/(Long/Long)
	// reformat at last minute so not enough time for this ^ :((

	private static Logger logger = Logger.getLogger(RepositoryDecisionMakers.class);

	/**
	 * Helper method to decide what specializaiton to assign
	 * 
	 * @param specializationId
	 * @param specializationTitle
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	static Specialization decideSpecialization(Long specializationId, String specializationTitle, EntityManager em)
			throws Exception {
		boolean specializationTitleGiven = !(specializationTitle == null || specializationTitle.equals(""));
		logger.debug("Specialization title given? : " + specializationTitleGiven);
		boolean specializationIdGiven = !(specializationId == null || specializationId < 1);
		logger.debug("Specialization id given? : " + specializationIdGiven);
		Specialization specialization;
		Query query;
		if (specializationTitleGiven && specializationIdGiven) {
			query = em.createQuery("Select s from Specialization s where s.title = ?1").setParameter(1,
					specializationTitle);
			specialization = (Specialization) JpaResultHelper.getSingleResultOrNull(query);
			if (specialization != null && specialization.getId().equals(specializationId)) {
				// pass
			} else {
				throw new Exception("No specialization sharing both id " + specializationId + " and title "
						+ specializationTitle + " exists. Please give either id or title.");
			}
		} else if (specializationTitleGiven) {
			query = em.createQuery("Select s from Specialization s where s.title = ?1").setParameter(1,
					specializationTitle);
			specialization = (Specialization) JpaResultHelper.getSingleResultOrNull(query);
			if (specialization == null) {
				throw new NoResultException("No specialization with title " + specializationTitle + " exists");
			}
		} else if (specializationIdGiven) {
			query = em.createQuery("Select s from Specialization s where s.id = ?1").setParameter(1, specializationId);
			specialization = (Specialization) JpaResultHelper.getSingleResultOrNull(query);
			if (specialization == null) {
				throw new Exception("No specialization with id " + specializationId + " exists.");
			}
		} else {
			throw new Exception("Neither specialization id nor title given. Specialization cannot be null.");
		}
		logger.debug("Specialization set to " + specialization);
		return specialization;
	}

	/**
	 * Helper method to descide what diagnosis to assign
	 * 
	 * @param diagnosisId
	 * @param diagnosisTitle
	 * @return
	 * @throws Exception
	 */
	static Diagnosis decideDiagnosis(Long diagnosisId, String diagnosisTitle, EntityManager em) throws Exception {
		boolean DiagnosisTitleGiven = !(diagnosisTitle == null || diagnosisTitle.equals(""));
		logger.debug("Diagnosis title given? : " + DiagnosisTitleGiven);
		boolean DiagnosisIdGiven = !(diagnosisId == null || diagnosisId < 1);
		logger.debug("Diagnosis id given? : " + DiagnosisIdGiven);
		Diagnosis diagnosis;
		Query query;
		if (DiagnosisTitleGiven && DiagnosisIdGiven) {
			query = em.createQuery("Select d from Diagnosis d where d.title = ?1").setParameter(1, diagnosisTitle);
			diagnosis = (Diagnosis) JpaResultHelper.getSingleResultOrNull(query);
			if (diagnosis != null && diagnosis.getId().equals(diagnosisId)) {
				// pass
			} else {
				throw new Exception("No diagnosis sharing both id " + diagnosisId + " and title " + diagnosisTitle
						+ " exists. Please provide either diagnosis id or pid.");
			}
		} else if (DiagnosisTitleGiven) {
			query = em.createQuery("Select d from Diagnosis d where d.title = ?1").setParameter(1, diagnosisTitle);
			diagnosis = (Diagnosis) JpaResultHelper.getSingleResultOrNull(query);
			if (diagnosis == null) {
				throw new Exception("No diagnosis with title " + diagnosisTitle + " exists.");
			}
		} else if (DiagnosisIdGiven) {
			System.out.println("########################################");
			System.out.println("DIAGNOSIS ID : " + diagnosisId);
			System.out.println("########################################");
			query = em.createQuery("Select d from Diagnosis d where d.id = ?1").setParameter(1, diagnosisId);
			diagnosis = (Diagnosis) JpaResultHelper.getSingleResultOrNull(query);
			if (diagnosis == null) {
				throw new Exception("No diagnosis with id " + diagnosisId + " exists.");
			}
		} else {
			throw new Exception("Neither diagnosis id nor title were given. Diagnosis cannot be null. ");
		}
		logger.debug("Diagnosis was set to: " + diagnosis);
		return diagnosis;
	}

	/**
	 * Helper method to decide what patient to assign
	 * 
	 * @param patientId
	 * @param patientPid
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	static Patient decidePatient(Long patientId, Long patientPid, EntityManager em) throws Exception {
		boolean patientPidGiven = !(patientPid == null || patientPid < 1);
		boolean patientIdGiven = !(patientId == null || patientId < 1);
		Patient patient = null;
		Query query;
		if (patientPidGiven && patientIdGiven) {
			query = em.createQuery("Select p from Patient p where p.pid = ?1").setParameter(1, patientPid);
			patient = (Patient) JpaResultHelper.getSingleResultOrNull(query);
			if (patient != null && patient.getId().equals(patientId)) {
				// pass
			} else {
				throw new Exception("No Patient sharing both id " + patientId + " and pid " + patientPid
						+ " exists. Please provide either id or pid");
			}
		} else if (patientPidGiven) {
			query = em.createQuery("Select p from Patient p where p.pid = ?1").setParameter(1, patientPid);
			patient = (Patient) JpaResultHelper.getSingleResultOrNull(query);
			if (patient == null) {
				throw new NullPointerException("No patient with pid " + patientPid + " exists.");
			}
		} else if (patientIdGiven) {
			query = em.createQuery("Select p from Patient p where p.id = ?1").setParameter(1, patientId);
			patient = (Patient) JpaResultHelper.getSingleResultOrNull(query);
			if (patient == null) {
				throw new NullPointerException("No patient with id " + patientId + " exists");
			}
		} else {
			throw new NullPointerException("Neither patient id nor pid provided.");
		}
		logger.debug("Patient set to " + patient);
		return patient;
	}

	/**
	 * Helper method to decide what doctor to assign
	 * 
	 * @param doctortId
	 * @param doctorPid
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	static Doctor decideDoctor(Long doctorId, Long doctorPid, EntityManager em) throws Exception {
		boolean doctorPidGiven = !(doctorPid == null || doctorPid < 1);
		boolean doctorIdGiven = !(doctorId == null || doctorId < 1);
		Doctor doctor = null;
		Query query;
		if (doctorPidGiven && doctorIdGiven) {
			query = em.createQuery("Select d from Doctor d where d.pid = ?1").setParameter(1, doctorPid);
			doctor = (Doctor) JpaResultHelper.getSingleResultOrNull(query);
			if (doctor != null && doctor.getId().equals(doctorId)) {
				// pass
			} else {
				throw new Exception("No doctor sharing both id " + doctorId + " and pid " + doctorPid
						+ " exists. Please provide either id or pid");
			}
		} else if (doctorPidGiven) {
			query = em.createQuery("Select d from Doctor d where d.pid = ?1").setParameter(1, doctorPid);
			doctor = (Doctor) JpaResultHelper.getSingleResultOrNull(query);
			if (doctor == null) {
				throw new NullPointerException("No doctor with pid " + doctorPid + " exists.");
			}
		} else if (doctorIdGiven) {
			query = em.createQuery("Select d from Doctor d where d.id = ?1").setParameter(1, doctorId);
			doctor = (Doctor) JpaResultHelper.getSingleResultOrNull(query);
			if (doctor == null) {
				throw new NullPointerException("No doctor  with id " + doctorId + " exists");
			}
		}
		logger.debug("Doctor set to " + doctor);
		return doctor;
	}

	/**
	 * A helper method to decide which ingredient to assign
	 * 
	 * @param ingredientId
	 * @param ingredientTitle
	 * @return
	 * @throws Exception
	 */
	static Ingredient decideIngredient(Long ingredientId, String ingredientTitle, EntityManager em) throws Exception {
		boolean ingredientIdGiven = !(ingredientId == null || ingredientId < 1);
		logger.debug("Ingredient id given : " + ingredientIdGiven);
		boolean ingredientTitleGiven = !(ingredientTitle == null || ingredientTitle.equals(""));
		logger.debug("Ingredient title given : " + ingredientTitleGiven);
		Ingredient ingredient;
		Query query;
		if (ingredientIdGiven && ingredientTitleGiven) {
			query = em.createQuery("Select i from Ingredient i where i.id = ?1").setParameter(1, ingredientId);
			ingredient = (Ingredient) JpaResultHelper.getSingleResultOrNull(query);
			if (ingredient != null && ingredientTitle.equals(ingredient.getTitle())) {
				// pass
			} else {
				throw new Exception("No ingredient sharing both id " + ingredientId + " and title " + ingredientTitle
						+ " exists. Please use either id or title.");
			}
		} else if (ingredientTitleGiven) {
			query = em.createQuery("Select i from Ingredient i where i.title = ?1").setParameter(1, ingredientTitle);
			ingredient = (Ingredient) JpaResultHelper.getSingleResultOrNull(query);
			if (ingredient == null) {
				throw new Exception("No ingredient with title " + ingredientTitle + " exists.");
			}
		} else if (ingredientIdGiven) {
			query = em.createQuery("Select i from Ingredient i where i.id = ?1").setParameter(1, ingredientId);
			ingredient = (Ingredient) JpaResultHelper.getSingleResultOrNull(query);
			if (ingredient == null) {
				throw new Exception(
						"Ingredient id was given as " + ingredientId + " but no corresponding ingredient found.");
			}
		} else {
			throw new Exception("Neither ingredient id nor ingredient title were given.");
		}
		logger.trace("Ingredient set to " + ingredient);
		return ingredient;
	}

}
