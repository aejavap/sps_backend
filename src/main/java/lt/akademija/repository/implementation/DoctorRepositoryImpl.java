package lt.akademija.repository.implementation;

import lt.akademija.model.dto.users.DoctorDTO;
import lt.akademija.model.entity.simple.Specialization;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Transactional
@Repository
public class DoctorRepositoryImpl implements RepositoryCustom<DoctorDTO> {

    private static Logger logger = Logger.getLogger(DoctorRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public DoctorDTO saveDTO(DoctorDTO doctorDTO) throws Exception {
        logger.info("Creating doctor from dto : " + doctorDTO);
        Doctor doctor = new Doctor();
        doctor = localCopy(doctorDTO, doctor);
        doctorDTO.setSpecializationId(doctor.getSpecializationId());
        doctorDTO.setSpecializationTitle(doctor.getSpecialization().getTitle());
        this.em.persist(doctor);
        logger.info("Doctor saved : " + doctor);
        doctorDTO.setId(doctor.getId());
        this.em.flush();
        return doctorDTO;
    }

    @Override
    public DoctorDTO updateDTOByPid(Long pid, DoctorDTO doctorDTO) throws Exception {
        logger.info("Updating doctor from dto: " + doctorDTO);
        Query query = em.createQuery("Select d from Doctor d where d.pid = ?1").setParameter(1, pid);
        Doctor doctor = (Doctor) JpaResultHelper.getSingleResultOrNull(query);
        if (doctor == null) {
            throw new Exception("No doctor with pid " + pid + " exists.");
        }
        doctor = localCopy(doctorDTO, doctor);
        doctorDTO.setSpecializationId(doctor.getSpecializationId());
        doctorDTO.setSpecializationTitle(doctor.getSpecialization().getTitle());
        this.em.merge(doctor);
        doctorDTO.setId(doctor.getId());
        logger.info("Doctor updated: " + doctor);
        this.em.flush();
        return doctorDTO;
    }

    @Override
    public DoctorDTO updateDTOById(Long id, DoctorDTO doctorDTO) throws Exception {
        logger.info("Updating doctor from dto: " + doctorDTO);
        Query query = em.createQuery("Select d from Doctor d where d.id = ?1").setParameter(1, id);
        Doctor doctor = (Doctor) JpaResultHelper.getSingleResultOrNull(query);
        if (doctor == null) {
            throw new Exception("No doctor with id " + id + " exists.");
        }
        doctor = localCopy(doctorDTO, doctor);
        doctorDTO.setSpecializationId(doctor.getSpecializationId());
        doctorDTO.setSpecializationTitle(doctor.getSpecialization().getTitle());
        this.em.merge(doctor);
        doctorDTO.setId(doctor.getId());
        logger.info("Doctor updated: " + doctor);
        this.em.flush();
        return doctorDTO;
    }

    private Doctor localCopy(DoctorDTO doctorDTO, Doctor doctor) throws Exception {
        logger.debug("Doctor local copy invoked.");
        String specializationTitle = doctorDTO.getSpecializationTitle();
        Long specializationId = doctorDTO.getSpecializationId();
        Specialization specialization = RepositoryDecisionMakers.decideSpecialization(specializationId,
                specializationTitle, em);
        doctor.setSpecialization(specialization);
        doctor.setPid(doctorDTO.getPid());
        doctor.setFirstName(doctorDTO.getFirstName());
        doctor.setLastName(doctorDTO.getLastName());
        return doctor;
    }

}
