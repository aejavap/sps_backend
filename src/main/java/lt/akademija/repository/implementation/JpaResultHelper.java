package lt.akademija.repository.implementation;

import java.util.List;

import javax.persistence.Query;

/**
 * Used to avoid unwelcomed NoResults exception
 *
 */
public class JpaResultHelper {
	public static Object getSingleResultOrNull(Query query) throws Exception {
		@SuppressWarnings("rawtypes")
		List results = query.getResultList();
		if (results.isEmpty())
			return null;
		else if (results.size() == 1)
			return results.get(0);
		throw new Exception();
	}
}
