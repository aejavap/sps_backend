package lt.akademija.repository.implementation;

import lt.akademija.model.dto.users.PharmacistDTO;
import lt.akademija.model.entity.users.Pharmacist;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Transactional
@Repository
public class PharmacistRepositoryImpl implements RepositoryCustom<PharmacistDTO> {
	private static Logger logger = Logger.getLogger(PharmacistRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public PharmacistDTO saveDTO(PharmacistDTO pharmacistDTO) {
		logger.info("Creating pharmacist from dto " + pharmacistDTO);
		Pharmacist pharmacist = new Pharmacist();
		pharmacist = localCopy(pharmacistDTO, pharmacist);
		this.em.persist(pharmacist);
		logger.info("Created pharmacist " + pharmacist);
		pharmacistDTO.setId(pharmacist.getId());
		this.em.flush();
		return pharmacistDTO;
	}

	@Override
	public PharmacistDTO updateDTOByPid(Long pid, PharmacistDTO pharmacistDTO) throws Exception {
		logger.info("Updating pharmacist from dto " + pharmacistDTO);
		Query query = em.createQuery("Select d from Pharmacist d where d.pid = ?1").setParameter(1, pid);
		Pharmacist pharmacist = (Pharmacist) JpaResultHelper.getSingleResultOrNull(query);
		if (pharmacist == null) {
			throw new NullPointerException("No pharmacist with pid " + pid + " exists.");
		}
		pharmacist = localCopy(pharmacistDTO, pharmacist);
		this.em.merge(pharmacist);
		logger.info("Update pharmacist: " + pharmacist);
		this.em.flush();
		return pharmacistDTO;
	}

	@Override
	public PharmacistDTO updateDTOById(Long id, PharmacistDTO pharmacistDTO) throws Exception {
		logger.info("Updating pharmacist from dto " + pharmacistDTO);
		Query query = em.createQuery("Select d from Pharmacist d where d.id = ?1").setParameter(1, id);
		Pharmacist pharmacist = (Pharmacist) JpaResultHelper.getSingleResultOrNull(query);
		if (pharmacist == null) {
			throw new NullPointerException("No pharmacist with id " + id + " exists.");
		}
		pharmacist = localCopy(pharmacistDTO, pharmacist);
		this.em.merge(pharmacist);
		logger.info("Update pharmacist: " + pharmacist);
		this.em.flush();
		return pharmacistDTO;
	}

	private Pharmacist localCopy(PharmacistDTO pharmacistDTO, Pharmacist pharmacist) {
		logger.debug("Pharmacist local copy invoked");
		pharmacist.setPid(pharmacistDTO.getPid());
		pharmacist.setFirstName(pharmacistDTO.getFirstName());
		pharmacist.setLastName(pharmacistDTO.getLastName());
		pharmacist.setCompanyType(pharmacistDTO.getCompanyType());
		pharmacist.setCompanyName(pharmacistDTO.getCompanyName());
		return pharmacist;
	}
}
