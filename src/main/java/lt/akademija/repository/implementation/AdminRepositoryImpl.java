package lt.akademija.repository.implementation;

import lt.akademija.model.dto.users.AdminDTO;
import lt.akademija.model.entity.users.Admin;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Transactional
@Repository
public class AdminRepositoryImpl implements RepositoryCustom<AdminDTO> {
	private static Logger logger = Logger.getLogger(AdminRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public AdminDTO saveDTO(AdminDTO adminDTO) {
		logger.info("Creating admin from dto: " + adminDTO);
		Admin admin = new Admin();
		admin = localCopy(adminDTO, admin);
		this.em.persist(admin);
		logger.info("Admin created : " + admin);
		adminDTO.setId(admin.getId());
		this.em.flush();
		return adminDTO;
	}

	@Override
	public AdminDTO updateDTOByPid(Long pid, AdminDTO adminDTO) throws Exception {
		logger.info("Updating admin from dto: " + adminDTO);
		Query query = em.createQuery("Select d from Admin d where d.pid = ?1").setParameter(1, pid);
		Admin admin = (Admin) JpaResultHelper.getSingleResultOrNull(query);
		if (admin == null) {
			throw new NullPointerException("No admin with pid " + pid + " exists.");
		}
		admin = localCopy(adminDTO, admin);
		this.em.merge(admin);
		logger.info("Admin updated: " + admin);
		adminDTO.setId(admin.getId());
		this.em.flush();
		return adminDTO;
	}

	@Override
	public AdminDTO updateDTOById(Long id, AdminDTO adminDTO) throws Exception {
		logger.info("Updating admin from dto: " + adminDTO);
		Query query = em.createQuery("Select d from Admin d where d.id = ?1").setParameter(1, id);
		Admin admin = (Admin) JpaResultHelper.getSingleResultOrNull(query);
		if (admin == null) {
			throw new NullPointerException("No admin with id " + id + " exists.");
		}
		admin = localCopy(adminDTO, admin);
		this.em.merge(admin);
		logger.info("Admin updated: " + admin);
		this.em.flush();
		return adminDTO;
	}

	private Admin localCopy(AdminDTO adminDTO, Admin admin) {
		logger.debug("Admin local copy invoked.");
		admin.setPid(adminDTO.getPid());
		admin.setFirstName(adminDTO.getFirstName());
		admin.setLastName(adminDTO.getLastName());
		return admin;
	}
}
