package lt.akademija.repository.users;

import lt.akademija.model.entity.users.AbstractUser;
import lt.akademija.repository.GenericRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

@NoRepositoryBean
public interface GenericUserRepository<T extends AbstractUser> extends GenericRepository<T>/* , RepositoryCustom<E> */ {

	T findOneByPid(Long pid);

	@Query(value = "select p from #{#entityName} p where p.id = ?1")
	T findById(Long id);

	@Query(value = "select p from #{#entityName} p where p.pid = (:pid) and p.id != (:id)")
	T findByPidNotWithId(@Param(value = "pid") Long pid, @Param(value = "id") Long id);

	@Query(value = "select p from #{#entityName} p where p.pid = (:pidNew) and p.pid != (:pidOld)")
	T findByPidNotWithPid(@Param(value = "pidNew") Long pidNew, @Param(value = "pidOld") Long pidOld);

	@Query(value = "select p from #{#entityName} p where UPPER(p.firstName) LIKE UPPER(:firstName)||'%'"
			+ "AND UPPER(p.lastName) LIKE UPPER(:lastName)||'%'" + "AND CAST(p.pid as text) LIKE (:pid)||'%'")
	Page<T> searchByNameOrSurnameOrPid(@Param(value = "firstName") String firstName,
			@Param(value = "lastName") String lastName, @Param(value = "pid") String pid, Pageable pageRequest);

	Page<T> findAllByLastNameStartingWithIgnoreCase(String lastName, Pageable pageable);

	Page<T> findAllByFirstNameStartingWithIgnoreCase(String firstName, Pageable pageable);

	Page<T> findAllByOrderByIdAsc(Pageable pageRequest);

	Page<T> findAllByOrderByPidAsc(Pageable pageRequest);

	Page<T> findAllByOrderByLastNameAsc(Pageable pageRequest);
}
