package lt.akademija.repository.users;

public interface RepositoryCustom<T> {

	<S extends T> S saveDTO(S DTO) throws Exception;

	T updateDTOByPid(Long pid, T DTO) throws Exception;

	T updateDTOById(Long id, T DTO) throws Exception;
}
