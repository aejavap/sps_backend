package lt.akademija.repository.users;

import lt.akademija.model.dto.users.DoctorDTO;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends GenericUserRepository<Doctor>, RepositoryCustom<DoctorDTO> {

	Page<Doctor> findAllByOrderBySpecializationAsc(Pageable pageRequest);

	@Query(value = "Select p from Doctor d JOIN d.patientList p where p.doctor.pid = ?1")
	Page<Patient> findPatientListByPid(Long pid, Pageable pageable);
	
}
