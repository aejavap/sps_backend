package lt.akademija.repository;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.dto.interfaces.MedicalHistoryDTOInterface;
import lt.akademija.model.entity.*;

import java.util.Date;
import java.util.List;

import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository dealing with MedicalHistory entities and other queries where
 * medical histories are the primary entities in question
 *
 */
@Repository
public interface MedicalHistoryRepository
		extends GenericRepository<MedicalHistory>, RepositoryCustom<MedicalHistoryDTO> {

	Page<MedicalHistoryDTOInterface> findByMedicalHistoryPK_Patient_Pid/* OrderByMedicalHistoryPK_TimestampDesc */(
			Long pid, Pageable pageRequest);

	Page<MedicalHistoryDTOInterface> findAllByMedicalHistoryPK_Patient_Id/* OrderByMedicalHistoryPK_TimestampDesc */(
			Long id, Pageable pageRequest);

	MedicalHistoryDTOInterface findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp(
			Long patientId, Long doctorId, Long diagnosisId, Date date);

	@Query("SELECT h.medicalHistoryPK.diagnosis.description FROM MedicalHistory h WHERE h.repeatVisit = FALSE AND h.medicalHistoryPK.timestamp >= (:startDate) AND h.medicalHistoryPK.timestamp < (:endDate) GROUP BY h.medicalHistoryPK.diagnosis.id ORDER BY COUNT(h) ASC")
	Page<String> getTopDiagnosesDescriptions(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate,
			Pageable pageRequest);

	@Query("SELECT count(h) FROM MedicalHistory h WHERE h.repeatVisit = FALSE AND h.medicalHistoryPK.timestamp >= (:startDate) AND h.medicalHistoryPK.timestamp < (:endDate) GROUP BY h.medicalHistoryPK.diagnosis.id ORDER BY COUNT(h) ASC")
	Page<Long> getTopDiagnosesCounts(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate,
			Pageable pageRequest);

	@Query("SELECT count(h) FROM MedicalHistory h WHERE h.repeatVisit = FALSE AND h.medicalHistoryPK.timestamp >= (:startDate) AND h.medicalHistoryPK.timestamp < (:endDate)")
	long countNonRepeat(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate);

	long countByMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Timestamp(Long doctorId, Date date);

	long countByMedicalHistoryPK_Timestamp(Date date);

	List<MedicalHistory> findByMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Timestamp(Long doctorId, Date date);

	List<MedicalHistory> findByMedicalHistoryPK_Timestamp(Date date);

}
