package lt.akademija.repository;

import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.dto.interfaces.PrescriptionFillsDTOInterface;
import lt.akademija.repository.users.RepositoryCustom;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lt.akademija.model.entity.*;

/**
 * Repository dealing with PrescriptionFills entity queries
 *
 */
@Repository
public interface PrescriptionFillsRepository
		extends GenericRepository<PrescriptionFills>, RepositoryCustom<PrescriptionFillsDTO> {

	Page<PrescriptionFillsDTOInterface> findByPrescriptionFillsPK_Prescription_Patient_Pid(Long pid,
			Pageable pageRequest);

	Page<PrescriptionFillsDTOInterface> findByPrescriptionFillsPK_Prescription_Patient_Id(Long id,
			Pageable pageRequest);

	Long countByPrescriptionFillsPK_Prescription_Id(Long id);

	Page<PrescriptionFillsDTOInterface> findByPrescriptionFillsPK_Prescription_Id(Long id, Pageable pageRequest);
	
	@Query("SELECT p.prescriptionFillsPK.prescription.activeIngredient.title FROM PrescriptionFills p WHERE p.prescriptionFillsPK.timestamp >= (:startDate) AND p.prescriptionFillsPK.timestamp < (:endDate) GROUP BY p.prescriptionFillsPK.prescription.activeIngredient ORDER BY COUNT(p) ASC")
	Page<String> getTopIngredientTitles(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate,
			Pageable pageRequest);

	@Query("SELECT count(p) FROM PrescriptionFills p WHERE p.prescriptionFillsPK.timestamp >= (:startDate) AND p.prescriptionFillsPK.timestamp < (:endDate) GROUP BY p.prescriptionFillsPK.prescription.activeIngredient ORDER BY COUNT(p) ASC")
	Page<Long> getTopIngredientCounts(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate,
			Pageable pageRequest);

	@Query("SELECT count(p) FROM PrescriptionFills p WHERE p.prescriptionFillsPK.timestamp >= (:startDate) AND p.prescriptionFillsPK.timestamp < (:endDate)")
	long countInPeriod(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate);
	
}
