package lt.akademija.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * Customizes basic Spring Security features.
 *
 */
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired //  lt.akademija.service.AccountService
	public UserDetailsService userDetailsService;

	@Autowired // Bean in App.java
	private PasswordEncoder passwordEncoder;

	/**
	 * Method obtaining authentication manager which in turn processes
	 * Authentication requests.
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

	/**
	 * Method exposing the AuthenticationManager from
	 * configure(AuthenticationManagerBuilder) to be exposed as a Bean.
	 */
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	/**
	 * Method defining the behaviour of global and path-specific security
	 * interceptors.
	 *
	 * Creates a filter chain with order = 0.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.cors()
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()		
				.authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				
				// disable swagger functionality
				.antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
						"/swagger-ui", "/webjars/**", "/swagger-resources/configuration/ui", "/swagge‌​r-ui.html")
				.denyAll()
				
				// `these are the same as in the ResourceServerConfig
				// required if testing frontend using react build within maven
				// required for permissions to work if using swagger
				.antMatchers("api/stat/**").anonymous()
				.antMatchers("/api/me", "/api/me-account", "/api/me-account/update", "/api/specialization/**", "/api/diagnosis/**", "/api/ingredient/**").hasAuthority("ROLE_USER")
				.antMatchers("/api/patient/**").hasAuthority("ROLE_PATIENT")
				.antMatchers("/api/doctor/**").hasAuthority("ROLE_DOCTOR")
				.antMatchers("/api/pharmacist/**").hasAuthority("ROLE_PHARMACIST")
				.antMatchers("/api/admin/**").hasAuthority("ROLE_ADMIN")
				// For paths NOT detailed above: we allow everyone to access those paths
				.and().httpBasic().and()
				.csrf().disable();
	}

}
