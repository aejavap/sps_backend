package lt.akademija.service;

import java.util.Optional;

import javax.security.auth.login.AccountException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lt.akademija.model.dto.AccountDTO;
import lt.akademija.model.dto.AccountDTOFull;
import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.users.AbstractUser;
import lt.akademija.repository.AccountRepository;

/**
 * Service dealing with user accounts. Neede for authentication as well.
 *
 */
@Service
public class AccountService implements UserDetailsService {

	private static final Logger logger = LogManager.getLogger(AccountService.class);

	@Autowired
	private AccountRepository accountRepo;

	@Autowired // Bean in App.java
	private PasswordEncoder passwordEncoder;

	/**
	 * Returns user account bu username
	 * 
	 * @param
	 * @return
	 * @throws UsernameNotFoundException
	 */
	// needed method
	@Override
	public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
		logger.info("loadUserByUsername in AccountService invoked for username " + string);
		Optional<Account> account = accountRepo.findByUsername(string);
		if (account.isPresent()) {
			logger.trace("getting user account");
			return account.get();
		} else {
			throw new UsernameNotFoundException(String.format("Username [%s] not found", string));
		}
	}

	/**
	 * Returns user Account by username
	 * 
	 * @param username
	 * @return
	 * @throws UsernameNotFoundException
	 */
	// same but better method name
	public Account getAccountByUsername(String username) throws UsernameNotFoundException {
		logger.info("getAccountByUsername in AccountService invoked for username " + username);
		Optional<Account> account = accountRepo.findByUsername(username);
		if (account.isPresent()) {
			logger.trace("getting user account");
			return account.get();
		} else {
			throw new UsernameNotFoundException(String.format("Username [%s] doest not exist", username));
		}
	}

	/**
	 * Returns Account of user making a request.
	 * 
	 * @return
	 * @throws UsernameNotFoundException
	 */
	public Account getContextUser() throws UsernameNotFoundException {
		logger.info("getContextUser in AccountService invoked");
		Account account = getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		return account;
	}

	/**
	 * Given accout id, returns relevant Account object.
	 * 
	 * @param id
	 * @return
	 * @throws AccountException
	 */
	public Account getAccountById(Long id) throws AccountException {
		logger.info("getAccountById in AccountService invoked for id " + id);
		Optional<Account> account = accountRepo.findById(id);
		if (account.isPresent()) {
			logger.debug("Account present");
			return account.get();
		} else {
			throw new AccountException(String.format("Account with id [%s] doest not exist", id));
		}
	}

	/**
	 * Given account id, returns boolean value indicating if such account exists or
	 * not
	 * 
	 * @param id
	 * @return
	 */
	public boolean existsById(Long id) {
		logger.info("existsById in AccountService invoked for id " + id);
		Optional<Account> account = accountRepo.findById(id);
		if (account.isPresent()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Given account id, returns the relevant AccountDTOFull of the account in
	 * question
	 * 
	 * @param id
	 * @return
	 * @throws AccountException
	 */
	public AccountDTOFull getAccountDTOById(Long id) throws AccountException {
		logger.info("getAccountDTOById in AccountService invoked for id " + id );
		Optional<Account> account = accountRepo.findById(id);
		if (account.isPresent()) {
			logger.debug("Account present");
			return AccountDTOFull.toDTO(account.get());
		} else {
			throw new AccountException(String.format("Account with id [%s] does not exist", id));
		}
	}

	public AccountDTOFull getAccountDTOByUsername(String username) throws AccountException {
		logger.info("getAccountDTOByUsername in AccountService invoked for username " + username);
		Optional<Account> account = accountRepo.findByUsername(username);
		if (account.isPresent()) {
			logger.debug("Account present");
			return AccountDTOFull.toDTO(account.get());
		} else {
			throw new AccountException(String.format("Username [%s] does not exist", username));
		}
	}

	/**
	 * Given account username returns the relevant AccountDTOFull of the account in
	 * question
	 * 
	 * @param account
	 * @param userGiven
	 * @return
	 * @throws AccountException
	 */
	public Account createAccount(Account account, AbstractUser userGiven) throws AccountException {
		logger.info("createAccount in AccountService invoked for user " + userGiven);
		if (accountRepo.countByUsername(account.getUsername()) == 0) {
			logger.debug("Username not in use");
			// Password encoding
			account.setPassword(passwordEncoder.encode(account.getPassword()));
			account.setAbstractUser(userGiven);
			return accountRepo.save(account);
		} else {
			throw new AccountException(String.format("Username [%s] already taken.", account.getUsername()));
		}
	}

	/**
	 * Given account id, resets account's password to user's pid number
	 * 
	 * @param accountId
	 * @throws AccountException
	 */
	public void resetPasswordById(Long accountId) throws AccountException {
		logger.info("resetPasswordById in AccountService invoked for id " + accountId);
		Optional<Account> possibleAccountInRep = accountRepo.findById(accountId);
		if (possibleAccountInRep.isPresent()) {
			logger.debug("Account is present");
			Account account = possibleAccountInRep.get();
			AbstractUser abstractUser = account.getAbstractUser();
			String newPassword = abstractUser.getPid().toString();
			account.setPassword(passwordEncoder.encode(newPassword));
			accountRepo.saveAndFlush(account);
		} else {
			throw new AccountException("No account with id " + accountId + " exists.");
		}
	}

	/**
	 * Given account's id, reverses account's enable value effectively
	 * activating/deactivating the account in question
	 * 
	 * @param accountId
	 * @throws AccountException
	 */
	public void switchEnabledById(Long accountId) throws AccountException {
		logger.info("switchEnabledById in AccountService invoked for id " + accountId);
		Optional<Account> possibleAccountInRep = accountRepo.findById(accountId);
		if (possibleAccountInRep.isPresent()) {
			logger.debug("Account present");
			Account account = possibleAccountInRep.get();
			account.setEnabled(!account.getEnabled());
			accountRepo.saveAndFlush(account);
		} else {
			throw new AccountException("No account with id " + accountId + " exists.");
		}
	}

	/**
	 * Given account id and AccountDTO, updates the Account in question
	 * 
	 * @param accountId
	 * @param updatedAccountDTO
	 * @return
	 * @throws AccountException
	 */
	public Account updateById(Long accountId, AccountDTO updatedAccountDTO) throws AccountException {
		Optional<Account> possibleAccountInRep = accountRepo.findById(accountId);
		if (possibleAccountInRep.isPresent()) {
			logger.info("AccountService attempting to do basic account update on user with id " + accountId);
			Account accountInRep = possibleAccountInRep.get();
			String newPassword = updatedAccountDTO.getPassword();
			String newUsername = updatedAccountDTO.getUsername();
			if ((newPassword == null || newPassword.equals("")) && (newUsername == null || newUsername.equals(""))) {
				throw new AccountException("Neither username nor password provided");
			} else if (newUsername == null || newUsername.equals("")) {
				accountInRep.setPassword(passwordEncoder.encode(newPassword));
			} else if (newPassword == null || newPassword.equals("")) {
				Optional<Account> possibleAccountWithNewUsername = accountRepo.findByUsername(newUsername);
				if (possibleAccountWithNewUsername.isPresent()) {
					throw new AccountException(String.format("Username [%s] already taken.", newUsername));
				} else {
					accountInRep.setUsername(newUsername);
				}
			} else {
				Optional<Account> possibleAccountWithNewUsername = accountRepo.findByUsername(newUsername);
				if (possibleAccountWithNewUsername.isPresent()) {
					throw new AccountException(String.format("Username [%s] already taken.", newUsername));
				} else {
					accountInRep.setPassword(passwordEncoder.encode(newPassword));
					accountInRep.setUsername(newUsername);
				}
			}
			logger.info("Account with id " + accountId + " was updated");
			return accountRepo.save(accountInRep);
		} else {
			throw new AccountException(String.format("Account with id [%s] does not exist.", accountId));
		}

	}

}
