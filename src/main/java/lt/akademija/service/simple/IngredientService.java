package lt.akademija.service.simple;

import org.springframework.stereotype.Service;

import lt.akademija.model.entity.simple.Ingredient;


/**
 * Case of GenericSimpleService applied to ingredient entity
 *
 */
@Service
public class IngredientService extends GenericSimpleService<Ingredient/* , IngredientRepository */> {
	public IngredientService() {
		super(Ingredient.class);
	}
}
