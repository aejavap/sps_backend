package lt.akademija.service;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.dto.interfaces.MedicalHistoryDTOInterface;
import lt.akademija.repository.MedicalHistoryRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Class responsible for working with medical history repository.
 */
@Service
public class MedicalHistoryService {

	private static final Logger logger = LogManager.getLogger(MedicalHistoryService.class);

	@Autowired
	private MedicalHistoryRepository historyRepository;

	/**
	 * Finds all medical records of patient.
	 * @param id - patient id.
	 * @param pageRequest - paging information.
	 * @return - paged medical records.
	 */

	public Page<MedicalHistoryDTO> findByPatientId(Long id, Pageable pageRequest) {
		logger.info("MedicalHistoryService attempting to find medical record with patient id " + id);
		return historyRepository
				.findAllByMedicalHistoryPK_Patient_Id/* OrderByMedicalHistoryPK_TimestampDesc */(id, pageRequest)
				.map(MedicalHistoryDTO::toDTO);
	}

	/**
	 * Finds medical records by patient pid.
	 * @param pid - patient personal id.
	 * @param pageRequest - paging information.
	 * @return - paged medical records.
	 */
	public Page<MedicalHistoryDTO> findByPatientPid(Long pid, Pageable pageRequest) {
		logger.info("MedicalHistoryService attempting to find medical record with patient pid " + pid);
		return historyRepository
				.findByMedicalHistoryPK_Patient_Pid/* OrderByMedicalHistoryPK_TimestampDesc */(pid, pageRequest)
				.map(MedicalHistoryDTO::toDTO);
	}

	/**
	 * Finds distinct medical record.
	 * @param patientId - patient id.
	 * @param doctorId - doctor id.
	 * @param diagnosisId - diagnosis id.
	 * @param date - record date.
	 * @return - medical record of the patient.
	 * @throws Exception - in case of failure blame it yourself.
	 */
	public MedicalHistoryDTO findByPk(Long patientId, Long doctorId, Long diagnosisId, Date date) throws Exception {
		logger.info("MedicalHistoryService attempting to find medical record with PK ");
		logger.info("PK provided: patientId " + patientId + ", doctorId " + doctorId + ",diagnosisId " + diagnosisId
				+ ", date " + date);
		if (date == null) {
			throw new NullPointerException("Date provided cannot be null");
		}
		List<Long> longParams = Arrays.asList(patientId, doctorId, diagnosisId);
		for (Long param : longParams) {
			if (param == null || param <= 0) {
				throw new Exception("None of the ids provided can be null or <= 0");
			}
		}

		MedicalHistoryDTOInterface medicalHistory = historyRepository
				.findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp(
						patientId, doctorId, diagnosisId, date);
		MedicalHistoryDTO dtoToSend = MedicalHistoryDTO.toDTO(medicalHistory);
		if (dtoToSend == null) {
			logger.debug("No medical record found with PK provided");
		} else {
			logger.debug("Medical record found with PK provided: " + medicalHistory);
		}
		return dtoToSend;
	}

	/**
	 * Creates medical record from its DTO information.
	 * @param historyDTO - medical history DTO.
	 * @return - in case of success returns created record.
	 * @throws Exception - info hwat went wrong is here.
	 */
	public MedicalHistoryDTO createByDTO(MedicalHistoryDTO historyDTO) throws Exception {
		logger.info("MedicalHistoryService attempting to create a medical record from dto:  " + historyDTO);
		return historyRepository.saveDTO(historyDTO);
	}
	
}
