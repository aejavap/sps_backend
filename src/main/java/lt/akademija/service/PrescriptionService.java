package lt.akademija.service;

import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.repository.PrescriptionRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Class retrieves prescriptions from database. And manages prescription DTO queries
 */
@Service
public class PrescriptionService {

	private static final Logger logger = LogManager.getLogger(PrescriptionService.class);

	@Autowired
	private PrescriptionRepository repository;

	@Autowired
	private PrescriptionFillsService fillsService;

	/**
	 * Finds all prescriptions of the patient by id;
	 * @param id - patient id;
	 * @param pageRequest - paging information;
	 * @return - paged prescriptions;
	 */

	public Page<PrescriptionDTO> findAllByPatientId(Long id, Pageable pageRequest) {
		logger.info("PrescriptionService attempting to find all prescriptions with patient id: " + id);
		return repository.findAllByPatient_Id(id, pageRequest).map(Entity -> {
			PrescriptionDTO prescriptions = PrescriptionDTO.toDTO(Entity);
			prescriptions.setFillsNo(fillsService.findFillsNoByPrescriptionId(prescriptions.getPrescriptionId()));
			return prescriptions;
		});
	}

	/**
	 * Finds all prescriptions of the patient by pid.
	 * @param pid - patient pid.
	 * @param pageRequest - paging information.
	 * @return - paged prescriptions.
	 */
	public Page<PrescriptionDTO> findAllByPatientPid(Long pid, Pageable pageRequest) {
		logger.info("PrescriptionService attempting to find all prescriptions with patient pid: " + pid);
		return repository.findAllByPatient_Pid(pid, pageRequest).map(Entity -> {
			PrescriptionDTO pr = PrescriptionDTO.toDTO(Entity);
			pr.setFillsNo(fillsService.findFillsNoByPrescriptionId(pr.getPrescriptionId()));
			return pr;
		});
	}

	/**
	 * Creates prescription record from DTO object.
	 * @param prescriptionDTO - prescription DTO.
	 * @return - created prescription DTO.
	 * @throws Exception - in case of unsuccessful attempt, throws exception
	 */
	public PrescriptionDTO createFromDTO(PrescriptionDTO prescriptionDTO) throws Exception {
		logger.info("PrescriptionService attempting to create a new prescription from dto: " + prescriptionDTO);
		return repository.saveDTO(prescriptionDTO);
	}

	/**
	 * Find prescription projection by its id.
	 * @param prescriptionId - prescription id.
	 * @return - prescription projection.
	 */
	public PrescriptionDTO findByPrescriptionId(Long prescriptionId) {
		logger.info("PrescriptionService attempting to find a prescription with id: " + prescriptionId);
		PrescriptionDTO pr = PrescriptionDTO.toDTO(repository.findOneById(prescriptionId));
		if (pr != null) {
			logger.debug("Prescription with id " + prescriptionId + " found.");
			pr.setFillsNo(fillsService.findFillsNoByPrescriptionId(pr.getPrescriptionId()));
		} else {
			logger.debug("Prescription with id " + prescriptionId + " was not found.");
		}
		return pr;
	}

}
