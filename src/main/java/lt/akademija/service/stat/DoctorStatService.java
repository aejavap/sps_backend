package lt.akademija.service.stat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.repository.MedicalHistoryRepository;
import lt.akademija.repository.users.DoctorRepository;

/**
 * Service dealing with particular doctor's statistics
 *
 */
@Service
public class DoctorStatService {

	private static final Logger logger = LogManager.getLogger(DoctorStatService.class);

	@Autowired
	DoctorRepository doctorRep;

	@Autowired
	MedicalHistoryRepository recordRep;

	/**
	 * Given a time period and doctor's id, returns a list of total number of
	 * medical records processed by said doctor per day during the period given
	 * 
	 * @param doctorId
	 *            - points to doctor in question
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exlusive, not in the future
	 * @return returns List<Long> - number for each day in chronoligical order
	 */
	public List<Long> getDoctorRecordsStat(Long doctorId, LocalDate startDate, LocalDate endDate) {
		logger.info("getDoctorRecordsStat called with doctor id " + doctorId + ", start date " + startDate
				+ ", end date " + endDate);
		StatHelper.checkDates(startDate, endDate);
		List<Long> doctorRecordsPerDay = new ArrayList<>();
		for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
			long recordsDoctor = recordRep.countByMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Timestamp(doctorId,
					java.sql.Date.valueOf(date));
			doctorRecordsPerDay.add(recordsDoctor);
		}
		return doctorRecordsPerDay;
	}

	/**
	 * Given a time period and doctor's id, retunrs a list of total visitation time
	 * done by said doctor per day in the period provided
	 * 
	 * @param doctorId
	 *            - points to doctor in question
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exclusive, not in the future
	 * @return a List<Long> - number for each day in chronological order
	 */
	public List<Long> getDoctorVisitTimeStat(Long doctorId, LocalDate startDate, LocalDate endDate) {
		logger.info("getDoctorVisitTimeStat called with doctor id " + doctorId + ", start date " + startDate
				+ ", end date " + endDate);
		StatHelper.checkDates(startDate, endDate);
		List<Long> totalVisitTimePerDay = new ArrayList<>();
		for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
			List<MedicalHistory> recordList = recordRep.findByMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Timestamp(
					doctorId, java.sql.Date.valueOf(date));
			long totalVisitTime = recordList.stream().mapToLong(a -> a.getAppointmentLength()).sum();
			totalVisitTimePerDay.add(totalVisitTime);
		}
		return totalVisitTimePerDay;
	}

}
