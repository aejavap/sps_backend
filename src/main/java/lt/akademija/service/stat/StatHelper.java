package lt.akademija.service.stat;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Helper class containing methods dealing with inputa data validation re:
 * statistics services
 *
 */
public class StatHelper {

	/**
	 * Some semi-arbitrary lowest date that assumes no records prior exist in e form
	 */
	static LocalDate minDate = LocalDate.of(1990, 1, 1);

	/**
	 * Method checking that time period < 1 year. Also, invokes both
	 * checkMinDate(startDate) and checkMaxDate(startDate). Throws custum
	 * IllegalArgumentException if check fails.
	 * 
	 * @param startDate
	 * @param endDate
	 */
	public static void checkDates(LocalDate startDate, LocalDate endDate) {
		checkMinDate(startDate);
		checkMaxDate(endDate);
		if (endDate.isBefore(startDate) || endDate.equals(startDate)) {
			throw new IllegalArgumentException("End date has to come after start date.");
		}
		long daysBetween = java.time.temporal.ChronoUnit.DAYS.between(startDate, endDate);
		if (daysBetween > 366) {
			throw new IllegalArgumentException("Maximum period of time is a year.");
		}
	}

	/**
	 * Check that startDate is not before minumum allowed date. Throws
	 * IllegalArgumentException if check fails
	 * 
	 * @param startDate
	 */
	static void checkMinDate(LocalDate startDate) {
		if (startDate.isBefore(minDate)) {
			throw new IllegalArgumentException("Start date has to be after 1990-01-01");
		}
	}

	/**
	 * Check that endDate is not in the future. Throws IllegalArgumentException if
	 * check fails.
	 * 
	 * @param endDate
	 */
	static void checkMaxDate(LocalDate endDate) {
		LocalDate tomorrow = LocalDate.now().plus(1, ChronoUnit.DAYS);
		if (tomorrow.isBefore(tomorrow)) {
			throw new IllegalArgumentException("Not dealing with future today. Please provide a valid end date.");
		}
	}

}
