package lt.akademija.service.stat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.repository.MedicalHistoryRepository;
import lt.akademija.repository.PrescriptionFillsRepository;
import lt.akademija.repository.users.DoctorRepository;

/**
 * Service providing healthcare system-wide statistics.
 *
 */
@Service
public class SystemStatService {

	private static final Logger logger = LogManager.getLogger(SystemStatService.class);

	@Autowired
	MedicalHistoryRepository recordRep;

	@Autowired
	PrescriptionFillsRepository prescriptionRep;

	@Autowired
	DoctorRepository doctorRep;

	/**
	 * Given a time period, returns a list of: average number of medical records
	 * processed per doctor per day in the system.
	 * 
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exlusive, not in the future
	 * @return - List<Double>
	 */
	public List<Double> getDoctorRecordsStat(LocalDate startDate, LocalDate endDate) {
		logger.info("getDoctorRecordsStat in SystemStatService called with start date " + startDate + ", end date "
				+ endDate);
		StatHelper.checkDates(startDate, endDate);
		List<Double> recordsPerDoctorPerDay = new ArrayList<>();
		long numberOfDoctors = doctorRep.count();
		for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
			Double recordsDoctor = (double) recordRep.countByMedicalHistoryPK_Timestamp(java.sql.Date.valueOf(date));
			recordsDoctor = recordsDoctor / numberOfDoctors;
			recordsPerDoctorPerDay.add(recordsDoctor);
		}
		return recordsPerDoctorPerDay;
	}

	/**
	 * Given a time period, returns a list of: average number of total visitation
	 * time per doctor per day in the system.
	 * 
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exlusive, not in the future
	 * @return - List<Double>
	 */
	public List<Double> getDoctorVisitTimeStat(LocalDate startDate, LocalDate endDate) {
		logger.info("getDoctorVisitTimeStat called in SystemStatService with start date " + startDate + ", end date "
				+ endDate);
		StatHelper.checkDates(startDate, endDate);
		List<Double> totalVisitTimePerDoctorPerDay = new ArrayList<>();
		long numberOfDoctors = doctorRep.count();
		for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
			List<MedicalHistory> recordList = recordRep.findByMedicalHistoryPK_Timestamp(java.sql.Date.valueOf(date));
			Double totalVisitTime = recordList.stream().mapToDouble(a -> a.getAppointmentLength()).sum();
			totalVisitTime = totalVisitTime / numberOfDoctors;
			totalVisitTimePerDoctorPerDay.add(totalVisitTime);
		}
		return totalVisitTimePerDoctorPerDay;
	}

	/**
	 * Given a time period, returns top diagnoses made in non-repeat visitations per
	 * day.
	 * 
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exlusive, not in the future
	 * @param pageRequest
	 *            - used for size parameter to get top n diagnoses
	 * @return - HashMap<String diagnosisTitle, Long countOfDiagnosesMade>
	 * @throws Exception
	 */
	public HashMap<String, Long> getTopDiagnoses(LocalDate startDate, LocalDate endDate, Pageable pageRequest)
			throws Exception {
		logger.info("GetTopDiagnoses invoked");
		StatHelper.checkMinDate(startDate);
		StatHelper.checkMaxDate(endDate);
		List<String> descriptionList = recordRep.getTopDiagnosesDescriptions(java.sql.Date.valueOf(startDate),
				java.sql.Date.valueOf(endDate), pageRequest).getContent();
		List<Long> countList = recordRep
				.getTopDiagnosesCounts(java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate), pageRequest)
				.getContent();
		HashMap<String, Long> diagnosesMap = new HashMap<String, Long>();
		Iterator<String> descriptionIterator = descriptionList.iterator();
		Iterator<Long> countIterator = countList.iterator();
		while (descriptionIterator.hasNext() && countIterator.hasNext()) {
			diagnosesMap.put(descriptionIterator.next(), countIterator.next());
		}
		if (descriptionIterator.hasNext() || countIterator.hasNext()) {
			throw new Exception("Top diagnoses ids and counts of differing size");
		}
		;
		return diagnosesMap;
	}

	/**
	 * Given a time periode, returns top active ingredients featuring in
	 * prescriptions filled in said period as well as numbers of total prescriptions
	 * filled with listed ingredient title.
	 * 
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exlusive, not in the future
	 * @param pageRequest
	 * @return - HashMap<String ingredientTitle, Long
	 *         numberOfPrescriptionsWithIngredient>
	 * @throws Exception
	 */
	public HashMap<String, Long> getTopIngredients(LocalDate startDate, LocalDate endDate, Pageable pageRequest)
			throws Exception {
		logger.info("GetTopIngredients invoked");
		StatHelper.checkMinDate(startDate);
		StatHelper.checkMaxDate(endDate);
		List<String> titleList = prescriptionRep
				.getTopIngredientTitles(java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate), pageRequest)
				.getContent();
		List<Long> countList = prescriptionRep
				.getTopIngredientCounts(java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate), pageRequest)
				.getContent();
		HashMap<String, Long> ingredientsMap = new HashMap<String, Long>();
		Iterator<String> titleIterator = titleList.iterator();
		Iterator<Long> countIterator = countList.iterator();
		while (titleIterator.hasNext() && countIterator.hasNext()) {
			ingredientsMap.put(titleIterator.next(), countIterator.next());
		}
		if (titleIterator.hasNext() || countIterator.hasNext()) {
			throw new Exception("Top ingredients ids and counts of differing size");
		}
		;
		return ingredientsMap;
	}

	/**
	 * Given a time periode, returns top non-repeat visitation diagnoses made in
	 * said period as well as the relevant percentagesof all non-repeat daignoses
	 * made.
	 * 
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exlusive, not in the future
	 * @param pageRequest
	 * @return - HashMap<String diagnosisTitle, Double percentageOfAllDiagnoses>
	 * @throws Exception
	 */
	public HashMap<String, Double> getTopDiagnosesPc(LocalDate startDate, LocalDate endDate, Pageable pageRequest)
			throws Exception {
		logger.info("GetTopDiagnosesPc invoked");
		StatHelper.checkMinDate(startDate);
		StatHelper.checkMaxDate(endDate);
		List<String> descriptionList = recordRep.getTopDiagnosesDescriptions(java.sql.Date.valueOf(startDate),
				java.sql.Date.valueOf(endDate), pageRequest).getContent();
		List<Long> countList = recordRep
				.getTopDiagnosesCounts(java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate), pageRequest)
				.getContent();
		List<Double> countListDoubles = new ArrayList<>();
		long numberOfDiagnoses = recordRep.countNonRepeat(java.sql.Date.valueOf(startDate),
				java.sql.Date.valueOf(endDate));
		for (Long number : countList) {
			Double doubleNumber = number.doubleValue();
			doubleNumber = doubleNumber / numberOfDiagnoses;
			countListDoubles.add(doubleNumber);
		}
		HashMap<String, Double> diagnosesMap = new HashMap<String, Double>();
		Iterator<String> descriptionIterator = descriptionList.iterator();
		Iterator<Double> countIterator = countListDoubles.iterator();
		while (descriptionIterator.hasNext() && countIterator.hasNext()) {
			diagnosesMap.put(descriptionIterator.next(), countIterator.next());
		}
		if (descriptionIterator.hasNext() || countIterator.hasNext()) {
			throw new Exception("Top diagnoses ids and counts of differing size");
		}
		;
		return diagnosesMap;
	}

	/**
	 * Given a time periode, returns top active ingredients featuring in
	 * prescriptions fills within said period as well as the relevant percentage of
	 * all prescriptions filled with said ingredients.
	 * 
	 * @param startDate
	 *            - inclusive, after 1990
	 * @param endDate
	 *            - exlusive, not in the future
	 * @param pageRequest
	 * @return - HashMap<String ingredientTitle, Double
	 *         percentageOfAllPrescriptions>
	 * @throws Exception
	 */
	public HashMap<String, Double> getTopIngredientsPc(LocalDate startDate, LocalDate endDate, Pageable pageRequest)
			throws Exception {
		logger.info("GetTopDiagnosesPc invoked");
		StatHelper.checkMinDate(startDate);
		StatHelper.checkMaxDate(endDate);
		List<String> titleList = prescriptionRep
				.getTopIngredientTitles(java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate), pageRequest)
				.getContent();
		List<Long> countList = prescriptionRep
				.getTopIngredientCounts(java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate), pageRequest)
				.getContent();
		List<Double> countListDoubles = new ArrayList<>();
		long numberOfIngredients = prescriptionRep.countInPeriod(java.sql.Date.valueOf(startDate),
				java.sql.Date.valueOf(endDate));
		for (Long number : countList) {
			Double doubleNumber = number.doubleValue();
			doubleNumber = doubleNumber / numberOfIngredients;
			countListDoubles.add(doubleNumber);
		}
		HashMap<String, Double> ingredientsMap = new HashMap<String, Double>();
		Iterator<String> titleIterator = titleList.iterator();
		Iterator<Double> countIterator = countListDoubles.iterator();
		while (titleIterator.hasNext() && countIterator.hasNext()) {
			ingredientsMap.put(titleIterator.next(), countIterator.next());
		}
		if (titleIterator.hasNext() || countIterator.hasNext()) {
			throw new Exception("Top ingredients ids and counts of differing size");
		}
		;
		return ingredientsMap;
	}

}
