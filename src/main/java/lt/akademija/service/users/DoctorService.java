package lt.akademija.service.users;

import lt.akademija.model.dto.users.DoctorDTO;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.repository.users.DoctorRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DoctorService extends GenericUserService<Doctor, DoctorDTO> {

	public DoctorService() {
		super(Doctor.class, DoctorDTO.class);
	}

//	public Page<DoctorDTO> getAllDoctorsBySpecializationId(Pageable pageRequest) {
//		return ((DoctorRepository) genericRepository).findAllByOrderBySpecializationAsc(pageRequest)
//				.map(DoctorDTO::toDTO);
//	}

}
