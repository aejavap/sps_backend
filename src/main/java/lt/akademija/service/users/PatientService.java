package lt.akademija.service.users;

import lt.akademija.model.dto.search.PatientSearchDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.PatientRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

/**
 * Patient service works with patient repository. And retrieves patients
 * according to various conditions
 */

@Service
public class PatientService extends GenericUserService<Patient, PatientDTO> {

	private static final Logger logger = LogManager.getLogger(PatientService.class);

	public PatientService() {
		super(Patient.class, PatientDTO.class);
	}

	/**
	 * Find patients by four parameters: name, surname, pid and diagnosis; Method
	 * work for partial search;
	 *
	 * @param patient
	 *            - patient dto with defined aforementioned fields;
	 * @param pageRequest
	 *            - paging information;
	 * @return - patients matching given criteria;
	 */
	public Page<PatientDTO> searchByNameAndSurnameAndPidAndDiagnosis(PatientSearchDTO patient, Pageable pageRequest)
			throws Exception {

		String firstName = "";
		String lastName = "";
		String pidString = "";

		if ((patient.getPid() == null) || (patient.getPid() < 1)) {
			logger.debug("Personal was not passed");
			pidString = "";
		} else {
			pidString = String.valueOf(patient.getPid());
			logger.debug("Personal id passed: " + pidString);
		}
		if (patient.getFirstName() != null) {
			firstName = patient.getFirstName().trim();
			logger.debug("FirstName passed: " + firstName);
		}
		if (patient.getLastName() != null) {
			lastName = patient.getLastName().trim();
			logger.debug("LastName passed: " + lastName);
		}

		String diagnosisTitle = patient.getDiagnosisTitle();
		Long doctorId = patient.getDoctorId();
		if (doctorId == null) {
			logger.debug("Doctor id null passed");
			throw new Exception("Doctor id null passed");
		}

		if ((patient.getDiagnosisTitle() == null) || (patient.getDiagnosisTitle().isEmpty())) {
			logger.debug("SearchByNameAndSurnameAndPid called");
			return ((PatientRepository) genericRepository)
					.searchByNameAndSurnameAndPid(firstName, lastName, pidString, patient.getDoctorId(), pageRequest)
					.map(PatientDTO::toDTO);
		} else {
			logger.debug("SearchByNameAndSurnameAndPidAndDiagnosis called");
			return ((PatientRepository) genericRepository).searchByNameAndSurnameAndPidAndDiagnosis(firstName, lastName,
					pidString, diagnosisTitle, doctorId, pageRequest).map(PatientDTO::toDTO);
		}
	}

	/**
	 * Finds all patients prescribed to doctor;
	 *
	 * @param doctorId
	 *            - doctor id;
	 * @return list of patients;
	 */

	public List<PatientDTO> findPatientsByDoctorId(Long doctorId) {
		// List<PatientDTO> dtoList = new ArrayList<>();

		List<PatientDTO> dtoList = ((PatientRepository) genericRepository).findPatientListByDoctorId(doctorId)
				.parallelStream().map(PatientDTO::toDTO).collect(Collectors.toList());

		// for (Patient patient : entitytList) {
		// PatientDTO dto = PatientDTO.toDTO(patient);
		// dtoList.add(dto);
		// }
		return dtoList;
	}

	/**
	 * Find patient by its pid.
	 *
	 * @param pid
	 *            - patient pid;
	 * @return - patient dto;
	 */
	public PatientDTO findByPidDTO(Long pid) {
		return PatientDTO.toDTO(genericRepository.findOneByPid(pid));
	}

	/**
	 * Creates patient from its DTO;
	 *
	 * @param patientDTO
	 *            - patient DTO;
	 * @return created patient DTO;
	 * @throws Exception
	 *             - if unsuccessful throws Exception;
	 */
	public PatientDTO createDTO(PatientDTO patientDTO) throws Exception {
		return ((PatientRepository) genericRepository).saveDTO(patientDTO);
	}

	/**
	 * Find all patients by their date of birth;
	 *
	 * @param pageRequest
	 *            - page information;
	 * @return - paged patient dto;
	 */
	public Page<PatientDTO> findAllByDob(Pageable pageRequest) {
		return ((PatientRepository) genericRepository).findAllByOrderByDobAsc(pageRequest).map(PatientDTO::toDTO);
	}

	// Keistas

	/**
	 * Finds patient list by doctors pid;
	 *
	 * @param pid
	 *            - doctors pid;
	 * @param pageRequest
	 *            - paging information;
	 * @return - paged patient dto;
	 */
	public Page<PatientDTO> findPatientList(Long pid, Pageable pageRequest) {
		return ((PatientRepository) genericRepository).findPatientListByDoctorPid(pid, pageRequest)
				.map(PatientDTO::toDTO);
	}

	/**
	 * Find patient all medical records by patients pid.
	 *
	 * @param pageRequest
	 *            - paging information;
	 * @param pid
	 *            - patient pid;
	 * @return paged medical records;
	 */
	public Page<MedicalHistory> findMedicalRecords(Pageable pageRequest, Long pid) {
		return ((PatientRepository) genericRepository).findMedicalRecordsByPid(pageRequest, pid);
	}

	/**
	 * Update patient by giving patient id and patient DTO
	 *
	 * @param id
	 *            - patient id;
	 * @param entity
	 *            - patient DTO
	 * @return - patient DTO with updated information
	 * @throws Exception
	 *             - if patient not found;
	 */
	public PatientDTO updateByIdDTO(Long id, PatientDTO entity) throws Exception {
		PatientDTO user = ((PatientRepository) genericRepository).updateDTOById(id, entity);
		logger.info("PatientDTO date in patientsService: " + user.getDob());
		return user;
	}

	/**
	 * Find all patiet precrisptions;
	 *
	 * @param pageRequest
	 *            - paging information;
	 * @param pid
	 *            - patient pid;
	 * @return paged prescriptions;
	 **/
	public Page<Prescription> findPrescriptions(Pageable pageRequest, Long pid) {
		return ((PatientRepository) genericRepository).findPrescriptionListByPid(pageRequest, pid);
	}

}
