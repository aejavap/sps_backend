package lt.akademija.service.users;

import lt.akademija.model.dto.users.AdminDTO;
import lt.akademija.model.entity.users.Admin;

import org.springframework.stereotype.Service;

@Service
public class AdminService extends GenericUserService<Admin, AdminDTO> {

	public AdminService() {
		super(Admin.class, AdminDTO.class);
	}
}
