package lt.akademija.service.users;

import lt.akademija.model.dto.search.BasicUserSearchDTO;
import lt.akademija.model.dto.users.AbstractUserDTO;
import lt.akademija.model.entity.users.AbstractUser;
import lt.akademija.repository.users.GenericUserRepository;

import lt.akademija.repository.users.RepositoryCustom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.Type;

/**
 * Generic class used for User entities, contains search by pid and id.
 *
 * @param <T> - entity, extending abstract user
 */

abstract public class GenericUserService<T extends AbstractUser, U extends AbstractUserDTO> {

    private static Logger logger = LogManager.getLogger(GenericUserService.class);

    private Class<T> type;

    private Class<U> DTO;

    GenericUserService(Class<T> type, Class<U> DTO) {
        this.type = type;
        this.DTO = DTO;
    }

    /*******************************************************************************************************
     * *********************************FIND METHODS******************************************************
     * *****************************************************************************************************
     */

    @Autowired
    protected GenericUserRepository<T> genericRepository;

    /**
     * Finds all entities
     *
     * @param pageRequest - carries paging information;
     * @return - paged results;
     */
    public Page<U> findAllDTO(Pageable pageRequest) throws Exception {
        logger.info("FindAllDTO called.");
        Page<T> found = genericRepository.findAll(pageRequest);
        Page<U> convertedToDTO = found.map(entity -> U.toDTO(entity, DTO));
//        if (convertedToDTO == null) {
//            logger.error("Exception in findAllDTO: ");
//            throw new Exception("Unable to retrieve paged user entities in findAll");
//        }
        return convertedToDTO;
    }

    /**
     * Finds all entities
     *
     * @param pageRequest - carries paging information;
     * @return - paged results;
     */
    public Page<T> findAll(Pageable pageRequest) {
        return genericRepository.findAll(pageRequest);
    }

    /**
     * Method implementation of Find interface, used to convert DTO to entity
     *
     * @param id - objects id in the table
     * @return entity
     * @throws Exception
     */

    public U findByIdDTO(Long id) throws Exception {
        logger.debug("FindByIdDTO called with id: " + id);
        T found = genericRepository.findOne(id);
//        if (found == null) {
//            logger.error("No " + type.getSimpleName() + " with id " + id + " was found.");
//            throw new Exception("No " + type.getSimpleName() + " with id " + id + " was found.");
//        }
        U convertedToDTO = U.toDTO(found, DTO);
        return convertedToDTO;
    }

    /**
     * Method implementation of Find interface, used to convert DTO to entity
     *
     * @param id - objects id in the table
     * @return entity
     */

    public T findById(Long id) throws Exception {
        logger.info("FindById called: " + id);
        T found = genericRepository.findOne(id);
//        if (found == null) {
//            logger.error("No " + type.getSimpleName() + " with id " + id + " was found.");
//            throw new Exception("No " + type.getSimpleName() + " with id " + id + " was found.");
//        }
        return found;
    }

    /**
     * Finds Entity DTO by pid;
     *
     * @param pid - entity pid;
     * @return entity DTO;
     */

    // @Override
    public U findByPidDTO(Long pid) throws Exception {

        logger.info("findByPidDTO called by: " + pid);
        T found = genericRepository.findOneByPid(pid);
//        if (found == null) {
//            logger.error("No " + type.getSimpleName() + " with pid " + pid + " was found.");
//            return null;
////            throw new Exception("No " + type.getSimpleName() + " with pid " + pid + " was found.");
//        }
        U convertedToDTO = U.toDTO(found, DTO);
        return convertedToDTO;
    }

    /**
     * Finds Entity by pid;
     *
     * @param pid - entity pid;
     * @return entity;
     */

    public T findByPid(Long pid) throws Exception {
        T found = genericRepository.findOneByPid(pid);
//        if (found == null) {
//            logger.error("No " + type.getSimpleName() + " with pid " + pid + " was found.");
//            throw new Exception("No " + type.getSimpleName() + " with pid " + pid + " was found.");
//        }
        return found;
    }
//
//    /**
//     * Find entity by id;
//     *
//     * @param id - entity id;
//     * @return - found entity;
//     */
//
//    public T findById(Long id) {
//        T found = genericRepository.findOne(id);
//        if (found == null) {
//            logger.error("No " + type.getSimpleName() + " with id " + id + " was found.");
//            new Exception("No " + type.getSimpleName() + " with id " + id + " was found.");
//        }
//        return found;
//    }


    /**
     * Method used to create entity in the db
     *
     * @param entity - passed entity
     * @return if successful returns, created entity
     * @throws Exception
     */

    /*******************************************************************************************************
     * *********************************CREATE METHODS******************************************************
     * *****************************************************************************************************
     */


    public U createUser(U entity) throws Exception {
        logger.info(entity.toString());
        logger.info("Create in generic service called");
        // entity.setId(null);
        @SuppressWarnings("unchecked")
        U entityAfterSave = ((RepositoryCustom<U>) genericRepository).saveDTO(entity);
        return entityAfterSave;
    }

    @SuppressWarnings("unchecked")
    public T createEntity(U DTO) throws Exception {
        logger.info(DTO.toString());
        logger.info("Create in generic service called");
        DTO.setId(null);
        DTO =((RepositoryCustom<U>) genericRepository).saveDTO(DTO);
        T entityAfterSave = genericRepository.findOne(DTO.getId());
        return entityAfterSave;
    }

    public void removeById(Long id) {
        genericRepository.delete(id);
    }

    /*******************************************************************************************************
     * *********************************UPDATE METHODS******************************************************
     * *****************************************************************************************************
     */

    /**
     * Updates entity by pid;
     *
     * @param pid    - entity pid;
     * @param entity - updated version of entity;
     * @return entity;
     * @throws Exception
     */
    public U updateByPid(Long pid, U entity) throws Exception {
        logger.info("Update started");
        Long newPid = entity.getPid();
        logger.info("newPid: " + newPid);
        T anotherWithSamePid = genericRepository.findByPidNotWithPid(newPid, pid);
        if (anotherWithSamePid == null) {
            @SuppressWarnings({"rawtypes", "unchecked"})
            U userUpdated = (U) ((RepositoryCustom) genericRepository).updateDTOByPid(pid, entity);
            logger.debug("user updated: " + userUpdated);
            return userUpdated;
        } else {
            logger.error("Another " + type.getSimpleName() + " with pid " + newPid + " already exists.");
            throw new Exception("Another " + type.getSimpleName() + " with pid " + newPid + " already exists.");
        }
    }

    /**
     * Method used to update entity, by its id
     *
     * @param id     - entity id
     * @param entity - updated version of entity
     * @return if successful returns updated entity
     */

    public U update(Long id, U entity) throws Exception {
        logger.debug("Update started");
        Long newPid = entity.getPid();
        logger.debug("newPid: " + newPid);
        T anotherWithSamePid = genericRepository.findByPidNotWithId(newPid, id);
        logger.debug("anotherWithSamePid: " + anotherWithSamePid);
        if (anotherWithSamePid == null) {
            @SuppressWarnings({"rawtypes", "unchecked"})
            U user = (U) ((RepositoryCustom) genericRepository).updateDTOById(id, entity);
            logger.debug("user updated: " + user);
            return user;
        } else {
            logger.error("Another " + type.getSimpleName() + " with pid " + newPid + " already exists.");
            throw new Exception("Another " + type.getSimpleName() + " with pid " + newPid + " already exists.");
        }
    }

    /*******************************************************************************************************
     * *********************************SEARCH METHODS******************************************************
     * *****************************************************************************************************
     */

    /**
     * Finds user by combination of regex applied to its firstname, lastname and pid
     * fields. The fields may only start with text or numbers typed
     *
     * @param pageRequest - paging info;
     * @return - paged result;
     */
    public Page<U> searchUsersByNameOrSurnameOrPid(BasicUserSearchDTO searchDTO, Pageable pageRequest) {

        logger.info("#################################");
        logger.info("Calling generic user service");
        logger.info("#################################");

        // make sure that even if parameters were not given, search continues
        String pidString = "";
        String firstName = "";
        String lastName = "";
        if (searchDTO.getPid() == null || searchDTO.getPid() == 0) {
            // pass
            logger.debug("Pid was not supplied for the search.");
        } else {
            pidString = String.valueOf(searchDTO.getPid());
        }
        if (searchDTO.getFirstName() != null) {
            logger.debug("FirstName null passe");
            firstName = searchDTO.getFirstName().trim();
        }
        if (searchDTO.getLastName() != null) {
            logger.debug("LastName null passe");
            lastName = searchDTO.getLastName().trim();
        }

        logger.info("#################################");
        logger.info("pidString: " + pidString);
        logger.info("firstName: " + firstName);
        logger.info("lastName: " + lastName);
        logger.info("#################################");

        logger.info("Sorting request: " + pageRequest.getSort());
        return genericRepository.searchByNameOrSurnameOrPid(firstName, lastName, pidString, pageRequest)
                .map(entity -> U.toDTO(entity, DTO));
    }

    /*******************************************************************************************************
     * *********************************HELPER METHODS******************************************************
     * *****************************************************************************************************
     */

    /**
     * Method implementation of Find interface. Method is used to determine if class
     * can operate with desired objects
     *
     * @param type - Object class
     * @return returns true if service can handle desired class
     */

    // @Override
    public boolean canHandle(Type type) {
        return this.type.equals(type);
    }

    /**
     * Class has info on entity it works on. This method returns entity class, this
     * service works on.
     *
     * @return - class of the object service works on
     */
    public Class<T> getType() {
        return type;
    }

    /*******************************************************************************************************
     * *********************************NOT USED METHODS******************************************************
     * *****************************************************************************************************
     */


    /*
    public Page<U> findUserByLastName(String lastName, Pageable pageRequest) {
        return genericRepository.findAllByLastNameStartingWithIgnoreCase(lastName, pageRequest)
                .map(entity -> U.toDTO(entity, DTO));
    }

    public Page<U> findUserByFirstName(String firstName, Pageable pageRequest) {
        return genericRepository.findAllByFirstNameStartingWithIgnoreCase(firstName, pageRequest)
                .map(entity -> U.toDTO(entity, DTO));
    }

    public Page<U> findAllById(Pageable pageRequest) {
        return ((GenericUserRepository<T>) genericRepository).findAllByOrderByIdAsc(pageRequest)
                .map(entity -> U.toDTO(entity, DTO));
    }

    public Page<U> findAllByPid(Pageable pageRequest) {
        return genericRepository.findAllByOrderByPidAsc(pageRequest).map(entity -> U.toDTO(entity, DTO));
    }

    public Page<U> findAllByLastName(Pageable pageRequest) {
        return genericRepository.findAllByOrderByLastNameAsc(pageRequest).map(entity -> {
            System.out.println("Object called: " + DTO);
            return U.toDTO(entity, DTO);
        });
    }*/
}
