package lt.akademija.service.Facade;

import lombok.Data;
import lt.akademija.model.dto.AccountDTO;
import lt.akademija.model.dto.AccountDTOFull;
import lt.akademija.model.dto.search.BasicUserSearchDTO;
import lt.akademija.model.dto.users.AbstractUserDTO;
import lt.akademija.model.dto.users.DoctorDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.simple.AbstractSimpleEntity;
import lt.akademija.model.entity.users.AbstractUser;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.service.AccountService;
import lt.akademija.service.simple.GenericSimpleService;
import lt.akademija.service.users.GenericUserService;
import lt.akademija.service.users.PatientService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.security.auth.login.AccountException;
import javax.transaction.Transactional;

import java.util.List;

/**
 * A facade providing a unified and simplified interface for all complex
 * functionality needed by the admin user.
 * <p>
 * <p>
 * Admin facade controls all classes extending GenericUserDTOService, which are
 * responsible, for operation with users. Classes representing users usually
 * contain user name in it. The choice of the correct user service is based on
 * that.
 */
@Service
@Data
public class AdminFacade {

	private static final Logger logger = LogManager.getLogger(AdminFacade.class);

	@Autowired
	private AccountService accountService;

	// Collect all DTO projection services which are initialized by classes
	// extending AbstractUserDTO;
	@Autowired
	private List<GenericUserService<?, ? extends AbstractUserDTO>> userServiceList;

	// Collect all classes extending GSimpleService
	@Autowired
	private List<? extends GenericSimpleService<? extends AbstractSimpleEntity>> simpleServiceList;

	/**
	 * Method lists all autowired classes
	 */
	@SuppressWarnings("rawtypes")
	@PostConstruct
	void listAll() {
		logger.info("listAll() in AdminFacade called. ");
		logger.info("These are the autowired GenericUserServices");
		for (GenericUserService gen : userServiceList)
			logger.info(gen.getType().getSimpleName());
	}

	/**
	 * Checks whether user type passed by url is contained in the class name used to
	 * represent it in the code.
	 *
	 * @param className
	 *            - class name short form string;
	 * @param url
	 *            - class name posted in the url;
	 * @return object DTO projection;
	 */
	private boolean canHandle(String className, String url) {
		return className.toLowerCase().contains(url);
	}

	/**
	 * Finds which method can work with given user type.
	 *
	 * @param url
	 *            - class name posted in the url;
	 * @param <T>
	 *            - DTO class extending AbstractUserDTO;
	 * @return object DTO projection;
	 */
	@SuppressWarnings("unchecked")
	private <T extends AbstractUserDTO> GenericUserService<?, T> chooseUserService(String url) throws Exception {
		String className;
		for (GenericUserService<?, ? extends AbstractUserDTO> user : userServiceList) {
			className = user.getType().getSimpleName();
			if (canHandle(className, url)) {
				return (GenericUserService<?, T>) user;
			}
		}
		throw new Exception(
				"Could not determine which service to use. Make sure \"user\" parameter is given without typos.");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <G extends GenericSimpleService<? extends AbstractSimpleEntity>> G chooseSimpleService(String entityType)
			throws Exception {
		entityType = entityType.replaceAll("\\s+", "").toLowerCase();
		String serviceType = null;
		for (GenericSimpleService service : simpleServiceList) {
			serviceType = service.getClass().getSimpleName().toLowerCase();
			if (serviceType.contains(entityType)) {
				return (G) service;
			}
		}
		throw new Exception(
				"Could not determine simple service to use. Make sure \"entityType\" parameter is given without typos");
	}

	// #####################################################
	// ############## GET METHODS #############################
	// #####################################################

	/**
	 * Returns a user by id given
	 *
	 * @param className
	 *            - class name short form string;
	 * @param userId
	 *            - user id in the db;
	 * @param <T>
	 *            - DTO object class;
	 * @return DTO projection;
	 * @throws Exception
	 *             if user type with id provided does not exist
	 */
	@SuppressWarnings("unchecked")
	public <T extends AbstractUserDTO> T getUserDetailsById(String className, Long userId) throws Exception {
		logger.info("getUserDetailsById in AdminFacade invoked with className " + className + " and userId " + userId);
		if (userId == null || userId < 1) {
			throw new IllegalArgumentException("UserId cannot be null or < 1.");
		}
		logger.debug("User id was given");
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		logger.debug("Service chosen: " + service.getClass().getSimpleName());
		if (service.canHandle(Patient.class)) {
			PatientDTO patient = (PatientDTO) service.findByIdDTO(userId);
			patient.setHasAccount(accountService.existsById(patient.getId()));
			return (T) patient;

		}
		return service.findByIdDTO(userId);
	}

	/**
	 * Returns a user by pid provided
	 *
	 * @param className
	 *            - class name short form string;
	 * @param pid
	 *            - personal id of user;
	 * @param <T>
	 *            - DTO object class;
	 * @return DTO projection;
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> T getUserDetailsByPid(String className, Long pid) throws Exception {
		logger.info("getUserDetailsByPid in AdminFacade invoked with className " + className + " and user pid " + pid);
		if (pid == null || pid < 1) {
			throw new IllegalArgumentException("Pid cannot be null or < 1");
		}
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		logger.debug("Service chosen: " + service.getClass().getSimpleName());
		return service.findByPidDTO(pid);
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws AccountException
	 *             if account with id provided does not exist
	 */
	public AccountDTOFull getAccountById(Long userId) throws AccountException {
		logger.info("getAccountById was invoked in AdminFacade");
		if (userId == null || userId < 1) {
			throw new IllegalArgumentException("UserId cannot be null or < 1");
		}
		try {
			return accountService.getAccountDTOById(userId);
		} catch (AccountException e) {
			throw e;
		}
	}

	// #########################################################
	// ################# SEARCH METHODS ########################
	// #########################################################

	/**
	 * Finds user by combination of regex applied to its firstname, lastname and pid
	 * fields. The fields may only start with text or numbers typed
	 *
	 * @param className
	 *            - name of the user grupe, where the search should be executed; //
	 *            * @param params - parameters passed in the Array of Strings.
	 *            !.FirstName; 2.LastName; 3.Pid;
	 * @param pageRequest
	 *            - paging info;
	 * @return - paged result;
	 * @throws Exception
	 */

	@SuppressWarnings("unchecked")
	public <T extends AbstractUserDTO> Page<T> searchUsersByNameOrSurnameOrPid(String className,
			BasicUserSearchDTO searchDTO, Pageable pageRequest) throws Exception {
		logger.info("searchUsersByNameOrSurnameOrPid invoked in AbstractFacade with className " + className + ", dto "
				+ searchDTO);
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		logger.debug("Service chosen: " + service.getClass().getSimpleName());
		if (service.canHandle(Patient.class)) {
			Page<PatientDTO> patientDTO = (Page<PatientDTO>) service.searchUsersByNameOrSurnameOrPid(searchDTO,
					pageRequest);
			patientDTO.forEach(p -> p.setHasAccount(accountService.existsById(p.getId())));
			return (Page<T>) patientDTO;
		} else {
			Page<T> users = service.searchUsersByNameOrSurnameOrPid(searchDTO, pageRequest);
			logger.info("Returning search results");
			return users;
		}

	}

	// ########################################################
	// ############ CREATE METHODS ############################
	// ########################################################

	/**
	 * Create a new Patient without an associated account;
	 *
	 * @param <T>
	 *            - user entity projection class, extending AbstractUserDTO;
	 * @return - returns relevant user id
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> Long createPatient(PatientDTO patientDTO) throws Exception {
		logger.info("createPatient in AdminFacade invoked with dto " + patientDTO);
		GenericUserService<?, PatientDTO> patientService = chooseUserService("patient");
		GenericUserService<?, DoctorDTO> doctorService = chooseUserService("doctor");

		Long givenPid = patientDTO.getPid();
		Patient patientWithGivenPid = ((PatientService) patientService).findByPid(givenPid);
		if (!(patientWithGivenPid == null)) {
			throw new IllegalArgumentException("patient with pid " + givenPid + " already exists.");
		}
		logger.debug("givenPid not in use");

		Long doctorId = patientDTO.getDoctorId();
		Long doctorPid = patientDTO.getDoctorPid();
		if ((doctorId == null) || (doctorId <= 0)) {
			if ((doctorPid == null) || (doctorPid <= 0)) {
				logger.debug("Doctor was not provided. Creating patient without a doctor.");
			} else {
				DoctorDTO doctorFound = doctorService.findByPidDTO(doctorPid);
				logger.debug("Found doctor: " + doctorFound);
				if (doctorFound == null) {
					throw new IllegalArgumentException("No doctor with pid: " + doctorPid + " exists!");
				} else {
					patientDTO.setDoctorId(doctorFound.getId());
				}
			}
		} else {
			DoctorDTO doctorFound = doctorService.findByIdDTO(doctorId);
			if (doctorFound == null) {
				throw new IllegalArgumentException("No doctor with id: " + doctorId + " exists!");
			}
		}

		logger.debug("About to create a patient");
		logger.debug("Patient dto at this point: " + patientDTO);
		PatientDTO patientCreated = patientService.createUser(patientDTO);
		logger.info("Patient created");
		return patientCreated.getId();
	}

	/**
	 * Create new user with an associated account (except for patient); Patient
	 * account must be created on a separate method - createPatientAccount Username
	 * and password sent along with PatientDTO will be ignored
	 *
	 * @param className
	 *            - class name short form string; //* @param user - DTO projection
	 * @param <T>
	 *            - user entity projection class, extending AbstractUserDTO;
	 * @param username
	 *            - desired username
	 * @param password
	 *            - desired password
	 * @return - returns relevant user id
	 */
	@Transactional
	public <T extends AbstractUserDTO> Long createUser(String className, T userDTO, String username, String password)
			throws Exception {
		logger.info("Creating user of type " + className + " from dto " + userDTO);
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		logger.debug("Service chosen: " + service.getClass().getSimpleName());
		Long givenPid = userDTO.getPid();
		AbstractUser entityWithGivenPid = service.findByPid(givenPid);
		if (!(entityWithGivenPid == null)) {
			throw new Exception(className + " with pid " + givenPid + " already exists!");
		} else {
			Account userAccount = new Account();
			logger.debug("Passed username: " + username);
			userAccount.setUsername(username);
			userAccount.setPassword(password);
			userAccount.grantAuthority("ROLE_USER");
			switch (className) {
			case "doctor": {
				logger.debug("Creating doctor");
				userAccount.grantAuthority("ROLE_DOCTOR");
				break;
			}
			case "admin": {
				logger.debug("Creating an admin");
				userAccount.grantAuthority("ROLE_ADMIN");
				break;
			}
			case "pharmacist": {
				logger.debug("Creating a pharmacist");
				userAccount.grantAuthority("ROLE_PHARMACIST");
				break;
			}
			// if className was patient, other method supposed to be called
			case "patient":
				throw new Exception("patient ended up in the wrong method");
			}
			AbstractUser userCreated = service.createEntity(userDTO);
			logger.info("User created");
			Long userCreatedId = userCreated.getId();
			logger.debug("Id of user created: " + userCreatedId);
			try {
				accountService.createAccount(userAccount, userCreated);
				logger.info("Account created");
				return userCreatedId;
			} catch (AccountException e) {
				service.removeById(userCreatedId);
				throw e;
			}
		}
	}

	/**
	 * Creates and associates an account with the user provided
	 *
	 * @param patientId
	 *            - patient Id associated with the account to-be
	 * @param accountDTO
	 *            - contains username and password for the account to-be
	 * @return - returns relevant user id
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> Long createPatientAccount(Long patientId, AccountDTO accountDTO)
			throws Exception {
		logger.info("createPatientAccount in AdminFacade invoked with patient id " + patientId);
		if (accountService.existsById(patientId)) {
			throw new AccountException(String.format("Account with id [%s] already exists", patientId));
		}
		logger.debug("No account with given id " + patientId + " exists");
		GenericUserService<?, T> service = chooseUserService("patient");
		Patient patientFound = (Patient) service.findById(patientId);
		Account patientAccount = new Account();
		patientAccount.setUsername(accountDTO.getUsername());
		patientAccount.setPassword(accountDTO.getPassword());
		patientAccount.grantAuthority("ROLE_USER");
		patientAccount.grantAuthority("ROLE_PATIENT");
		Account accountCreated = accountService.createAccount(patientAccount, patientFound);
		logger.info("Patient account created");
		return accountCreated.getId();
	}

	public <T extends AbstractSimpleEntity> Long createSimpleEntity(String entityType, T entity) throws Exception {
		entity.setId(null);
		logger.info("createSimpleEntity in AdminFacade invoked with for type " + entityType + " with given model "
				+ entity);
		entityType = entityType.trim().toLowerCase();
		GenericSimpleService<T> serviceChosen = chooseSimpleService(entityType);
		String serviceCalled = serviceChosen.getClass().getSimpleName();
		String titleProvided = entity.getTitle();
		T entityWithSameTitle = serviceChosen.findByTitle(titleProvided);
		if (!(entityWithSameTitle == null)) {
			throw new Exception("Entity with title " + titleProvided + " already exists.");
		}
		logger.debug("Service chosen: " + serviceCalled);
		T entityCreated = serviceChosen.create(entity);
		logger.info("Entity created");
		return entityCreated.getId();
	}

	// #########################################################
	// ############ UPDATE METHODS #############################
	// #########################################################

	/**
	 * Updates username and password for the account specified.
	 *
	 * @param userId
	 *            - user id used to specify the account in question
	 * @param accountDTO
	 *            - DTO object containing
	 * @return - returns user id provided
	 */
	public Account updateUserAccountById(Long userId, AccountDTO accountDTO) throws AccountException {
		logger.info(
				"updateUserAccountById in AdminFacade invoked for user id " + userId + " with dto given " + accountDTO);
		return accountService.updateById(userId, accountDTO);
	}

	/**
	 * Finds user by its id and updates it
	 *
	 * @param className
	 *            - short string name of the class;
	 * @param id
	 *            - user id provided;
	 * @param user
	 *            - user DTO;
	 * @param <T>
	 *            - DTO class;
	 * @return user DTO;
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> T updateUserById(String className, Long id, T user) throws Exception {
		logger.info("updateUserById in AdminFacade invoked for user type " + className + ", id " + id
				+ " and given mode " + user);
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		logger.debug("Service chosen: " + service.getClass().getSimpleName());
		T patient = service.update(id, user);
		logger.info("User details updated");
		return patient;
	}

	/**
	 * Finds user by its pid and updates it
	 *
	 * @param className
	 *            - short string name of the class;
	 * @param pid
	 *            - user pid provided
	 * @param user
	 *            - userDTO
	 * @return userDTO;
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> T updateUserByPid(String className, Long pid, T user) throws Exception {
		logger.info("updateUserByPid in AdminFacade invoked for user type " + className + ", pid " + pid + " and mode "
				+ user);
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		logger.debug("Service chosen: " + service.getClass().getSimpleName());
		return service.updateByPid(pid, user);
	}

	public <T extends AbstractSimpleEntity> T updateSimpleEntityById(String entityType, Long id, T entity)
			throws Exception {
		logger.info("updateSimpleEntityById in AdminFacade invoked for entity type " + entityType + ", id " + id
				+ " with given model " + entity);
		entityType = entityType.trim().toLowerCase();
		@SuppressWarnings("unchecked")
		GenericSimpleService<T> serviceChosen = (GenericSimpleService<T>) chooseSimpleService(entityType);
		logger.debug("Service chosen: " + serviceChosen.getClass().getSimpleName());
		String titleProvided = entity.getTitle();
		T entityWithSameTitle = serviceChosen.findByTitle(titleProvided);
		if (!(entityWithSameTitle == null)) {
			throw new Exception("Another entity with title " + titleProvided + " already exists.");
		}

		T entityUpdated = (T) serviceChosen.update(id, entity);
		logger.info("Entity updated");
		return entityUpdated;
	}

	public void setEnabledById(Long accountId, boolean newEnabled) throws AccountException {
		logger.info("setEnabledById in AdminFacade invoked for account id " + accountId + " with enabled set to "
				+ newEnabled);
		Account account = accountService.getAccountById(accountId);
		if (account == null) {
			throw new AccountException("No account with id " + accountId + " exists.");
		} else {
			boolean currentEnabled = account.getEnabled();
			if (currentEnabled == newEnabled) {
				return;
			} else {
				accountService.switchEnabledById(accountId);
				logger.info("Enabled switched");
			}
		}
	}

	public void resetPasswordById(Long accountId) throws AccountException {
		logger.info("resetPasswordById in AdminFacade invoked for account id " + accountId);
		Account account = accountService.getAccountById(accountId);
		if (account == null) {
			throw new AccountException("No account with id " + accountId + " exists.");
		} else {
			accountService.resetPasswordById(accountId);
			logger.debug("Password reset");
		}
	}

}
