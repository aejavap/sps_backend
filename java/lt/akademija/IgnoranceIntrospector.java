//package lt.akademija;
//
//
//import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
//import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
//import lt.akademija.model.dto.AccountDTO;
//import lt.akademija.model.dto.users.AccountDTOFull;
//
///**
// * Class managing json for class inheritance
// * ignore property depending on Class.
// */
//class IgnoranceIntrospector extends JacksonAnnotationIntrospector {
//    public boolean hasIgnoreMarker(AnnotatedMember m) {
//                //Ignores password field for AccountDTOFull
//        System.out.println(m.getDeclaringClass()+" "+m.getMember()+" "+m.getType());
//        return m.getDeclaringClass() == AccountDTO.class && m.getName().equals("password")
//                || super.hasIgnoreMarker(m);
//    }
//}