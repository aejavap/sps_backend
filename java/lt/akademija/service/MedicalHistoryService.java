package lt.akademija.service;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.repository.DTOInterface.MedicalHistoryDTOInterface;
import lt.akademija.repository.other.MedicalHistoryRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

// logger done
// exceptions done
@Service
public class MedicalHistoryService {

	private static final Logger logger = LogManager.getLogger(MedicalHistoryService.class);

	@Autowired
	private MedicalHistoryRepository historyRepository;

	public Page<MedicalHistoryDTO> findByPatientId(Long id, Pageable pageRequest) {
		logger.info("MedicalHistoryService attempting to find medical record with patient id " + id);
		return historyRepository
				.findAllByMedicalHistoryPK_Patient_Id/* OrderByMedicalHistoryPK_TimestampDesc */(id, pageRequest)
				.map(MedicalHistoryDTO::toDTO);
	}

	public Page<MedicalHistoryDTO> findByPatientPid(Long pid, Pageable pageRequest) {
		logger.info("MedicalHistoryService attempting to find medical record with patient pid " + pid);
		return historyRepository
				.findByMedicalHistoryPK_Patient_Pid/* OrderByMedicalHistoryPK_TimestampDesc */(pid, pageRequest)
				.map(MedicalHistoryDTO::toDTO);
	}

	public MedicalHistoryDTO findByPk(Long patientId, Long doctorId, Long diagnosisId, Date date) throws Exception {
		logger.info("MedicalHistoryService attempting to find medical record with PK ");
		logger.info("PK provided: patientId " + patientId + ", doctorId " + doctorId + ",diagnosisId " + diagnosisId
				+ ", date " + date);
		if (date == null) {
			throw new Exception("Date provided cannot be null");
		}
		List<Long> longParams = Arrays.asList(patientId, doctorId, diagnosisId);
		for (Long param : longParams) {
			if (param == null || param <= 0) {
				throw new Exception("None of the ids provided can be null or <= 0");
			}
		}

		MedicalHistoryDTOInterface medicalHistory = historyRepository
				.findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp(
						patientId, doctorId, diagnosisId, date);
		MedicalHistoryDTO dtoToSend = MedicalHistoryDTO.toDTO(medicalHistory);
		if (dtoToSend == null) {
			logger.debug("No medical record found with PK provided");
		} else {
			logger.debug("Medical record found with PK provided: " + medicalHistory);
		}
		return dtoToSend;
	}

	public MedicalHistoryDTO createByDTO(MedicalHistoryDTO historyDTO) throws Exception {
		logger.info("MedicalHistoryService attempting to create a medical record from dto:  " + historyDTO);
		return historyRepository.saveDTO(historyDTO);
	}
}
