package lt.akademija.service.users;

import lt.akademija.model.dto.search.BasicUserSearchDTO;
import lt.akademija.model.dto.users.AbstractUserDTO;
import lt.akademija.model.entity.users.AbstractUser;
import lt.akademija.repository.users.GenericUserRepository;

import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.Type;

/**
 * Generic class used for User entities, contains search by pid and id.
 * 
 * @param <T>
 *            - entity, extending abstract user
 */

abstract public class GenericUserService<T extends AbstractUser, U extends AbstractUserDTO> {

	private Class<T> type;

	private Class<U> DTO;

	// GUserService(Class<T> type) {
	// this.type = type;
	// }

	GenericUserService(Class<T> type, Class<U> DTO) {
		this.type = type;
		this.DTO = DTO;
	}

	@Autowired
	protected GenericUserRepository<T> genericRepository;

	/**
	 * Finds all entities
	 *
	 * @param pageRequest
	 *            - carries paging information;
	 * @return - paged results;
	 */
	public Page<U> findAllDTO(Pageable pageRequest) {
		return genericRepository.findAll(pageRequest).map(entity -> U.toDTO(entity, DTO));
	}

	/**
	 * Finds all entities
	 *
	 * @param pageRequest
	 *            - carries paging information;
	 * @return - paged results;
	 */
	public Page<T> findAll(Pageable pageRequest) {
		return genericRepository.findAll(pageRequest);
	}

	/**
	 * Method implementation of Find interface, used to convert DTO to entity
	 * 
	 * @param id
	 *            - objects id in the table
	 * @return entity
	 * @throws Exception 
	 */

	public U findByIdDTO(Long id) throws Exception {

		System.out.println("find called by: " + id);
		U targetFound = U.toDTO(genericRepository.findOne(id), DTO);
		if (targetFound == null) {
			throw new Exception("No " + type.getSimpleName() + " with id " + id + " was found.");
		}
		return U.toDTO(genericRepository.findOne(id), DTO);
	}

	/**
	 * Method implementation of Find interface, used to convert DTO to entity
	 * 
	 * @param id
	 *            - objects id in the table
	 * @return entity
	 */

	public T findById(Long id) {

		System.out.println("find called by: " + id);
		return genericRepository.findOne(id);
	}

	/**
	 * Finds Entity by pid;
	 *
	 * @param pid
	 *            - entity pid;
	 * @return entity;
	 */

	// @Override
	public U findByPidDTO(Long pid) {
		return U.toDTO(genericRepository.findByPid(pid), DTO);
	}

	/**
	 * Finds Entity by pid;
	 *
	 * @param pid
	 *            - entity pid;
	 * @return entity;
	 */

	// @Override
	public T findByPid(Long pid) {
		return genericRepository.findByPid(pid);
	}

	public T findEntityById(Long id) {
		return genericRepository.findOne(id);
	}

	/**
	 * Method used to create entity in the db
	 * 
	 * @param entity
	 *            - passed entity
	 * @return if successful returns, created entity
	 * @throws Exception 
	 */

	public U createUser(U entity) throws Exception {
		System.out.println(entity.toString());
		System.out.println("Create in generic service called");
		// entity.setId(null);
		U entityAfterSave = ((RepositoryCustom<U>) genericRepository).saveDTO(entity);
		return entityAfterSave;
	}

	public T createEntity(U DTO) throws Exception {
		System.out.println(DTO.toString());
		System.out.println("Create in generic service called");
		DTO.setId(null);
		((RepositoryCustom<U>) genericRepository).saveDTO(DTO);
		T entityAfterSave = genericRepository.findOne(DTO.getId());
		return entityAfterSave;
	}

	public void removeById(Long id) {
		genericRepository.delete(id);
	}

	/**
	 * Updates entity by pid;
	 *
	 * @param pid
	 *            - entity pid;
	 * @param entity
	 *            - updated version of entity;
	 * @return entity;
	 * @throws Exception 
	 */
	public U updateByPid(Long pid, U entity) throws Exception {
		// T oldObject = genericRepository.findByPid(Pid);
		// BeanUtils.copyProperties(entity, oldObject, "id");
		// genericRepository.save(oldObject);
		System.out.println("Update started");
		Long newPid = entity.getPid();
		System.out.println("newPid: " + newPid);
		T anotherWithSamePid = genericRepository.findByPidNotWithPid(newPid, pid);
		if (anotherWithSamePid == null) {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			U userUpdated =  (U) ((RepositoryCustom) genericRepository).updateDTOByPid(pid, entity);
			System.out.println("user updated: " + userUpdated);
			return userUpdated;
		} else {
			throw new Exception("Another " + type.getSimpleName() + " with pid " + newPid + " already exists.");
		}
	}

	/**
	 * Method used to update entity, by its id
	 * 
	 * @param id
	 *            - entity id
	 * @param entity
	 *            - updated version of entity
	 * @return if successful returns updated entity
	 */

	public U update(Long id, U entity) throws Exception {
		// T oldObject = genericRepository.findOne(id);
		// BeanUtils.copyProperties(entity, oldObject, "id");
		// genericRepository.save(oldObject);
		System.out.println("Update started");
		Long newPid = entity.getPid();
		System.out.println("newPid: " + newPid);
		T anotherWithSamePid = genericRepository.findByPidNotWithId(newPid, id);
		System.out.println("anotherWithSamePid: " + anotherWithSamePid);
		if (anotherWithSamePid == null) {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			U user = (U) ((RepositoryCustom) genericRepository).updateDTOById(id, entity);
			System.out.println("user updated: " + user);
			return user;
		} else {
			throw new Exception("Another " + type.getSimpleName() + " with pid " + newPid + " already exists.");
		}
	}

	/**
	 * Method implementation of Find interface. Method is used to determine if class
	 * can operate with desired objects
	 * 
	 * @param type
	 *            - Object class
	 * @return returns true if service can handle desired class
	 */

	// @Override
	public boolean canHandle(Type type) {
		return this.type.equals(type);
	}

	/**
	 * Class has info on entity it works on. This method returns entity class, this
	 * service works on.
	 * 
	 * @return - class of the object service works on
	 */
	public Class<T> getType() {
		return type;
	}

	public Page<U> findUserByLastName(String lastName, Pageable pageRequest) {
		return genericRepository.findAllByLastNameStartingWithIgnoreCase(lastName, pageRequest)
				.map(entity -> U.toDTO(entity, DTO));
	}

	public Page<U> findUserByFirstName(String firstName, Pageable pageRequest) {
		return genericRepository.findAllByFirstNameStartingWithIgnoreCase(firstName, pageRequest)
				.map(entity -> U.toDTO(entity, DTO));
	}

	public Page<U> findAllById(Pageable pageRequest) {
		return ((GenericUserRepository<T>) genericRepository).findAllByOrderByIdAsc(pageRequest)
				.map(entity -> U.toDTO(entity, DTO));
	}

	public Page<U> findAllByPid(Pageable pageRequest) {
		return genericRepository.findAllByOrderByPidAsc(pageRequest).map(entity -> U.toDTO(entity, DTO));
	}

	public Page<U> findAllByLastName(Pageable pageRequest) {
		return genericRepository.findAllByOrderByLastNameAsc(pageRequest).map(entity -> {
			System.out.println("Object called: " + DTO);
			return U.toDTO(entity, DTO);
		});
	}

	/**
	 * Finds user by combination of regex applied to its firstname, lastname and pid
	 * fields. The fields may only start with text or numbers typed
	 * 
	 * @param pageRequest
	 *            - paging info;
	 * @return - paged result;
	 */
	public Page<U> searchUsersByNameOrSurnameOrPid(BasicUserSearchDTO searchDTO, Pageable pageRequest) {
		
		System.out.println("#################################");
		System.out.println("Calling generic user service");
		System.out.println("#################################");
		
		// make sure that even if parameters were not given, search continues
		String pidString = "";
		String firstName = "";
		String lastName = "";
		if ( searchDTO.getPid() == null || searchDTO.getPid() == 0) {
			// pass
		} else {
			pidString = String.valueOf(searchDTO.getPid());
		}
		if ( !(searchDTO.getFirstName()==null) ) {
			firstName = searchDTO.getFirstName().trim();
		}
		if ( !(searchDTO.getLastName()==null) ) {
			lastName = searchDTO.getLastName().trim();
		}
		
		System.out.println("#################################");
		System.out.println("pidString: " + pidString);
		System.out.println("firstName: " + firstName);
		System.out.println("lastName: " + lastName);
		System.out.println("#################################");
		
		System.out.println("Sorting request: " + pageRequest.getSort());
		return genericRepository
				.searchByNameOrSurnameOrPid(firstName, lastName, pidString, pageRequest)
				.map(entity -> U.toDTO(entity, DTO));
	}

}
