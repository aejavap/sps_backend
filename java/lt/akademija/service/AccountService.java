package lt.akademija.service;

import java.util.Optional;

import javax.security.auth.login.AccountException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lt.akademija.model.dto.AccountDTO;
import lt.akademija.model.dto.AccountDTOFull;
import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.users.AbstractUser;
import lt.akademija.repository.AccountRepository;

@Service
public class AccountService implements UserDetailsService {

	private static final Logger logger = LogManager.getLogger(AccountService.class);

	@Autowired
	private AccountRepository accountRepo;

	@Autowired // Bean in App.java
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
		Optional<Account> account = accountRepo.findByUsername(string);
		if (account.isPresent()) {
			return account.get();
		} else {
			throw new UsernameNotFoundException(String.format("Username[%s] not found", string));
		}
	}
	
	public Account getContextUser() throws UsernameNotFoundException {
			Account account = getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			return account;
	}

	public Account getAccountByUsername(String username) throws UsernameNotFoundException {
		Optional<Account> account = accountRepo.findByUsername(username);
		if (account.isPresent()) {
			return account.get();
		} else {
			throw new UsernameNotFoundException(String.format("Username[%s] not found", username));
		}
	}

	public Account getAccountById(Long id) throws AccountException {
		Optional<Account> account = accountRepo.findById(id);
		if (account.isPresent()) {
			return account.get();
		} else {
			throw new AccountException(String.format("User with id [%s] not found", id));
		}
	}

	public boolean existsById(Long id) {
		Optional<Account> account = accountRepo.findById(id);
		if (account.isPresent()) {
			return true;
		} else {
			return false;
		}
	}

	public AccountDTOFull getAccountDTOById(Long id) throws AccountException {
		Optional<Account> account = accountRepo.findById(id);
		if (account.isPresent()) {
			return AccountDTOFull.toDTO(account.get());
		} else {
			throw new AccountException(String.format("User with id [%s] not found", id));
		}
		// Optional<AccountDTOFull> account = accountRepo.findById(id);
		// if ( account.isPresent() ) {
		// System.out.println("This thing called");
		// return account.get();
		// } else {
		// throw new AccountException(String.format("User with id [%s] not found", id));
		// }
	}

	public AccountDTOFull getAccountDTOByUsername(String username) throws AccountException {
		Optional<Account> account = accountRepo.findByUsername(username);
		if (account.isPresent()) {
			return AccountDTOFull.toDTO(account.get());
		} else {
			throw new AccountException(String.format("User [%s] not found", username));
		}
	}

	public Account createAccount(Account account, AbstractUser userGiven) throws AccountException {
		if (accountRepo.countByUsername(account.getUsername()) == 0) {
			// Password encoding
			account.setPassword(passwordEncoder.encode(account.getPassword()));
			account.setAbstractUser(userGiven);
			return accountRepo.save(account);
		} else {
			throw new AccountException(String.format("Username[%s] already taken.", account.getUsername()));
		}
	}
	
	public void resetPasswordById(Long accountId) throws AccountException {
		Optional<Account> possibleAccountInRep = accountRepo.findById(accountId);
		if (possibleAccountInRep.isPresent()) {
			Account account = possibleAccountInRep.get();
			AbstractUser abstractUser = account.getAbstractUser();
			String newPassword = "";
			newPassword += abstractUser.getFirstName().toLowerCase().charAt(0);
			newPassword += abstractUser.getLastName().toLowerCase();
			newPassword += abstractUser.getPid().toString().substring(1, 7);
			System.out.println("###########################################");
			System.out.println("New password : " + newPassword);
			System.out.println("###########################################");
			account.setPassword(passwordEncoder.encode(newPassword));
			accountRepo.saveAndFlush(account);
		} else {
			throw new AccountException("No account with id " + accountId + " exists.");
		}
	}
	
	public void switchEnabledById(Long accountId) throws AccountException {
		Optional<Account> possibleAccountInRep = accountRepo.findById(accountId);
		if (possibleAccountInRep.isPresent()) {
			Account account = possibleAccountInRep.get();
			account.setEnabled(!account.getEnabled());
			accountRepo.saveAndFlush(account);
		} else {
			throw new AccountException("No account with id " + accountId + " exists.");
		}
	}

	public Account updateById(Long accountId, AccountDTO updatedAccountDTO) throws AccountException {
		Optional<Account> possibleAccountInRep = accountRepo.findById(accountId);
		if (possibleAccountInRep.isPresent()) {
			logger.info("AccountService attempting to do basic account update on user with id " + accountId);
			Account accountInRep = possibleAccountInRep.get();
			String newPassword = updatedAccountDTO.getPassword();
			String newUsername = updatedAccountDTO.getUsername();
			if ((newPassword == null || newPassword.equals("")) && (newUsername == null || newUsername.equals(""))) {
				throw new AccountException("Neither username nor password provided");
			} else if (newUsername == null || newUsername.equals("")) {
				accountInRep.setPassword(passwordEncoder.encode(newPassword));
			} else if (newPassword == null || newPassword.equals("")) {
				Optional<Account> possibleAccountWithNewUsername = accountRepo.findByUsername(newUsername);
				if (possibleAccountWithNewUsername.isPresent()) {
					throw new AccountException("Username provided invalid: such username already in use.");
				} else {
					accountInRep.setUsername(newUsername);
				}
			} else {
				Optional<Account> possibleAccountWithNewUsername = accountRepo.findByUsername(newUsername);
				if (possibleAccountWithNewUsername.isPresent()) {
					throw new AccountException("Username provided invalid: such username already in use.");
				} else {
					accountInRep.setPassword(passwordEncoder.encode(newPassword));
					accountInRep.setUsername(newUsername);
				}
			}
			logger.info("Account with id " + accountId + " was updated");
			return accountRepo.save(accountInRep);
		} else {
			throw new AccountException(String.format("Account with id [%s] does not exist.", accountId));
		}

	}

}
