package lt.akademija.service;

import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.repository.DTOInterface.PrescriptionDTOInterface;
import lt.akademija.repository.other.PrescriptionFillsRepository;
import lt.akademija.repository.other.PrescriptionRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

// exceptions done
// logging done
@Service
public class PrescriptionFillsService {

	private static final Logger logger = LogManager.getLogger(PrescriptionFillsService.class);

	@Autowired
	private PrescriptionFillsRepository fillsRepository;

	@Autowired
	private PrescriptionRepository prescriptionRepository;

	public Page<PrescriptionFillsDTO> findByPatientId(Long id, Pageable pageRequest) {
		logger.info("PrescriptionFillsService attempting to find prescription by patient id " + id);
		return fillsRepository.findByPrescriptionFillsPK_Prescription_Patient_Id(id, pageRequest)
				.map(PrescriptionFillsDTO::toDTO);
	}

	public Page<PrescriptionFillsDTO> findByPatientPid(Long pid, Pageable pageRequest) {
		logger.info("PrescriptionFillsService attempting to find prescription by patient pid " + pid);
		return fillsRepository.findByPrescriptionFillsPK_Prescription_Patient_Pid(pid, pageRequest)
				.map(PrescriptionFillsDTO::toDTO);
	}

	public PrescriptionFillsDTO createByDTO(PrescriptionFillsDTO fillsDTO) throws Exception {
		logger.info("PrescriptionFillsService attempting to create fill from dto " + fillsDTO);
		Long prescriptionIdGiven = fillsDTO.getPrescriptionId();
		PrescriptionDTOInterface prescriptionFound = prescriptionRepository.findOneById(prescriptionIdGiven);
		if (prescriptionFound == null) {
			throw new Exception(
					"Cannot create prescription fills. No prescription with id " + prescriptionIdGiven + " exists.");
		} else {
			logger.debug("Prescription with id " + prescriptionIdGiven + " found.");
		}
		return fillsRepository.saveDTO(fillsDTO);
	}

	public Long findFillsNoByPrescriptionId(Long prescriptionId) {
		logger.info(
				"PrescriptionFillsService attempting to get number of fillings by prescription id  " + prescriptionId);
		return fillsRepository.countByPrescriptionFillsPK_Prescription_Id(prescriptionId);
	}

	public Page<PrescriptionFillsDTO> findByPrescriptionId(Long id, Pageable pageRequest) {
		logger.info("PrescriptionFillsService attempting to find prescription by prescription id " + id);
		return fillsRepository.findByPrescriptionFillsPK_Prescription_Id(id, pageRequest)
				.map(PrescriptionFillsDTO::toDTO);
	}

}
