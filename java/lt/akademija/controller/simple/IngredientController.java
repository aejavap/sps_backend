package lt.akademija.controller.simple;

import io.swagger.annotations.Api;
import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.service.simple.IngredientService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "Ingredients")
@RequestMapping(value = "api/ingredients/")
public class IngredientController extends GenericSimpleEntityController<Ingredient> {
}
