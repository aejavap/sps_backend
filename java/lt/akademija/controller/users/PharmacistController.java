package lt.akademija.controller.users;

import io.swagger.annotations.*;
import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.service.AccountService;
import lt.akademija.service.PrescriptionFillsService;
import lt.akademija.service.PrescriptionService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

// logging done
// exceptions done
@RestController
@Api(value = "pharmacist")
@RequestMapping(value = "api/pharmacist/")
public class PharmacistController {

	@Autowired
	private PrescriptionService prescriptionService;

	@Autowired
	private PrescriptionFillsService prescriptionFillsService;

	@Autowired
	private AccountService accountService;

	private static final Logger logger = LogManager.getLogger(PharmacistController.class);

	@ApiOperation(value = "Lists all prescriptions by patient Pid", notes = "Returns list of prescriptions")
	@GetMapping(value = "/prescription/patient/{pid}")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<PrescriptionDTO> findPrescriptions(@PathVariable Long pid, Pageable pageRequest) {
		Long pharmacistId = accountService.getContextUser().getId();
		logger.info("Pharmacist with id " + pharmacistId
				+ " is attempting to access prescription history of patient with pid " + pid);
		return prescriptionService.findAllByPatientPid(pid, pageRequest);
	}

	@ApiOperation(value = "View prescription by its id")
	@GetMapping(value = "/prescription/{prescriptionId}")
	public PrescriptionDTO findPrescription(@PathVariable Long prescriptionId) {
		Long pharmacistId = accountService.getContextUser().getId();
		logger.info("Pharmacist with id " + pharmacistId + " is attempting to access prescription with id "
				+ prescriptionId);
		return prescriptionService.findByPrescriptionId(prescriptionId);
	}

	@ApiOperation(value = "View prescription fills by prescription id")
	@GetMapping(value = "/prescription-fill/{prescriptionId}")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<PrescriptionFillsDTO> findPrescriptionFill(@PathVariable Long prescriptionId, Pageable pageRequest) {
		Long pharmacistId = accountService.getContextUser().getId();
		logger.info("Pharmacist with id " + pharmacistId
				+ " is attempting to access prescription fills with prescription id id " + prescriptionId);
		return prescriptionFillsService.findByPrescriptionId(prescriptionId, pageRequest);
	}

	@ApiOperation(value = "Create a new prescription filling")
	@PostMapping(value = "/new/prescription-fill")
	public PrescriptionFillsDTO createPrescriptionFill(@RequestBody PrescriptionFillsDTO fillingDTO) throws Exception {
		Long pharmacistId = accountService.getContextUser().getId();
		Long prescriptionIdGiven = fillingDTO.getPrescriptionId();
		logger.info("Pharmacist with id " + pharmacistId
				+ " is attempting to create a new prescription for prescription with id " + prescriptionIdGiven);
		PrescriptionDTO prescriptionFound = prescriptionService.findByPrescriptionId(prescriptionIdGiven);
		if (prescriptionFound == null) {
			throw new NullPointerException("No prescription with id " + prescriptionIdGiven + " was found.");
		} else {
			logger.debug("No prescription with id " + prescriptionIdGiven + " was found.");
		}
		fillingDTO.setPhramacistId(pharmacistId);
		return prescriptionFillsService.createByDTO(fillingDTO);
	}

}
