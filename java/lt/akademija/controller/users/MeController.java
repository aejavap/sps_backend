package lt.akademija.controller.users;

import java.util.List;

import javax.security.auth.login.AccountException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import lt.akademija.model.dto.AccountDTO;
import lt.akademija.model.dto.AccountDTOFull;
import lt.akademija.model.dto.users.AbstractUserDTO;
import lt.akademija.model.entity.Account;
import lt.akademija.service.AccountService;
import lt.akademija.service.users.AdminService;
import lt.akademija.service.users.DoctorService;
import lt.akademija.service.users.PatientService;
import lt.akademija.service.users.PharmacistService;

// logger done
// exceptions done
/**
 * 
 * Controller for users to check their user/account details and to update their username/passwords.
 *
 */
@RestController
public class MeController {

	@Autowired
	AccountService accountService;

	@Autowired
	PatientService patientService;

	@Autowired
	DoctorService doctorService;

	@Autowired
	PharmacistService pharmacistService;

	@Autowired
	AdminService adminService;
	
	private static final Logger logger = LogManager.getLogger(MeController.class);

	@Autowired // Bean in App.java
	private PasswordEncoder passwordEncoder;

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Get user details of an authenticated user (account details not included)")
	@GetMapping(value = "/api/me")
	public <T extends AbstractUserDTO> T getMyUserDetails() throws Exception {
		Account account = accountService.getContextUser();
		logger.info("User with id " + account.getId() + " is attempting to access one's user details.");
		String userRole = getUserRole(account);
		Long userId = account.getId();
		switch (userRole) {
		case "ROLE_PATIENT":
			return (T) patientService.findByIdDTO(userId);
		case "ROLE_DOCTOR":
			return (T) doctorService.findByIdDTO(userId);
		case "ROLE_ADMIN":
			return (T) adminService.findByIdDTO(userId);
		case "ROLE_PHARMACIST":
			return (T) pharmacistService.findByIdDTO(userId);
		default:
			throw new Exception("User service could not be determined.");
		}
	}

	@ApiOperation(value = "Get account details of an authenticated user (user details not included)")
	@GetMapping(value = "/api/me/account")
	public AccountDTOFull getMyAccountDetails() throws AccountException {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		logger.info("User " + username + " is attempting to get one's account details.");
		return accountService.getAccountDTOByUsername(username);
	}

	@ApiOperation(value = "Update account details of an authenticated user")
	@PutMapping(value = "/api/me/account/update")
	@ResponseStatus(value = HttpStatus.OK)
	public void updateMyAccountDetails(@RequestParam String currentPasswordGiven, @RequestBody AccountDTO accountDTO) throws AccountException{
		Account currentAccount = accountService.getContextUser();
		String currentUsername = currentAccount.getUsername();
		logger.info("User " + currentUsername + " is attempting to update ones account details with dto " + accountDTO);
		boolean passwordMatches = passwordEncoder.matches(currentPasswordGiven, currentAccount.getPassword());
		if (passwordMatches) {
			logger.info("User " + currentUsername + " provided correct password. Proceeding to update user's account.");
			accountService.updateById(currentAccount.getId(), accountDTO);
		} else {
			throw new AccountException("Failed authentication: password did not match.");
		}

	}
	
	/**
	 * Helper method to determine what role the user has.
	 * @param account
	 * @return Returns a String
	 * @throws AccountException
	 */
	public String getUserRole(Account account) throws AccountException {
		String username = account.getUsername();
		List<GrantedAuthority> authorityList = account.getAuthorities();
		int numberOfAuthorities = authorityList.size();
		logger.debug("User " + username + " has " + numberOfAuthorities + " authorities.");
		if (numberOfAuthorities > 2) {
			throw new AccountException("User " + username + " has more than 2 roles. User should not exist.");
		} else if (numberOfAuthorities < 2) {
			throw new AccountException("User " + username + " has less than 2 roles. User should not exist.");
		}

		String userRole = null;
		for (GrantedAuthority authority : authorityList) {
			String roleInLoop = authority.toString();
			if (!roleInLoop.equals("ROLE_USER")) {
				userRole = roleInLoop;
				break;
			}
		}

		if (userRole == null) {
			throw new AccountException("User role could not be determined.");
		} else {
			return userRole;
		}
		
	}

}
