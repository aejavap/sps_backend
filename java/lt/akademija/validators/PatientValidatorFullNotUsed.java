//package lt.akademija.validators;
//
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.Date;
//
//import org.springframework.validation.Errors;
//import org.springframework.validation.ValidationUtils;
//import org.springframework.validation.Validator;
//
//import lt.akademija.model.entity.users.Patient;
//
//public class PatientValidator implements Validator {
//
//	@Override
//	public boolean supports(Class<?> clazz) {
//		return Patient.class.isAssignableFrom(clazz);
//	}
//
//	@Override
//	public void validate(Object target, Errors error) {
//		/*check if required fields are not empty or whitespace*/		
//		ValidationUtils.rejectIfEmptyOrWhitespace(error, "firstName", "fistName.empty");
//		ValidationUtils.rejectIfEmptyOrWhitespace(error, "lastName", "lastName.empty");
//		ValidationUtils.rejectIfEmptyOrWhitespace(error, "dob", "dateOfBirth.empty");
//		ValidationUtils.rejectIfEmptyOrWhitespace(error, "pid", "personalID.empty");
//		
//		/*CHECK PID*/
//		Patient patient = (Patient) target;
//		char [] pidChars = patient.getPid().toString().toCharArray();
//		
//		/*check if pid has 11 numbers*/
//		if (pidChars.length!=11) {
//			error.reject("pid", "incorrect format ,expected 11 digits");
//		}
//		
//		/*check if pid 1 digit is from 1 to 6*/
//		
//		char firstDigit=pidChars[0];
//		StringBuilder sb=new StringBuilder();
//		
//		if( firstDigit=='1' || firstDigit=='2') {
//			sb.append("18");
//		}
//		else if (firstDigit=='3' || firstDigit=='4') {
//			sb.append("19");
//		}
//		else if (firstDigit=='5' || firstDigit=='6') {
//			sb.append("20");
//		}
//		else {
//			error.rejectValue("pid", "incorrect format,expected first digit from 1 to 6");
//		}
//			
//		/*check if pid corresponds to date of birth*/
//		
//	    char[] datefromPid = Arrays.copyOfRange(pidChars, 1, 7);
//	    sb.append(datefromPid);
//	    String dateFromPid=sb.toString();
//	    
//	    sb.delete(0, sb.length());
//	    
//	    Calendar calDateOfBirth = Calendar.getInstance();
//	    calDateOfBirth.setTime(patient.getDob());
//	    sb.append(Integer.toString(calDateOfBirth.get(Calendar.YEAR)));
//	    sb.append(Integer.toString((calDateOfBirth.get(Calendar.MONTH)+1)));
//	    sb.append(Integer.toString(calDateOfBirth.get(Calendar.DATE)));
//	    
//	    String dateOfBirth=sb.toString();
//	    
//	    if (!(dateOfBirth.equals(dateFromPid))) {
//	    	error.rejectValue("pid","personal ID birthdate not corresponding to birthdate" );
//	    }
//	       
//		/*check if pid last  digit corresponds to formula*/
//	    
//	    char lastDigit='\0';
//	    int sum=0;
//	    
//	    for (int i=0; i<10; i++) {
//	    	if (i<9) {
//	    	sum+=(Character.getNumericValue(pidChars[i]))*(i+1);
//	    	}
//	    	else {
//	    		sum+=(Character.getNumericValue(pidChars[i]))*1;
//	    	}
//	    }
//	    
//	    if (sum%11!=10) {
//	    	lastDigit=Integer.toString(sum%11).charAt(0);
//	    	}
//	    else {
//	    	sum=0;
//	    	for (int i=0; i<10; i++) {
//		    	if (i<7) {
//		    		sum+=(Character.getNumericValue(pidChars[i]))*(i+3);
//		    	}
//		    	else {
//		    		sum+=(Character.getNumericValue(pidChars[i]))*(i-6);
//		    		}
//		    	}
//	    	 if (sum%11!=10) {
//	 	    	lastDigit=Integer.toString(sum%11).charAt(0);
//	 	    	}
//	    	 else {
//	    		 lastDigit=0;
//	    		 }
//	    }
//	  
//	    if(lastDigit!=pidChars[10]) {
//	    	error.reject("pid", "incorrect format, expected another control sum (last digit)");
//	    }
//	    
//	    /*CHECK DATEOF BIRTH*/
//	    /*check if date of birth is not future date*/
//	    	    
//	    if (patient.getDob().after(new Date())) {
//            error.rejectValue("dob", "future date of birth");
//        } 
//	}
//}
