package lt.akademija.validators;

import java.util.Arrays;
import java.util.Calendar;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import lt.akademija.model.entity.users.Patient;

public class PatientValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Patient.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors error) {
		
		/*Check if dob corresponds to pid*/
		Patient patient = (Patient) target;
		char [] pidChars = patient.getPid().toString().toCharArray();

		char firstDigit=pidChars[0];
		StringBuilder sb=new StringBuilder();
		
		if( firstDigit=='1' || firstDigit=='2') {
			sb.append("18");
		}
		else if (firstDigit=='3' || firstDigit=='4') {
			sb.append("19");
		}
		else if (firstDigit=='5' || firstDigit=='6') {
			sb.append("20");
		}

	    char[] datefromPid = Arrays.copyOfRange(pidChars, 1, 7);
	    sb.append(datefromPid);
	    String dateFromPid=sb.toString();
	    
	    
	    
	    sb.delete(0, sb.length());
	    
	    Calendar calDateOfBirth = Calendar.getInstance();
	    calDateOfBirth.setTime(patient.getDob());
	    sb.append(Integer.toString(calDateOfBirth.get(Calendar.YEAR)));
	    int month=calDateOfBirth.get(Calendar.MONTH)+1;
	    if(month<10) {
	    	sb.append("0");
	    	sb.append(Integer.toString(month));
	    }
	    else {
	    	sb.append(Integer.toString(month));
	    }
	    int day=calDateOfBirth.get(Calendar.DATE);
	    
	    if (day<10) {
	    	sb.append("0");
	    	sb.append(Integer.toString(day));
	    }
	    else {
	    	sb.append(Integer.toString(day));
	    }
	    
	    String dateOfBirth=sb.toString();
	    
	    if (!(dateOfBirth.equals(dateFromPid))) {
	    	error.rejectValue("pid","personal ID birthdate not corresponding to birthdate" );
	    }
	}
}
