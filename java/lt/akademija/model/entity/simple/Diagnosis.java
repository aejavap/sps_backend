package lt.akademija.model.entity.simple;

import javax.persistence.*;

import lombok.*;


@Table(name = "DIAGNOSES")
@EqualsAndHashCode
@Entity
@Data
public class Diagnosis extends AbstractSimpleEntity {
    @Column(name = "DESCRIPTION", length = 8000)
    private String description;

    @Override
    public String toString() {
        return "Diagnosis{" +
                "id='"+super.getId()+'\''+
                "title='" + super.getTitle() + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
