package lt.akademija.model.entity.simple;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name = "INGREDIENTS")
@EqualsAndHashCode
@Entity
public class Ingredient extends AbstractSimpleEntity {

//    @Override
//    public String toString() {
//        return "Ingredient{" +
//                "id='"+super.getId()+'\''+
//                "title='" + title + '\'' +
//                '}';
//    }
}
