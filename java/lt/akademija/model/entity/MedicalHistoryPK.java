package lt.akademija.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.users.*;

import lombok.Data;

@Embeddable
@EqualsAndHashCode
@Data
public class MedicalHistoryPK implements Serializable {

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "PATIENT_ID", insertable = false, updatable = false, nullable = false)
    //@JsonIgnore
    private Patient patient;

    //Not sure if possible make nullable
    //https://stackoverflow.com/questions/386040/whats-wrong-with-nullable-columns-in-composite-primary-keys
    //Solution to add additional table

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "DIAGNOSIS_ID", insertable = false, updatable = false, nullable = false)
    private Diagnosis diagnosis;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "DOCTOR_ID", insertable = false, updatable = false, nullable = false)
    private Doctor doctor;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE", insertable=false, updatable=false, nullable = false)
    private Date timestamp;

    //Default constructor needed
    public MedicalHistoryPK() {
    }


    public MedicalHistoryPK(Patient patient, Doctor doctor, Diagnosis diagnosis) {
        this.patient = patient;
        this.doctor = doctor;
        this.diagnosis = diagnosis;
        this.timestamp = new Date();
    }

}
