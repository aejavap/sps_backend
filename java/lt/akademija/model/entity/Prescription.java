package lt.akademija.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "PRESCRIPTIONS")
public class Prescription {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    @Column(name = "PRESCRIBED_ON", nullable = false)
    private Date prescriptionDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "VALID_UNTIL", nullable = true)
    private Date validUntil;

    @Column(name = "DOSAGE_NOTES", length = 8000)
    private String dosageNotes;

    //Child in relation with patient(Foreign key)
    //Bidirectional
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "PATIENT_ID", nullable = false)
    private Patient patient;

    //Child in relation with doctor(Foreign key)
    //Bidrectional
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "DOCTOR_ID", nullable = false)
    private Doctor doctor;

    //Unidirectional relation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTIVE_INGREDIENT", nullable = false)
    @JsonIgnore
    private Ingredient activeIngredient;

    //Changed to number
    @Column(name = "ACTIVE_INGREDIENT_PER_DOSE")
    @Min(value = 0, message = "The value must positive")
    private Double activeIngredientPerDose;

//    @Enumerated(EnumType.STRING)
    @Column(name = "ACTIVE_INGREDIENT_UNITS")
    private String activeIngredientUnits;

    //Primary key
    //Primary key in relation
    @OneToMany(mappedBy = "prescriptionFillsPK.prescription", cascade = {CascadeType.MERGE, CascadeType.PERSIST},
            fetch = FetchType.LAZY)
    @JsonIgnore
    private List<PrescriptionFills> prescriptionFills = new ArrayList<>();

    public Prescription(){}

    @PersistenceConstructor
    public Prescription(PrescriptionDTO prescriptionDTO, Ingredient activeIngredient, Doctor doctor, Patient patient) {
        this.prescriptionDate = prescriptionDTO.getPrescriptionDate();
        this.validUntil = prescriptionDTO.getValidUntil();
        this.activeIngredientPerDose = prescriptionDTO.getActiveIngredientPerDose();
        this.activeIngredientUnits = prescriptionDTO.getActiveIngredientUnits();
        this.dosageNotes = prescriptionDTO.getDosageNotes();
        this.activeIngredient = activeIngredient;
        this.doctor = doctor;
        this.patient = patient;
        patient.addPrescription(this);
        doctor.addPrescription(this);
    }

//    enum ActiveIngredientUnits {
//        MILIGRAMS("mg"), MICROGRAMS("microg"), TARGETVOLINTERNUNITS("TV/IU");
//
//        private String name;
//
//        ActiveIngredientUnits(String name) {
//            this.name = name;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        //Finds by name
//        //Needs to add exception
//        static public ActiveIngredientUnits byValue(String name) {
//            for (ActiveIngredientUnits active : ActiveIngredientUnits.values()) {
//                if (active.name.equals(name))
//                    return active;
//            }
//            return null;
//        }
//    }

    public String getActiveIngredientUnits() {
        return activeIngredientUnits;
    }

    public void setActiveIngredientUnits(String activeIngredientUnits) {
        this.activeIngredientUnits = activeIngredientUnits;
    }

//   public String ActiveIngredientUnitsToString()
//    {
//        if(this.activeIngredient != null)
//            return activeIngredientUnits.getName();
//        return "null";
//    }

    @Override
    public String toString() {
        return "Prescription{" +
                "prescriptionDate=" + prescriptionDate +
                ", validUntil=" + validUntil +
                ", dosageNotes='" + dosageNotes + '\'' +
                ", patient=" + patient +
                ", doctor=" + doctor +
                ", activeIngredient=" + activeIngredient +
                ", activeIngredientPerDose=" + activeIngredientPerDose +
                ", activeIngredientUnits=" + activeIngredientUnits +
                '}';
    }
}
