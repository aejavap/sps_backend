package lt.akademija.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.users.Pharmacist;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode
@Data
@Embeddable
public class PrescriptionFillsPK implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE",  insertable = false, updatable = false, nullable = false)
    private Date  timestamp;

    //Foreign key
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "PHARMACIST_ID", referencedColumnName = "id", insertable = false, updatable = false, nullable = false)
    private Pharmacist pharmacist;

    //Foreign key
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "PRESCRIPTION_ID", referencedColumnName = "id", insertable = false, updatable = false, nullable = false)
    private Prescription prescription;

  //Default constructor needed
    public PrescriptionFillsPK() {
    }

    public PrescriptionFillsPK(Date date, Pharmacist pharmacist, Prescription prescription) {
        this.pharmacist = pharmacist;
        this.prescription = prescription;
        this.timestamp = new Date();
    }
}
