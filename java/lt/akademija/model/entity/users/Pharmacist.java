package lt.akademija.model.entity.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.entity.PrescriptionFills;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode
@Entity
@DiscriminatorValue("PHARMACIST")
@Table(name = "PHARMACISTS")
public class Pharmacist extends AbstractUser {

	@Column(name = "COMPANY_TYPE")
	private String companyType;
	
    @Column(name = "COMPANY_NAME")
    private String companyName;

    //Primary key in relation
    @OneToMany(mappedBy = "prescriptionFillsPK.pharmacist", cascade = {CascadeType.MERGE, CascadeType.PERSIST},
            fetch = FetchType.LAZY)
    @Column
    @JsonIgnore
    private List<PrescriptionFills> prescriptionsFill;

    //To synchronize bidirectional relationship from PrescriptionFills side
    public void addPrescriptionFill(PrescriptionFills prescriptionFills)
    {
        prescriptionsFill.add(prescriptionFills);
    }

//    enum Type{
//        VšĮ("VšĮ"), UAB("UAB"), AB("AB"), MB("MB");
//
//        private String name;
//        Type(String name)
//        {
//            this.name = name;
//        }
//    }

    @Override
    public String toString() {
        return "Pharmacist{" +
                "id='"+super.getId()+'\''+
                "firstName='" + super.getFirstName() + '\'' +
                ", lastName='" + super.getLastName() + '\'' +
                ", companyType=" + companyType + + '\'' +
                ", companyName='" + companyName + '\'' +
                '}';
    }
}

