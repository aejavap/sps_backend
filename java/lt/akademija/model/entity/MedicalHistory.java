package lt.akademija.model.entity;

import javax.persistence.*;
import javax.validation.constraints.Min;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import org.springframework.data.annotation.PersistenceConstructor;

import java.io.Serializable;

@Entity
@Table(name = "MEDICAL_HISTORIES")
@EqualsAndHashCode
@Data
public class MedicalHistory implements Serializable {

    @EmbeddedId
    private MedicalHistoryPK medicalHistoryPK;

    @Column(name = "NOTES", length = 8000)
    private String notes;

    //Positive integer values validation
    // What units??
    @Column(name = "APPOINTMENT_LENGTH", nullable = false)
    @Min(value = 0L, message = "The value must be positive")
    private int appointmentLength;

    @Column(name = "COMPENSATED", nullable = false)
    private boolean compensated;

    @Column(name = "REPEAT_VISITATION", nullable = false)
    private boolean repeatVisit;

    //Default constructor needed
    public MedicalHistory() {
    }

    @PersistenceConstructor
    public MedicalHistory(MedicalHistoryDTO medicalHistoryDTO, Doctor doctor, Patient patient, Diagnosis diagnosis) {
        this.medicalHistoryPK = new MedicalHistoryPK(patient, doctor, diagnosis);
        this.appointmentLength = medicalHistoryDTO.getAppointmentLength();
        this.compensated = medicalHistoryDTO.getCompensated();
        this.repeatVisit = medicalHistoryDTO.getRepeatVisitation();
        this.notes = medicalHistoryDTO.getNotes();
    }

}
