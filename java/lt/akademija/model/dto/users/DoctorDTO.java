package lt.akademija.model.dto.users;

import lombok.Data;
import lt.akademija.model.entity.users.Doctor;

@Data
public class DoctorDTO extends AbstractUserDTO{

    public static DoctorDTO toDTO(Doctor doctor)
    {
        System.out.println("DTO constructor called");
        DoctorDTO doctorDTO = AbstractUserDTO.toDTO(doctor,DoctorDTO.class);
        if(doctor.getSpecialization() != null)
            doctorDTO.setSpecializationId(doctor.getSpecialization().getId());
        	doctorDTO.setSpecializationTitle(doctor.getSpecialization().getTitle());
        return doctorDTO;
    }
    private Long specializationId;
    private String specializationTitle;
}
