package lt.akademija.model.dto.users;

import lombok.Data;
import lt.akademija.model.entity.users.Admin;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

@Data
//@AllArgsConstructor
//@Value
public class AdminDTO extends AbstractUserDTO{
    public static AdminDTO toDTO(Admin admin)
    {
        AdminDTO adminDTO = AbstractUserDTO.toDTO(admin, AdminDTO.class);
        System.out.println("AdminDTO; "+admin);
//        System.out.println("DTO constructor called");
//        BeanUtils.copyProperties(adminDTOInterface, admin);
        return adminDTO;
    }

}
