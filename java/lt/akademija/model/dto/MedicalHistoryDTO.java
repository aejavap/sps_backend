package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.repository.DTOInterface.MedicalHistoryDTOInterface;

import org.springframework.beans.BeanUtils;

import java.util.Date;
import java.util.TimeZone;

@Data
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicalHistoryDTO {

    private Long patientId;
    private Long patientPid;
    @JsonIgnore
    private Long doctorId;
    @JsonIgnore
    private String doctorFirstName;
    @JsonIgnore
    private String doctorLastName;
    @JsonIgnore
    private Date date;
    private Long diagnosisId;
    private String diagnosisTitle;
    private String diagnosisDescription;
    private String notes;
    private Integer appointmentLength;
    private Boolean compensated;
    private Boolean repeatVisitation;

   public static MedicalHistoryDTO toDTO(MedicalHistoryDTOInterface historyDTOInterface) {
        MedicalHistoryDTO historyDTO = new MedicalHistoryDTO();
        System.out.println("Medical history DTO constructor called");
        BeanUtils.copyProperties(historyDTOInterface, historyDTO);
        System.out.println("After medical history DTO constructor");
        return historyDTO;
    }

    @JsonProperty
    public Long getDoctorId() {
        return doctorId;
    }

    @JsonIgnore
    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    @JsonProperty
    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    @JsonIgnore
    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    @JsonProperty
    public String getDoctorLastName() {
        return doctorLastName;
    }

    @JsonIgnore
    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    public Date getDate() {
        return date;
    }

    @JsonIgnore
    public void setDate(Date date) {
        this.date = date;
    }

    @JsonProperty
    public Long getDiagnosisId() {
        return diagnosisId;
    }

    @JsonIgnore
    public void setDiagnosisId(Long diagnosisId) {
        this.diagnosisId = diagnosisId;
    }

    @JsonProperty
    public String getDiagnosisDescription() {
    	return diagnosisDescription;
    }

    @JsonIgnore
    public void setDiagnosisDescription(String diagnosisDescription) {
    	this.diagnosisDescription = diagnosisDescription;
    }
    
    @JsonProperty
    public String getDiagnosisTitle() {
    	return this.diagnosisTitle;
    }
    
    @JsonIgnore
    public void setDiagnosisTitle(String titleGiven) {
    	this.diagnosisTitle = titleGiven;
    }
    
    
}
