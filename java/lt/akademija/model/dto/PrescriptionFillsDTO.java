package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lt.akademija.model.entity.PrescriptionFills;
import lt.akademija.repository.DTOInterface.PrescriptionFillsDTOInterface;

import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
public class PrescriptionFillsDTO {
    @JsonIgnore
    private Date date;
    private Long prescriptionId;
    @JsonIgnore
    private Long phramacistId;
    @JsonIgnore
    private String phFirstName;
    @JsonIgnore
    private String phLastName;

    public static PrescriptionFillsDTO toDTO(PrescriptionFillsDTOInterface prescriptionFillsDTOInterface)
    {
        PrescriptionFillsDTO prescriptionFills = new PrescriptionFillsDTO();
        System.out.println("DTO constructor called");
        BeanUtils.copyProperties(prescriptionFillsDTOInterface, prescriptionFills);
        return prescriptionFills;
    }

    @JsonIgnore
    public Long getPhramacistId() {
        return phramacistId;
    }

    @JsonProperty
    public void setPhramacistId(Long phramacistId) {
        this.phramacistId = phramacistId;
    }

    @JsonProperty
    public String getPhFirstName() {
        return phFirstName;
    }

    @JsonIgnore
    public void setPhFirstName(String phFirstName) {
        this.phFirstName = phFirstName;
    }

    @JsonProperty
    public String getPhLastName() {
        return phLastName;
    }

    @JsonIgnore
    public void setPhLastName(String phLastName) {
        this.phLastName = phLastName;
    }

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    public Date getDate() {
        return date;
    }

    @JsonIgnore
    public void setDate(Date date) {
        this.date = date;
    }
}
