package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.*;

import lombok.*;
import lt.akademija.repository.DTOInterface.PrescriptionDTOInterface;

import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrescriptionDTO{
    @JsonIgnore
	private Long prescriptionId;
    private Long activeIngredientId;
    private String activeIngredientName;
    private Double activeIngredientPerDose;
    private String activeIngredientUnits;
    private String dosageNotes;
    @JsonIgnore
    private Long doctorId;
    @JsonIgnore
    private String doctorFirstName;
    @JsonIgnore
    private String doctorLastName;

    private Long patientPid;
    private Long patientId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    @JsonIgnore
    private Date prescriptionDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    private Date validUntil;
    @JsonIgnore
    private Long fillsNo;

    public static PrescriptionDTO toDTO(PrescriptionDTOInterface prescriptionDTOInterface)
    {
        PrescriptionDTO prescription = new PrescriptionDTO();
        System.out.println("DTO constructor called");
        BeanUtils.copyProperties(prescriptionDTOInterface, prescription);
        return prescription;
    }

    @JsonProperty
    public Long getActiveIngredient() {
        return activeIngredientId;
    }

    @JsonProperty
    public void setActiveIngredient(Long activeIngredient) {
        this.activeIngredientId = activeIngredient;
    }

    @JsonProperty
    public Long getPrescriptionId() {
        return prescriptionId;
    }

    @JsonIgnore
    public void setPrescriptionId(Long prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    @JsonProperty
    public Long getDoctorId() {
        return doctorId;
    }

    @JsonIgnore
    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    @JsonProperty
    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    @JsonIgnore
    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    @JsonProperty
    public String getDoctorLastName() {
        return doctorLastName;
    }

    @JsonIgnore
    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    public Date getPrescriptionDate() {
        return prescriptionDate;
    }

    @JsonIgnore
    public void setPrescriptionDate(Date prescriptionDate) {
        this.prescriptionDate = prescriptionDate;
    }

    @JsonProperty
    public Long getFillsNo() {
        return fillsNo;
    }

    @JsonIgnore
    public void setFillsNo(Long fillsNo) {
        this.fillsNo = fillsNo;
    }

    @JsonProperty
    public String getActiveIngredientName() {
        return activeIngredientName;
    }

    @JsonIgnore
    public void setActiveIngredientName(String activeIngredientName) {
        this.activeIngredientName = activeIngredientName;
    }
    
    
    
}
