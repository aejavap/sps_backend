package lt.akademija.model.logging;
//package lt.akademija.model.Logging;
//
//
//import org.apache.log4j.Logger;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.*;
//import org.springframework.stereotype.Component;
//
//
//@Component
//@Aspect
//
//public class RepositoryLogger {
//
//
//    private static final Logger logger = Logger.getLogger("Logger");
//
//    //Track all methods in service package with single arg
//    @Around(value = "execution(* lt.akademija.repository..*(..))&& args(arg)")
//    public <T> void logREST(ProceedingJoinPoint jp, T arg){
//        try{
//            String method = jp.getSignature().toShortString();
//            //String className = jp.getSignature().getName();
//            // logger.info("Before start");
//
//            logger.info("Arg passed: "+arg);
//            Class<?> temp = jp.proceed().getClass();
//            System.out.println(temp);
//        }catch(Throwable e){
//            String method = jp.getSignature().toShortString();
//            logger.error("Error in: " + method);
//            logger.error("Details: "+e.getMessage());
//        }
//    }
//
//    //Track all parametless methods in service package
//    @Around(value = "execution(* lt.akademija.repository..*())")
//    public void logREST(ProceedingJoinPoint jp){
//        try{
//            String method = jp.getSignature().toShortString();
//            logger.info("Called in: "+method);
//            jp.proceed();
//        }catch(Throwable e){
//            String method = jp.getSignature().toShortString();
//            logger.error("Error in: " + method);
//            logger.error("Details: "+e.getMessage());
//        }
//    }
//}
