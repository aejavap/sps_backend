package lt.akademija.repository.simple;

import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.repository.GenericRepository;

public interface DiagnosisRepository extends GenericSimpleEntityRepository<Diagnosis> {
//    @Override
//    @Query("SELECT id, title, description FROM Diagnosis")
//    List<Diagnosis> findAll();
}
