package lt.akademija.repository.simple;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import lt.akademija.repository.GenericRepository;

@NoRepositoryBean
public interface GenericSimpleEntityRepository<T> extends GenericRepository<T> {
	T findByTitle(String title);
	Page<T> findAllByTitleContainsIgnoreCase(String title, Pageable pageRequest);
}
