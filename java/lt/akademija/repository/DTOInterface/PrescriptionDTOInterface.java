package lt.akademija.repository.DTOInterface;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface PrescriptionDTOInterface {
    @Value("#{target.id}")
    Long getPrescriptionId();

    @Value("#{target.activeIngredient.id}")
    Long getActiveIngredientId();

    Double getActiveIngredientPerDose();

    String getActiveIngredientUnits();

    String getDosageNotes();

    @Value("#{target.doctor.firstName}")
    String getDoctorFirstName();

    @Value("#{target.doctor.lastName}")
    String getDoctorLastName();

    @Value("#{target.patient.pid}")
    Long getPatientPid();

    @Value("#{target.patient.id}")
    Long getPatientId();

    @Value("#{target.activeIngredient.title}")
    String getActiveIngredientName();

    Date getPrescriptionDate();

    Date getValidUntil();

}
