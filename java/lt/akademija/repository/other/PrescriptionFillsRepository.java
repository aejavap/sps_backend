package lt.akademija.repository.other;

import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.repository.GenericRepository;
import lt.akademija.repository.DTOInterface.PrescriptionFillsDTOInterface;
import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lt.akademija.model.entity.*;

import java.util.List;

@Repository
public interface PrescriptionFillsRepository extends GenericRepository<PrescriptionFills>, RepositoryCustom<PrescriptionFillsDTO> {

    Page<PrescriptionFillsDTOInterface> findByPrescriptionFillsPK_Prescription_Patient_Pid(Long pid, Pageable pageRequest);

    Page<PrescriptionFillsDTOInterface> findByPrescriptionFillsPK_Prescription_Patient_Id(Long id, Pageable pageRequest);

    Long countByPrescriptionFillsPK_Prescription_Id(Long id);

    Page<PrescriptionFillsDTOInterface> findByPrescriptionFillsPK_Prescription_Id(Long id, Pageable pageRequest);

//    @Query("SELECT c FROM PrescriptionFills c where c.prescription = PrescriptionId")
//    List<PrescriptionFills> findById(Long PrescriptionId);
}
