package lt.akademija.repository.implementation;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.RepositoryCustom;
import lt.akademija.service.simple.DiagnosisService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
@Repository
public class MedicalHistoryRepositoryImpl implements RepositoryCustom<MedicalHistoryDTO> {
	private static Logger logger = Logger.getLogger(MedicalHistoryRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	DiagnosisService diagnosisService;

	@Override
	public MedicalHistoryDTO saveDTO(MedicalHistoryDTO historyDTO) throws Exception {
		String diagnosisTitle = historyDTO.getDiagnosisTitle();
		Long diagnosisId = historyDTO.getDiagnosisId();
		boolean DiagnosisTitleGiven = !(diagnosisTitle == null || diagnosisTitle.equals(""));
		logger.debug("Diagnosis title given? : " + DiagnosisTitleGiven);
		boolean DiagnosisIdGiven = !(diagnosisId == null || diagnosisId < 1);
		logger.debug("Diagnosis id given? : " + DiagnosisIdGiven);
		Diagnosis diagnosis;
		if (DiagnosisTitleGiven && DiagnosisIdGiven) {
			diagnosis = diagnosisService.findByTitle(diagnosisTitle);
			if (diagnosis.getId().equals(diagnosisId)) {
				logger.debug("Diagnosis was set to: " + diagnosis);
			} else {
				throw new Exception("Both diagnosis title and id given but they pointed to different diagnoses.");
			}
		} else if (DiagnosisTitleGiven) {
			diagnosis = diagnosisService.findByTitle(diagnosisTitle);
			if (diagnosis == null) {
				throw new Exception("Diagnosis title was given as " + diagnosisTitle
						+ " but id did not correspond to any known diagnosis.");
			} else {
				logger.debug("Diagnosis was set to: " + diagnosis);
			}
		} else if (DiagnosisIdGiven) {
			diagnosis = diagnosisService.findById(diagnosisId);
			if (diagnosis == null) {
				throw new Exception("Diagnosis id was given as " + diagnosisId + " but it did not correspond to any known diagnosis");
			} else {
				logger.debug("Diagnosis was set to: " + diagnosis);
			}
		} else {
			throw new Exception("Neither diagnosis id nor title were given. Diagnosis cannot be null. ");
		}

		Long doctorId = historyDTO.getDoctorId();
		if (doctorId == null) {
			logger.error("Doctor id null passed medical history repo");
			return null;
		}
		Doctor doctor = (Doctor) em.createQuery("Select d from Doctor d where d.id = ?1").setParameter(1, doctorId)
				.getSingleResult();
		historyDTO.setDoctorId(doctorId);
		if (doctor == null) {
			logger.error("doctor not found id: " + doctorId);
			return null;
		}
		Long pid = historyDTO.getPatientPid();
		Long patientId = historyDTO.getPatientId();
		Patient patient = null;
		if (pid != null && pid >= 0) {
			logger.info("Patient search by pid. Current value: " + pid);
			patient = (Patient) em.createQuery("Select d from Patient d where d.pid = ?1").setParameter(1, pid)
					.getSingleResult();
			if (patient == null) {
				logger.error("Patient not found. Wrong pid: " + pid);
				return null;
			}
			historyDTO.setPatientId(patient.getId());

		} else if (patientId != null && patientId >= 0) {
			logger.info("Patient search by id. Current value: " + patientId);
			patient = (Patient) em.createQuery("Select d from Patient d where d.id = ?1").setParameter(1, patientId)
					.getSingleResult();
			if (patient == null) {
				logger.error("Patient not found. Wrong id: " + patientId);
				return null;
			}
			historyDTO.setPatientPid(patient.getPid());
		} else {
			logger.error("Patient pid and id null");
			return null;
		}
		if (patient == null) {
			logger.error("Patient not found: id =" + patientId + " pid: " + pid);
			return null;
		}
		logger.info("Medical history constructor called: ");
		MedicalHistory history = new MedicalHistory(historyDTO, doctor, patient, diagnosis);
		logger.info("Medical history: " + history);
		historyDTO.setDate(history.getMedicalHistoryPK().getTimestamp());
		historyDTO.setDoctorFirstName(history.getMedicalHistoryPK().getDoctor().getFirstName());
		historyDTO.setDoctorLastName(history.getMedicalHistoryPK().getDoctor().getLastName());
		historyDTO.setDiagnosisDescription(history.getMedicalHistoryPK().getDiagnosis().getDescription());
		historyDTO.setDiagnosisTitle(history.getMedicalHistoryPK().getDiagnosis().getTitle());
		logger.info("Created historyDTO: " + historyDTO);
		this.em.persist(history);
		this.em.flush();
		return historyDTO;
	}

	@Override
	public MedicalHistoryDTO updateDTOByPid(Long pid, MedicalHistoryDTO historyDTO) {
		return null;
	}

	@Override
	public MedicalHistoryDTO updateDTOById(Long id, MedicalHistoryDTO historyDTO) {
		return null;
	}
}
