package lt.akademija.repository.implementation;

import lt.akademija.model.dto.users.PharmacistDTO;
import lt.akademija.model.entity.users.Pharmacist;
import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
@Repository
public class PharmacistRepositoryImpl implements RepositoryCustom<PharmacistDTO> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public PharmacistDTO saveDTO (PharmacistDTO pharmacistDTO){
        Pharmacist pharmacist = new Pharmacist();
        pharmacist = localCopy(pharmacistDTO, pharmacist);
        this.em.persist(pharmacist);
        pharmacistDTO.setId(pharmacist.getId());
        this.em.flush();
        return pharmacistDTO;
    }

    @Override
    public PharmacistDTO updateDTOByPid(Long pid, PharmacistDTO pharmacistDTO){
        Pharmacist pharmacist = (Pharmacist) em.createQuery("Select d from Pharmacist d where d.pid = ?1")
                .setParameter(1, pid).getSingleResult();
        if(pharmacist == null)
        {
            System.out.println("Pharmacist not found in updateDTOByPid");
            return null;
        }
        pharmacist = localCopy(pharmacistDTO, pharmacist);
        this.em.merge(pharmacist);
        this.em.flush();
        return pharmacistDTO;
    }
    @Override
    public PharmacistDTO updateDTOById(Long id, PharmacistDTO pharmacistDTO){
        Pharmacist pharmacist = (Pharmacist) em.createQuery("Select d from Pharmacist d where d.id = ?1")
                .setParameter(1, id).getSingleResult();
        if(pharmacist == null)
        {
            System.out.println("Pharmacist not found in updateDTOById");
            return null;
        }
        pharmacist = localCopy(pharmacistDTO, pharmacist);
        this.em.merge(pharmacist);
        this.em.flush();
        return pharmacistDTO;
    }

    private Pharmacist localCopy(PharmacistDTO pharmacistDTO, Pharmacist pharmacist) {
        pharmacist.setPid(pharmacistDTO.getPid());
        pharmacist.setFirstName(pharmacistDTO.getFirstName());
        pharmacist.setLastName(pharmacistDTO.getLastName());
        pharmacist.setCompanyType(pharmacistDTO.getCompanyType());
        pharmacist.setCompanyName(pharmacist.getCompanyName());
        return pharmacist;
    }
}
