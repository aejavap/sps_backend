package lt.akademija.repository.implementation;

import lt.akademija.model.dto.users.DoctorDTO;
import lt.akademija.model.entity.simple.Specialization;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.repository.users.RepositoryCustom;
import lt.akademija.service.simple.SpecializationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
@Repository
public class DoctorRepositoryImpl implements RepositoryCustom<DoctorDTO> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	SpecializationService specializationService;

	@Override
	public DoctorDTO saveDTO(DoctorDTO doctorDTO) throws Exception {
		Doctor doctor = new Doctor();
		doctor = localCopy(doctorDTO, doctor);
		String specializationTitle = doctorDTO.getSpecializationTitle();
		Long specializationId = doctorDTO.getSpecializationId();
		boolean specializationTitleGiven = !(specializationTitle == null || specializationTitle.equals(""));
		boolean specializationIdGiven = !(specializationId == null || specializationId < 1);
		if (specializationTitleGiven && specializationIdGiven) {
			Long specializationIdFound = specializationService.findByTitle(specializationTitle).getId();
			if (!(specializationIdFound == null) && !(specializationIdFound.equals(specializationId))) {
				throw new Exception(
						"Both specialization id and title given. But they did not both point to the same specialization.");
			}
		} else if (specializationTitleGiven) {
			Specialization specializationFound = specializationService.findByTitle(specializationTitle);
			if (!(specializationFound == null)) {
				doctor.setSpecialization(specializationFound);
			} else {
				throw new Exception("");
			}
		}
		
		if (doctor.getSpecialization() == null) {
			doctorDTO.setSpecializationId(null);
		}
		this.em.persist(doctor);
		doctorDTO.setId(doctor.getId());
		this.em.flush();
		return doctorDTO;
	}

	@Override
	public DoctorDTO updateDTOByPid(Long pid, DoctorDTO doctorDTO) {
		Doctor doctor = (Doctor) em.createQuery("Select d from Doctor d where d.pid = ?1").setParameter(1, pid)
				.getSingleResult();
		if (doctor == null) {
			System.out.println("Doctor not found in find by pid");
			return null;
		}
		doctor = localCopy(doctorDTO, doctor);
		if (doctor.getSpecialization() == null) {
			doctorDTO.setSpecializationId(null);
		}
		this.em.merge(doctor);
		this.em.flush();
		return doctorDTO;
	}

	@Override
	public DoctorDTO updateDTOById(Long id, DoctorDTO doctorDTO) {
		Doctor doctor = (Doctor) em.createQuery("Select d from Doctor d where d.id = ?1").setParameter(1, id)
				.getSingleResult();
		if (doctor == null) {
			System.out.println("Doctor not found in find by Id");
			return null;
		}
		doctor = localCopy(doctorDTO, doctor);
		if (doctor.getSpecialization() == null) {
			doctorDTO.setSpecializationId(null);
		}
		this.em.merge(doctor);
		this.em.flush();
		return doctorDTO;
	}

	private Doctor localCopy(DoctorDTO doctorDTO, Doctor doctor) {
		Long id = doctorDTO.getSpecializationId();
		if (id != null) {
			Specialization specialization = (Specialization) em
					.createQuery("Select s from Specialization s where s.id = ?1").setParameter(1, id)
					.getSingleResult();
			if (specialization != null) {
				System.out.println("Specialization found: " + specialization);
				doctor.setSpecialization(specialization);
			}
		} else {
			System.out.println("Specialization not found in doctor repo");
		}
		doctor.setPid(doctorDTO.getPid());
		doctor.setFirstName(doctorDTO.getFirstName());
		doctor.setLastName(doctorDTO.getLastName());
		return doctor;
	}

}
