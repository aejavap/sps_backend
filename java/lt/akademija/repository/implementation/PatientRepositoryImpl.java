package lt.akademija.repository.implementation;

import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
@Repository
public class PatientRepositoryImpl implements RepositoryCustom<PatientDTO> {

    @PersistenceContext
    private EntityManager em;

    /**
     * Creates patient entity from passed DTO patient instance;
     *
     * @param patientDTO - patient DTO;
     * @return - return DTO of created patient;
     */
    @Override
    public PatientDTO saveDTO(PatientDTO patientDTO) {
        Patient patient = new Patient();
        patient = localCopy(patientDTO, patient);
        if (patient.getDoctor() == null) {
            patientDTO.setDoctorId(null);
        }
        this.em.persist(patient);
        patientDTO.setId(patient.getId());
        this.em.flush();
        return patientDTO;
    }

    /**
     * Updates patient by its pid
     * @param pid - patient pid;
     * @param patientDTO - patientDTO;
     * @return created patient DTO;
     */
    @Override
    public PatientDTO updateDTOByPid(Long pid, PatientDTO patientDTO) {
        Patient patient = (Patient) em.createQuery("Select d from Patient d where d.pid = ?1")
                .setParameter(1, pid).getSingleResult();
        if (patient == null) {
            System.out.println("Patient not found in patient update by pid");
            return null;
        }
        patient = localCopy(patientDTO, patient);
        if (patient.getDoctor() == null) {
            System.out.println("Patient not found in patient update by pid");
            patientDTO.setDoctorId(null);
        }
        this.em.merge(patient);
        this.em.flush();
        return patientDTO;
    }

    /**
     * Finds patient by id and calls patient update method;
     *
     * @param Id         - patient id;
     * @param patientDTO - DTO of the patient
     * @return - if successful returns DTO else returns null;
     */

    @Override
    public PatientDTO updateDTOById(Long Id, PatientDTO patientDTO) {
        System.out.println("Update by Id in DTO called");
        Patient patient = (Patient) em.createQuery("Select d from Patient d where d.id = ?1")
                .setParameter(1, Id).getSingleResult();
        if (patient == null) {
            System.out.println("Patient not found in patient update by id");
            return null;
        }
        patient = localCopy(patientDTO, patient);
        if (patient.getDoctor() == null) {
            System.out.println("Patient not found in patient update by id");
            patientDTO.setDoctorId(null);
        }
        this.em.merge(patient);
        this.em.flush();
        System.out.println("PatientDTO dob in updateById: "+patientDTO.getDob());
        return patientDTO;
    }

    /**
     * Helper method used in other two update methods. Copies fields, finds doctor;
     *
     * @param patientDTO - patient DTO;
     * @param patient    - patient entity;
     * @return updated patient entity;
     */

    private Patient localCopy(PatientDTO patientDTO, Patient patient) {
        Long id = patientDTO.getDoctorId();
        Doctor doctor = null;
        if(id != null)
        {
            doctor = (Doctor) em.createQuery("Select d from Doctor d where d.id = ?1")
                    .setParameter(1, id).getSingleResult();
        }
        if (doctor != null) {
            System.out.println("doctor found: " + doctor);
            patient.setDoctor(doctor);
        }
        patient.setPid(patientDTO.getPid());
        patient.setDob(patientDTO.getDob());
        patient.setFirstName(patientDTO.getFirstName());
        patient.setLastName(patientDTO.getLastName());
        return patient;
    }

}
