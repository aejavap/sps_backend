package lt.akademija.repository.implementation;

import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.RepositoryCustom;
import lt.akademija.service.simple.IngredientService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

// reikia aptvarkyt savo koda (darius apie savo koda cia)
@Transactional
@Repository
public class PrescriptionRepositoryImpl implements RepositoryCustom<PrescriptionDTO> {
    private static Logger logger = Logger.getLogger(PrescriptionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    IngredientService ingredientService;

    @Override
    public PrescriptionDTO saveDTO(PrescriptionDTO prescriptionDTO) throws Exception {
        Long id = prescriptionDTO.getActiveIngredientId();
        String ingredientTitle = prescriptionDTO.getActiveIngredientName();
        boolean ingredientIdGiven = !(id == null || id < 1);
        boolean ingredientTitleGiven = !(ingredientTitle == null || ingredientTitle.equals(""));
        logger.debug("DTO received : " + prescriptionDTO);
        logger.debug("Ingredient title given : " + ingredientTitleGiven);
        logger.debug("Ingredient id given : " + ingredientIdGiven);
        Ingredient ingredient = null;
        if (ingredientIdGiven && ingredientTitleGiven) {
        	ingredient = ingredientService.findById(id);
        	String titleFoundById = ingredient.getTitle();
        	if ( !ingredientTitle.equals(titleFoundById) ) {
        		throw new Exception("Both ingredient id and title were given but they did not point to the same ingredient.");
        	} else {
        		logger.trace("Ingredient set to " + ingredient);
        	}
        } else if (ingredientTitleGiven) {
        	ingredient = ingredientService.findByTitle(ingredientTitle);
        	if (ingredient == null) {
        		throw new Exception("Ingredient title was given as " + ingredientTitle + " but no corresponding ingredient found.");
        	} else {
        		logger.trace("Ingredient set to " + ingredient);
        	}
        } else if (ingredientIdGiven) {
        	ingredient = ingredientService.findById(id);
        	if (ingredient == null) {
        		throw new Exception("Ingredient id was given as " + id + " but no corresponding ingredient found.");
        	} else {
        		logger.trace("Ingredient set to " + ingredient);
        	}
        } else {
        	throw new Exception("Neither ingredient id nor ingredient title were given.");
        }
        
        id = prescriptionDTO.getDoctorId();
        if(id == null)
        {
            logger.error("Doctor id required. Current value null");
            return null;
        }
        Doctor doctor = (Doctor)em.createQuery("Select d from Doctor d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        if(doctor == null)
        {
            logger.error("Doctor not found: id ="+id);
            return null;
        }
        Long pid = prescriptionDTO.getPatientPid();
        id = prescriptionDTO.getPatientId();
        Patient patient = null;
        if(pid != null && pid >= 0)
        {
            logger.info("Patient search by pid. Current value: "+pid);
            patient = (Patient)em.createQuery("Select d from Patient d where d.pid = ?1")
                    .setParameter(1,pid).getSingleResult();
            if(patient == null)
            {
                logger.error("Patient not found. Wrong pid: "+pid);
                return null;
            }
            prescriptionDTO.setPatientId(patient.getId());

        }else if(id != null && id >= 0)
        {
            logger.info("Patient search by id. Current value: "+id);
            patient = (Patient)em.createQuery("Select d from Patient d where d.id = ?1")
                    .setParameter(1,id).getSingleResult();
            if(patient == null)
            {
                logger.error("Patient not found. Wrong id: "+id);
                return null;
            }
            prescriptionDTO.setPatientPid(patient.getPid());
        }else{
            logger.error("Patient pid and id null");
            return null;
        }
        if(patient == null)
        {
            logger.error("Patient not found: id ="+id+" pid: "+pid);
            return null;
        }
        Prescription prescription = new Prescription(prescriptionDTO, ingredient, doctor, patient);
        this.em.persist(prescription);
        prescriptionDTO.setPrescriptionId(prescription.getId());
        logger.debug("Created prescription: "+prescription.toString());
        prescriptionDTO.setDoctorFirstName(prescription.getDoctor().getFirstName());
        prescriptionDTO.setDoctorLastName(prescription.getDoctor().getLastName());
        prescriptionDTO.setPrescriptionDate(prescription.getPrescriptionDate());
        prescriptionDTO.setActiveIngredientName(prescription.getActiveIngredient().getTitle());
        this.em.flush();
        return prescriptionDTO;
    }

    @Override
    public PrescriptionDTO updateDTOByPid(Long pid, PrescriptionDTO DTO) {
        return null;
    }

    @Override
    public PrescriptionDTO updateDTOById(Long id, PrescriptionDTO DTO) {
        return null;
    }
}
