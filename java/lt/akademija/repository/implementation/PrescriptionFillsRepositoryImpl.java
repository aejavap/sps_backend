package lt.akademija.repository.implementation;

import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.PrescriptionFills;

import lt.akademija.model.entity.users.Pharmacist;
import lt.akademija.repository.users.RepositoryCustom;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;

@Transactional
@Repository
public class PrescriptionFillsRepositoryImpl implements RepositoryCustom<PrescriptionFillsDTO> {
    private static Logger logger = Logger.getLogger(PrescriptionRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public PrescriptionFillsDTO saveDTO(PrescriptionFillsDTO prescriptionFillsDTO) {
        Long id = prescriptionFillsDTO.getPhramacistId();
        if(id == null)
        {
            logger.error("In PrescriptionFillsRepo pharmacistId null passed");
            return null;
        }
        Pharmacist pharmacist = (Pharmacist) em.createQuery("Select d from Pharmacist d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        if(pharmacist == null)
        {
            System.out.println("Pharmacist not found in prescription fills");
            return null;
        }
        prescriptionFillsDTO.setPhFirstName(pharmacist.getFirstName());
        prescriptionFillsDTO.setPhLastName(pharmacist.getLastName());
        id = prescriptionFillsDTO.getPrescriptionId();
        if(id == null)
        {
            logger.error("In PrescriptionFillsRepo prescriptionid null passed");
            return null;
        }
        Prescription prescription = (Prescription) em.createQuery("Select d from Prescription d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        if(prescription == null)
        {
            logger.error("Prescription not found");
            return null;
        }
        Date date = prescriptionFillsDTO.getDate();
        PrescriptionFills prescriptionFills = new PrescriptionFills(date, pharmacist, prescription);
        prescriptionFillsDTO.setDate(prescriptionFills.getPrescriptionFillsPK().getTimestamp());
        this.em.persist(prescriptionFills);
        this.em.flush();
        return prescriptionFillsDTO;
    }

    @Override
    public PrescriptionFillsDTO updateDTOByPid(Long pid, PrescriptionFillsDTO DTO) {
        return null;
    }

    @Override
    public PrescriptionFillsDTO updateDTOById(Long id, PrescriptionFillsDTO DTO) {
        return null;
    }
}
